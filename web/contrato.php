<?php        
if (isset($_GET["submit"])) {
    if (isset($_GET['id']) && isset($_GET['tipo'])) {
        $id = $_GET['id'];
        $tipo = $_GET['tipo'];
        $contrato = "CONTRATO";
        if($tipo == "COMPRA"){
            $contrato = "CONTRATO_COMPRA";
        }
        $target_dir = "../../optra/operations/" . $id . "/";
        if(is_dir($target_dir)){
            unlink($target_dir.$contrato.".pdf");
        }
        else{
            mkdir($target_dir);   
        }
        try {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_dir .$contrato.".pdf")) {
                echo "OK";           
            } else {
                echo "Error: No se pudieron guardar los cambios en el servidor, reintente.";
            }
        } catch (Exception $ex) {
            die("ERROR: Falló la conexión con el servidor.");
        }
    } else {
        die("NO AUTORIZADO.");
    }
} else {
    die("NO AUTORIZADO.");
}
?>
