<?php        
if (isset($_GET["submit"])) {
    if (isset($_GET['rut'])) {
        $rut = $_GET['rut'];
        $target_dir = "../propietarios/" . $rut . "/";
        if(is_dir($target_dir)){
            unlink($target_dir."template.bin");
        }
        else{
            mkdir($target_dir);   
        }
        try {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_dir ."template.bin")) {
                echo "OK";
            } else {
                echo "Error: No se pudieron guardar los cambios en el servidor, reintente.";
            }
        } catch (Exception $ex) {
            die("ERROR: Falló la conexión con el servidor.");
        }
    } else {
        die("NO AUTORIZADO.");
    }
} else {
    die("NO AUTORIZADO.");
}
?>
