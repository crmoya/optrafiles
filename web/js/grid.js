$(document).ready(function () {
    $('.clickable').click(function (e) {
        var href = $(this).data('href');
        location.href = href;
    });
    $('.clickable').css('cursor', 'pointer');
});
$(document).on('pjax:complete', function () {
    $('.clickable').click(function (e) {
        var href = $(this).data('href');
        location.href = href;
    });
    $('.clickable').css('cursor', 'pointer');
});