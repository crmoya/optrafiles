/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).on('keyup', 'input[id*="rut"]', function () {
    var rut = $(this).val();
    rut = $.formatRut(rut);
    $(this).val(rut);
});
$(document).on('keyup', 'input[id*="cedula_identidad"]', function () {
    var rut = $(this).val();
    rut = $.formatRut(rut);
    $(this).val(rut);
});
$(document).on('keyup', 'input.register-rut', function () {
    var rut = $(this).val();
    rut = $.formatRut(rut);
    $(this).val(rut);
});
$(document).on('focusout', 'input.optional-rut', function () {
    if ($(this).val().match(/[0-9]/g)) {
        var rut = $(this).val();
        rut = $.formatRut(rut);
        $(this).val(rut);
    }
});
$(document).on('focusout', 'input.rut-alternative', function () {
    var idType = $('.id-type').select2('data')[0].text;
    console.log(idType);
    if (idType.toLowerCase().indexOf("rut") >= 0 || idType.toLowerCase().indexOf("run") >= 0) {
        var rut = $(this).val();
        rut = $.formatRut(rut);
        $(this).val(rut);
    }
});