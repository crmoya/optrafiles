$('#otattachform-file').on('fileloaded', function (event, file, previewId, index, reader) {
    var name = file.name.replace(/\.[^/.]+$/, "");
    name = name.toUpperCase();
    if (!$('#otattachform-description').val()) {
        $('#otattachform-description').val(name);
    }
});
$('#otattachform-file').on('fileuploaded', function(event, data, previewId, index) {
    var jsonResponse = JSON.parse(data.jqXHR.responseJSON);
    
    if(jsonResponse.success){
        location.reload(true);
    }
});