function getIndividualData(data) {
    var individualData = [];
    var num = data.indexOf("Tipo Vehículo");
    var num2 = data.indexOf("Año");
    var num3 = data.indexOf("Marca");
    var num4 = data.indexOf("Modelo");
    var num5 = data.indexOf("Nro. Motor");
    var num6 = data.indexOf("Nro. Chasis");
    var num7 = data.indexOf("Nro. Serie");
    var num8 = data.indexOf("Nro. Vin");
    var num9 = data.indexOf("Color");
    var num10 = data.indexOf("Inscripción");
    var num11 = data.indexOf("DATOS DEL VEHICULO");
    var num12 = data.indexOf("Nombre");
    var num13 = (data.indexOf("R.U.N.") != -1) ? data.indexOf("R.U.N.") : data.indexOf("R.U.T.");
    var num14 = data.indexOf("Fec. adquisición");
    var num15 = data.indexOf("Combustible");
    var flag = (num10 != -1);
    if (flag)
    {
        individualData["vehiclePatent"] = data.substring(num10 + 13, num11).trim();
    }
    var flag2 = num != -1;
    if (flag2)
    {
        individualData["vehicleType"] = data.substring(num + 13, num2).trim();
    }
    var flag3 = num2 != -1;
    if (flag3)
    {
        individualData["vehicleYear"] = data.substring(num2 + 3, num3).trim();
    }
    var flag4 = num3 != -1;
    if (flag4)
    {
        individualData["vehicleBrand"] = data.substring(num3 + 5, num4).trim();
    }
    var flag5 = num5 > 0;
    if (flag5)
    {
        individualData["vehicleModel"] = data.substring(num4 + 6, num5).trim();
    } else
    {
        var flag6 = num6 != -1;
        if (flag6)
        {
            individualData["vehicleModel"] = data.substring(num4 + 6, num6).trim();
        } else
        {
            var flag7 = num7 != -1;
            if (flag7)
            {
                individualData["vehicleModel"] = data.substring(num4 + 6, num7).trim();
            } else
            {
                var flag8 = num8 != -1;
                if (flag8)
                {
                    individualData["vehicleModel"] = data.substring(num4 + 6, num8).trim();
                } else
                {
                    individualData["vehicleModel"] = data.substring(num4 + 6, num9).trim();
                }
            }
        }
    }
    var flag9 = num5 != -1;
    if (flag9)
    {
        var flag10 = num6 > 0;
        if (flag10)
        {
            individualData["vehicleMotorNumber"] = data.substring(num5 + 10, num6).trim();
        } else
        {
            individualData["vehicleMotorNumber"] = data.substring(num5 + 10, num7).trim();
        }
    }
    var flag11 = num6 > 0 && num7 < 0;
    if (flag11)
    {
        var flag12 = num8 > 0;
        if (flag12)
        {
            individualData["vehicleChassisNumber"] = data.substring(num6 + 11, num8).trim();
        } else
        {
            individualData["vehicleChassisNumber"] = data.substring(num6 + 11, num9).trim();
        }
    } else
    {
        var flag13 = num6 > 0 && num7 > 0;
        if (flag13)
        {
            individualData["vehicleChassisNumber"] = data.substring(num6 + 11, num7).trim();
        } else
        {
            individualData["vehicleChassisNumber"] = data.substring(num7 + 10, num9).trim();
        }
    }
    var flag14 = num13 != -1;
    if (flag14)
    {
        individualData["ownerRUN"] = data.substring(num13 + 6, num14).trim();
    }

    var flag15 = num12 > 0;
    if (flag15) {
        individualData["ownerName"] = data.substring(num12 + 6, num13).trim();
    }

    var flag16 = num9 > 0;
    if (flag16) {
        individualData['vehicleColor'] = data.substring(num9 + 5, num15).trim();
    }


    return individualData;

}

$(document).ready(function () {
    $('#materiavehiculosform-monto').focusout(function () {
        $('#materiavehiculosform-monto_palabras').val(getWordForNumber($(this).val()));
    });
});

$('.barcode-button').click(function (e) {
    $(document).scannerDetection({
        avgTimeByChar: 40,
        stopPropagation: false, // Stop immediate propagation on keypress event
        preventDefault: false, // Prevent default action on keypress event
        onComplete: function (barcode, qty) {
            var editedBarcode = barcode.split("%N").join("Ñ")
                    .split("%n").join("ñ")
                    .split("$A").join("Á")
                    .split("$a").join("á")
                    .split("$E").join("É")
                    .split("$e").join("é")
                    .split("$I").join("Í")
                    .split("$i").join("í")
                    .split("$O").join("Ó")
                    .split("$o").join("ó")
                    .split("$U").join("Ú")
                    .split("$u").join("ú")
                    .split("%3").join("-")
                    .split("%.").join("")
                    .split("$C").join("")
                    .split("$").join("")
                    .split("%").join("");
            var individualData = getIndividualData(editedBarcode);

            $('#materiavehiculosform-vendedor').val(individualData.ownerName);
            $('#materiavehiculosform-rut_vendedor').val(individualData.ownerRUN);
            $('#materiavehiculosform-tipo_vehiculo').val(individualData.vehicleType);
            $('#materiavehiculosform-marca').val(individualData.vehicleBrand);
            $('#materiavehiculosform-modelo').val(individualData.vehicleModel);
            $('#materiavehiculosform-ano_vehiculo').val(individualData.vehicleYear);
            $('#materiavehiculosform-motor').val(individualData.vehicleMotorNumber);
            $('#materiavehiculosform-patente').val(individualData.vehiclePatent);
            $('#materiavehiculosform-chasis').val(individualData.vehicleChassisNumber);
            $('#materiavehiculosform-color').val(individualData.vehicleColor);

            $('#modal').modal('hide');
        },
        onError: function (string) {
            console.log(string);
        }
    });
});



function Unidades(num) {

    switch (num)
    {
        case 1:
            return 'UNO';
        case 2:
            return 'DOS';
        case 3:
            return 'TRES';
        case 4:
            return 'CUATRO';
        case 5:
            return 'CINCO';
        case 6:
            return 'SEIS';
        case 7:
            return 'SIETE';
        case 8:
            return 'OCHO';
        case 9:
            return 'NUEVE';
    }

    return '';
}//Unidades()

function Decenas(num) {

    var decena = Math.floor(num / 10);
    var unidad = num - (decena * 10);

    switch (decena)
    {
        case 1:
        switch (unidad)
        {
            case 0:
                return 'DIEZ';
            case 1:
                return 'ONCE';
            case 2:
                return 'DOCE';
            case 3:
                return 'TRECE';
            case 4:
                return 'CATORCE';
            case 5:
                return 'QUINCE';
            default:
                return 'DIECI' + Unidades(unidad);
        }
        case 2:
        switch (unidad)
        {
            case 0:
                return 'VEINTE';
            default:
                return 'VEINTI' + Unidades(unidad);
        }
        case 3:
            return DecenasY('TREINTA', unidad);
        case 4:
            return DecenasY('CUARENTA', unidad);
        case 5:
            return DecenasY('CINCUENTA', unidad);
        case 6:
            return DecenasY('SESENTA', unidad);
        case 7:
            return DecenasY('SETENTA', unidad);
        case 8:
            return DecenasY('OCHENTA', unidad);
        case 9:
            return DecenasY('NOVENTA', unidad);
        case 0:
            return Unidades(unidad);
    }
}//Unidades()

function DecenasY(strSin, numUnidades) {
    if (numUnidades > 0)
        return strSin + ' Y ' + Unidades(numUnidades);

    return strSin;
}//DecenasY()

function Centenas(num) {
    var centenas = Math.floor(num / 100);
    var decenas = num - (centenas * 100);

    switch (centenas)
    {
        case 1:
            if (decenas > 0)
                return 'CIENTO ' + Decenas(decenas);
            return 'CIEN';
        case 2:
            return 'DOSCIENTOS ' + Decenas(decenas);
        case 3:
            return 'TRESCIENTOS ' + Decenas(decenas);
        case 4:
            return 'CUATROCIENTOS ' + Decenas(decenas);
        case 5:
            return 'QUINIENTOS ' + Decenas(decenas);
        case 6:
            return 'SEISCIENTOS ' + Decenas(decenas);
        case 7:
            return 'SETECIENTOS ' + Decenas(decenas);
        case 8:
            return 'OCHOCIENTOS ' + Decenas(decenas);
        case 9:
            return 'NOVECIENTOS ' + Decenas(decenas);
    }

    return Decenas(decenas);
}//Centenas()

function Seccion(num, divisor, strSingular, strPlural) {
    var cientos = Math.floor(num / divisor);
    var resto = num - (cientos * divisor);

    var letras = '';

    if (cientos > 0)
        if (cientos > 1)
            letras = Centenas(cientos) + ' ' + strPlural;
        else
            letras = strSingular;

    if (resto > 0)
        letras += '';

    return letras;
}//Seccion()

function Miles(num) {
    var divisor = 1000;
    var cientos = Math.floor(num / divisor);
    var resto = num - (cientos * divisor)

    var strMiles = Seccion(num, divisor, 'MIL', 'MIL');
    var strCentenas = Centenas(resto);

    if (strMiles == '')
        return strCentenas;

    return strMiles + ' ' + strCentenas;
}//Miles()

function Millones(num) {
    var divisor = 1000000;
    var cientos = Math.floor(num / divisor);
    var resto = num - (cientos * divisor);

    var strMillones = Seccion(num, divisor, 'UN MILLÓN', 'MILLONES');
    var strMiles = Miles(resto);

    if (strMillones == '')
        return strMiles;

    return strMillones + ' ' + strMiles;
}//Millones()

function getWordForNumber(num) {
    var data = {
        numero: num,
        enteros: Math.floor(num),
        parteDecimal: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
        letrasParteDecimal: '',
    };

    if (data.parteDecimal > 0) {
        data.letrasParteDecimal = ' COMA ' + (function () {
            return Millones(data.parteDecimal);
        })();
    }

    if (data.enteros == 0) {
        var ceros = '';
        for (i = 0; i < data.numero.length; i++) {
            if (data.numero.charAt(i) == '0')
                ceros = ceros + 'CERO ';
            else
                break;
        }
        return ceros.trim() + data.letrasParteDecimal;
    } else {
        var ceros = '';
        for (i = 0; i < data.numero.length; i++) {
            if (data.numero.charAt(i) == '0')
                ceros = ceros + 'CERO ';
            else
                break;
        }
        return ceros + Millones(data.enteros) + data.letrasParteDecimal;
    }
}