function createNewOption(e, url, formId, selectId, title) {
    if (e.params.data.id == 0) {
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load(url);
        } else {
//if modal isn't open; open it and load content
            $('#modal').modal('show')
                    .find('#modalContent')
                    .load(url);
        }
//dynamiclly set the header for the modal
        document.getElementById('modalHeader').innerHTML = '<h4>' + title + '</h4>';
        $(document).on('beforeSubmit', formId, function (event) {
            var form = $(this);
            var formData = form.serialize();
            
            $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: formData,
                dataType: 'json',
                success: function (data) {
                    if(!data.success){
                        if(data.message == 'ERROREMPRESASINRUT'){
                            swal("ERROR","Error: No se pueden asociar 2 comparecientes de empresa sin rut en la misma OT.","error");
                        }
                        else{
                            swal("ERROR",data.message,"error");
                        }
                    }
                    $('#modal').modal('hide');
                    if (data.success) {
                        $('.alert')
                                .removeClass('alert-error')
                                .addClass('alert-success')
                                .removeClass('hidden');
                        $('.alert > span').text(data.message);
                        if ($.isArray(selectId)) {
                            $.each(selectId, function (index, value) {
                                if ($(value).find("option[value=" + data.id + "]").length) {
                                    $(value).val(data.id).trigger("change");
                                } else {
                                    // Create the DOM option that is pre-selected by default
                                    var newState = new Option(data.text, data.id, true, true);
                                    // Append it to the select
                                    $(value).append(newState).trigger('change');
                                }
                            });
                        } else {
                            if ($(selectId).find("option[value=" + data.id + "]").length) {
                                $(selectId).val(data.id).trigger("change");
                            } else {
                                // Create the DOM option that is pre-selected by default
                                var newState = new Option(data.text, data.id, true, true);
                                // Append it to the select
                                $(selectId).append(newState).trigger('change');
                            }
                        }
                        selectId = null;    // evita que al haber más de un modal en la misma página, se quede pegado el selectId
                    } else {
                        $('.alert')
                                .removeClass('alert-success')
                                .addClass('alert-error')
                                .removeClass('hidden');
                        $('.alert > span').text(data.message);
                    }
                    $('.alert').fadeOut(2000);
                },
                error: function (e) {
                    console.log(e);
                    swal("ERROR",e.message,"error");
                    $('#modal').modal('hide');
                    $('.alert')
                            .removeClass('alert-success')
                            .addClass('alert-error')
                            .removeClass('hidden');
                    $('.alert > span').text('Hubo un problema al tratar de crear al cliente. Intente nuevamente.');
                }
            });
        })
        $(document).on('submit', formId, function (e) {
            e.preventDefault();
        });
    } else {
        var modifiedId = e.params.data.id;
        var modifiedText = e.params.data.text;
        if ($.isArray(selectId)) {
            $.each(selectId, function (index, value) {
                if ($(value).find("option[value=" + modifiedId + "]").length) {
                    $(value).val(modifiedId).trigger("change");
                } else {
                    // Create the DOM option that is pre-selected by default
                    var newState = new Option(modifiedText, modifiedId, true, true);
                    // Append it to the select
                    $(value).append(newState).trigger('change');
                }
            });
        }
    }
}