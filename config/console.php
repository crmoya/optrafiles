<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'OptraApp',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            //'scriptUrl' => 'http://analiticachile.cl',
            'enablePrettyUrl' => true,
            'rules' => [
                '<controller:\[\w\-]+>/<id:\d+>' => '<controller>/view',
                '<controller:\[\w\-]+>/<action:\[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\[\w\-]+>/<action:\[\w\-]+>' => '<controller>/<action>',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.notariamiranda.cl',
                'username' => 'app@notariamiranda.cl',
                'password' => 'n0t4r14@4pp',
                'port' => '26',
            ],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
        /*
          'controllerMap' => [
          'fixture' => [ // Fixture generation command line.
          'class' => 'yii\faker\FixtureController',
          ],
          ],
         */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
