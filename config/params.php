<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'app@notariamiranda.cl',
    'seguimientoEmail' => 'app@notariamiranda.cl',
    'directEmailSend' => false,
    'user.passwordResetTokenExpire' => 3600,
    'googleSecretClient' => [
        "web" => [
            "client_id" => "202740580766-40ml8e2kann25fi60j64mo1fagkf2su8.apps.googleusercontent.com",
            "project_id" => "notaria-205401",
            "auth_uri" => "https://accounts.google.com/o/oauth2/auth",
            "token_uri" => "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url" => "https://www.googleapis.com/oauth2/v1/certs",
            "client_secret" => "qyA0j7GmMbfGqFFywiVJitie",
            "redirect_uris" => ["http://analiticachile.cl/escritura/oauth2callback"],
        ]
    ],
];
