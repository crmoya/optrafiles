<?php

namespace app\controllers;

use Yii;
use app\models\MatrizTipoVenta;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MatrizTipoVentaController implements the CRUD actions for Modelo model.
 */
class MatrizTipoVentaController extends BaseRbacController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Modelo models.
     * @return mixed
     */
    public function actionIndex() {
        //  \app\models\MatrizTipoVenta::TIPO_VENTA
        $model = new \app\models\MatrizTipoVentaForm();
        $sucursal_id = \app\models\User::findOne(\Yii::$app->user->id)->sucursal_id;
        $datos = array();
        foreach ($model->search($sucursal_id)->getModels() as $files) {
            $datos[] = [$files["description"], $files["url"]]; //, $files["sucursal"]["nombre"]];
        }
        return $this->render("index", ["model" => $model, "datos" => $datos]);
    }

    public function actionAttachDocument($action) {
        $sucursal_id = Yii::$app->user->getIdentity()->sucursal->id;
        $model = new \app\models\MatrizTipoVentaForm();
        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
            if ($action == "personeria") {
                $model->description = \app\models\MatrizTipoVentaForm::PERSONERIA;
            } else if ($action == "mandatario") {
                $model->description = \app\models\MatrizTipoVentaForm::MANDATARIO;
            }
            if ($model->saveDocument($sucursal_id)) {
//                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                Yii::$app->getSession()->setFlash('success', 'Documento cargado exitosamente');
//                return \yii\helpers\Json::encode(['output' => '', 'message' => 'OK', 'success' => true]);
                return $this->redirect(['index', "id" => $sucursal_id, "action" => null]);
            } else {
                Yii::$app->getSession()->setFlash('error', 'Imposible guardar el archivo asociado. Intente más tarde');
                return $this->redirect(['index', "id" => $sucursal_id, "action" => null]);
            }
        } else {
            return $this->renderAjax("attach-document", ["model" => $model, "id" => $sucursal_id]);
        }
    }

    public function actionAttachedView($url) {
        $url_explode = explode(DIRECTORY_SEPARATOR, $url);

        $id = $url_explode[2];
        $archivo = $url_explode[3];

        $desc_begin = strrpos($archivo, DIRECTORY_SEPARATOR) + 1;

        $fileName = substr($archivo, $desc_begin);
        $description = substr($fileName, strpos($fileName, '_') + 1);

        if (file_exists($url)) {
            return Yii::$app->response->sendFile($url, $description);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Imposible descargar el documento adjunto. Intente más tarde');
            return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
        }
    }

    /**
     * Permite eliminar un documento adjunto asociado a la escritura
     * 
     * @param string $url Ruta asociada al documento
     * @return mixed
     */
    public function actionAttachedDelete($url) {
        $url_explode = explode(DIRECTORY_SEPARATOR, $url);
        $id = $url_explode[2];
        if (unlink($url)) {
            Yii::$app->getSession()->setFlash('success', 'Documento adjunto eliminado exitosamente');
            return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Imposible eliminar el documento adjunto. Intente más tarde');
            return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
        }
    }

    /**
     * Deletes an existing MatrizTipoVenta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $path = MatrizTipoVenta::PATH . $model->nombre . '.*';
        $file_array = glob($path);

        foreach ($file_array as $nombre_fichero) {
            unlink($nombre_fichero);
        }
        $model->delete();

        return $this->redirect(['index']);
    }
    */
}
