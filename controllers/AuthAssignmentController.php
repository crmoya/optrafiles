<?php

namespace app\controllers;

use Yii;
use app\models\AuthAssignment;
use app\models\AuthItemChild;
use app\models\AuthAssignmentSearch;
use app\models\UserSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthAssignmentController implements the CRUD actions for AuthAssignment model.
 */
class AuthAssignmentController extends BaseRbacController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthAssignment models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AuthAssignmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthAssignment model.
     * @param string $item_name
     * @param string $user_id
     * @return mixed
     */
    public function actionView($item_name, $user_id) {
        return $this->render('view', [
                    'model' => $this->findModel($item_name, $user_id),
        ]);
    }

    /**
     * Creates a new AuthAssignment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthAssignment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'item_name' => $model->item_name, 'user_id' => $model->user_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthAssignment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $item_name
     * @param string $user_id
     * @return mixed
     */
    public function actionUpdate($item_name, $user_id) {
        $model = $this->findModel($item_name, $user_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'item_name' => $model->item_name, 'user_id' => $model->user_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionAsignar() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('asignar', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUsuario($id) {
        $model = \app\models\User::findOne($id);
        $modelForm = new \app\models\RolUsuarioForm();

        $request = Yii::$app->request;
        $post = $request->post();
        if (isset($post['funciones'])) {
            //elimino todas las funciones asignadas anteriormente al usuario
            \app\models\AuthAssignment::deleteAll(['user_id' => $id]);

            $ok = true;
            if (isset($post['ROLSeleccionado']) && isset($post["RolUsuarioForm"])) {
                //agrego solamente las funciones del rol seleccionado
                $funciones = AuthItemChild::find()->where(['parent' => $post["RolUsuarioForm"]['rol']])->select(['child'])->all();
                foreach ($funciones as $funcion) {
                    $permiso = new \app\models\AuthAssignment();
                    $permiso->user_id = $id;
                    $permiso->item_name = $funcion;
                    $ok = $permiso->save();
                }
                //además debo agregarle el rol al usuario
                if (isset($post["RolUsuarioForm"])) {
                    if (isset($post['RolUsuarioForm']['rol'])) {
                        if ($post['RolUsuarioForm']['rol'] != "") {
                            $permiso = new \app\models\AuthAssignment();
                            $permiso->user_id = $id;
                            $permiso->item_name = $post['RolUsuarioForm']['rol'];
                            $ok = $permiso->save();
                        }
                    }
                }
            } else {
                //agrego solamente las que están seleccionadas en el checkboxlist
                foreach ($post['funciones'] as $nombre => $valor) {
                    if ($valor == 'on') {
                        $permiso = new \app\models\AuthAssignment();
                        $permiso->user_id = $id;
                        $permiso->item_name = $nombre;
                        $ok = $permiso->save();
                    }
                }
                //además debo agregarle el rol al usuario
                if (isset($post["RolUsuarioForm"])) {
                    if (isset($post['RolUsuarioForm']['rol'])) {
                        if ($post['RolUsuarioForm']['rol'] != "") {
                            $permiso = new \app\models\AuthAssignment();
                            $permiso->user_id = $id;
                            $permiso->item_name = $post['RolUsuarioForm']['rol'];
                            $ok = $permiso->save();
                        }
                    }
                }
            }

            if ($ok) {
                Yii::$app->getSession()->setFlash('success', "Funciones asignadas con éxito al usuario " . $model->username);
            } else {
                Yii::$app->getSession()->setFlash('error', "Ocurrió un error al intentar asignar las funciones al usuario " . $model->username . ". Por favor, reintente.");
            }
        }


        return $this->render('usuario', [
                    'model' => $model,
                    'modelForm' => $modelForm,
                    'time' => date('H:i:s')
        ]);
    }

    /**
     * Deletes an existing AuthAssignment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $item_name
     * @param string $user_id
     * @return mixed
     */
    public function actionDelete($item_name, $user_id) {
        $this->findModel($item_name, $user_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthAssignment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $item_name
     * @param string $user_id
     * @return AuthAssignment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($item_name, $user_id) {
        if (($model = AuthAssignment::findOne(['item_name' => $item_name, 'user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
