<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Utils;
use app\models\Propietario;
use app\models\Marca;
use app\models\Modelo;
use app\models\TipoCombustible;
use app\models\TipoVehiculo;
use app\models\Vehiculo;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionInsertarCav(){
        
        $detalles = "";
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $mensaje = "OK";
        $request = Yii::$app->request;
        $rut = $request->post('rut');
        
        $nombres = Utils::replaceVowels($request->post('nombres'));
        $apellido_paterno = Utils::replaceVowels($request->post('apellido_paterno'));
        $apellido_materno = Utils::replaceVowels($request->post('apellido_materno'));
        $natural = $request->post('natural');
        $patente = $request->post('patente');
        $chasis = $request->post('chasis');
        $motor = $request->post('motor');
        $agno = $request->post('agno');
        $nMarca = Utils::replaceVowels($request->post('marca'));
        $nModelo = Utils::replaceVowels($request->post('modelo'));
        $nTipo = Utils::replaceVowels($request->post('tipo'));
        $nCombustible = Utils::replaceVowels($request->post('combustible'));
        $color = Utils::replaceVowels($request->post('color'));
        $pbv = $request->post('pbv');
        
        $ok = true;
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $propietario = Propietario::find()->where(['rut'=>$rut])->one();
            if(is_null($propietario)){
                $propietario = new Propietario();
                $propietario->rut = $rut;
                $propietario->nombres = $nombres;
                $propietario->apellido_paterno = $apellido_paterno;
                $propietario->apellido_materno = $apellido_materno;
                $propietario->persona_natural = (int)$natural;
                if(!$propietario->save()){
                    $ok = false;
                    $detalles .= "PROPIETARIO ";
                }
            }
            
            
            $marca = Marca::find()->where(['nombre'=>$nMarca])->one();
            if(is_null($marca)){
                $marca = new Marca();
                $marca->nombre = $nMarca;
                if(!$marca->save()){
                    $ok = false;
                    $detalles .= "MARCA ";
                }
            }
            

            $modelo = Modelo::find()->where(['nombre'=>$nModelo,'marca_id'=>$marca->id])->one();
            if(is_null($modelo)){
                $modelo = new Modelo();
                $modelo->nombre = $nModelo;
                $modelo->marca_id = $marca->id;
                if(!$modelo->save()){
                    $ok = false;
                    $detalles .= "MODELO ";
                }
            }

            $tipo = TipoVehiculo::find()->where(['nombre'=>$nTipo])->one();
            if(is_null($tipo)){
                $tipo = new TipoVehiculo();
                $tipo->nombre = $nTipo;
                if(!$tipo->save()){
                    $ok = false;
                    $detalles .= "TIPO: ".implode(',', $tipo->getErrors());
                }
            }
            

            $combustible = TipoCombustible::find()->where(['nombre'=>$nCombustible])->one();
            if(is_null($combustible)){
                $combustible = new TipoCombustible();
                $combustible->nombre = $nCombustible;
                if(!$combustible->save()){
                    $ok = false;
                    $detalles .= "COMBUSTIBLE ";
                }
            }

            $vehiculo = Vehiculo::find()->where(['patente'=>$patente])->one();
            
            if(is_null($vehiculo)){
                $vehiculo = new Vehiculo();
                $vehiculo->patente = $patente;
                $vehiculo->chasis = $chasis;
                $vehiculo->motor = $motor;
                $vehiculo->agno = $agno;
                $vehiculo->modelo_id = $modelo->id;
                $vehiculo->tiene_prenda = 0;
                $vehiculo->tiene_multa = 0;
                $vehiculo->tipo_vehiculo_id = $tipo->id;
                $vehiculo->color = $color;
                $vehiculo->tipo_combustible_id = $combustible->id;
                $vehiculo->pbv = $pbv;
                $vehiculo->propietario_id = $propietario->id;
                $vehiculo->tasacion_comercial = 0;
                $vehiculo->fecha_recepcion = date('Y-m-d h:i:s');
                if(!$vehiculo->save()){
                    $ok = false;
                    $detalles .= "VEHICULO: ".implode(',', $vehiculo->getErrors());
                }
                
            
            }
            
            if($ok){
                $transaction->commit();
            }
            else{
                $mensaje = "ERROR transaction ".$detalles;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $mensaje = "ERROR E ".$e->getMessage();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $mensaje = "ERROR T ".$e->getMessage();
        }
        return ['mensaje'=>$mensaje];
    }

    public function actionInsertarCav2(){
        
        $detalles = "";
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $mensaje = "OK";
        $request = Yii::$app->request;
        $patente = $request->post('patente');
        $fecha = $request->post('fecha');
        $multas = $request->post('multas');
        $prenda = $request->post('prenda');
        
        $ok = true;
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $vehiculo = Vehiculo::find()->where(['patente'=>$patente])->one();
            if(!is_null($vehiculo)){
                $vehiculo->fecha_cav = $fecha;
                $vehiculo->tiene_multa = (int)$multas;
                $vehiculo->tiene_prenda = (int)$prenda;
                $ok = $vehiculo->save();
            }
            
            if($ok){
                $transaction->commit();
            }
            else{
                $transaction->rollBack();
                $mensaje = "ERROR";
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $mensaje = "ERROR E ".$e->getMessage();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $mensaje = "ERROR T ".$e->getMessage();
        }
        return ['mensaje'=>$mensaje];
    }

    
    public function actionGetPropietario($rut){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $propietario = \app\models\Propietario::find()->where(['rut'=>$rut])->one();
        if(!is_null($propietario)){
            $enviar = [
                'nombres'=>$propietario->nombres,
                'apellido_paterno'=>$propietario->apellido_paterno,
                'apellido_materno'=>$propietario->apellido_materno,
                'fecha_fotos'=>$propietario->fecha_fotos,
                'fecha_hora' => 'el '.date("d/m/Y").' a las '.date("H:i")
            ];
            
            return $enviar;
        }
        return null;
    }
    
    public function actionGetRepresentante($rut){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $representante = \app\models\Representante::find()->where(['rut'=>$rut])->one();
        if(!is_null($representante)){
            $enviar = [
                'nombres'=>$representante->nombre,
                'apellido_paterno'=>$representante->apellido_paterno,
                'apellido_materno'=>$representante->apellido_materno,
                'fecha_fotos'=>$representante->fecha_fotos,
                'fecha_hora' => 'el '.date("d/m/Y").' a las '.date("H:i")
            ];
            
            return $enviar;
        }
        return null;
    }
    
    public function actionGetMandatario($rut){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $mandatario = \app\models\Mandatario::find()->where(['rut'=>$rut])->one();
        if(!is_null($mandatario)){
            $enviar = [
                'nombres'=>$mandatario->nombres,
                'apellido_paterno'=>$mandatario->apellido_paterno,
                'apellido_materno'=>$mandatario->apellido_materno,
                'fecha_fotos'=>$mandatario->fecha_fotos,
                'fecha_hora' => 'el '.date("d/m/Y").' a las '.date("H:i")
            ];
            
            return $enviar;
        }
        return null;
    }
    
    public function actionGetOperacion($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $operacion = \app\models\Operacion::findOne($id);
        $esCompra = true;
        if(is_null($operacion->compra_fecha) && is_null($operacion->compra_precio) && is_null($operacion->compra_propietario_id) && is_null($operacion->compra_tipo) && is_null($operacion->compra_personeria) && is_null($operacion->compra_user_id) && is_null($operacion->compra_para_id)){
            $esCompra = false;
        }
        
        $venta_representantes = [];
        $compra_representantes = [];
        $venta_propietarios = [];
        $compra_propietarios = [];
        if(!is_null($operacion)){
            
            if($operacion->venta_tipo == 2){
                $ventaRepresentantes = $operacion->ventaRepresentantes;
                foreach($ventaRepresentantes as $ventaRep){
                    $venta_representantes[] = ['rut'=>$ventaRep->representante->rut];
                }
            }
            
            if($operacion->compra_tipo == 2){
                $compraRepresentantes = $operacion->compraRepresentantes;
                foreach($compraRepresentantes as $compraRep){
                    $compra_representantes[] = ['rut'=>$compraRep->representante->rut];
                }
            }
            
            if($operacion->venta_tipo == 4){
                $ventaPropietarios = $operacion->ventaPropietarios;
                foreach($ventaPropietarios as $ventaProp){
                    $venta_propietarios[] = ['rut'=>$ventaProp->propietario->rut];
                }
            }
            
            if($operacion->compra_tipo == 4){
                $compraPropietarios = $operacion->compraPropietarios;
                foreach($compraPropietarios as $compraProp){
                    $compra_propietarios[] = ['rut'=>$compraProp->propietario->rut];
                }
            }
            
            
            
            $enviar = [
                'id'=>$id,
                'venta_propietario_rut'=>$operacion->ventaPropietario->rut,
                'compra_propietario_rut'=>$operacion->compraPropietario->rut,
                'venta_tipo' => $operacion->venta_tipo,
                'compra_tipo' => $operacion->compra_tipo,
                'venta_representantes' => $venta_representantes,
                'compra_representantes' => $compra_representantes,
                'venta_propietarios' => $venta_propietarios,
                'compra_propietarios' => $compra_propietarios,
                'venta_mandatario_rut' => $operacion->ventaMandatario->rut,
                'compra_para_rut' => $operacion->compraPara->rut,
                'es_compra' => $esCompra,
            ];
            
            return $enviar;
        }
        return null;
    }    
    
    public function actionGetContrato(){
        $request = Yii::$app->request;
        $id = $request->get('id');
        $tipo = $request->get('tipo');
        
        if($id != null){
            $operacion = \app\models\Operacion::findOne($id);
            if(!is_null($operacion)){
                $sucursal_id = $operacion->sucursal_id;
                $fullname = realpath(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "optra" . DIRECTORY_SEPARATOR . 'operations' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $tipo . '.docx');
                if(file_exists($fullname)){
                    return Yii::$app->response->sendFile($fullname);    
                }
            }
        }
        throw new \yii\web\NotFoundHttpException();
    }   
    
    public function actionHuella() {
        $request = Yii::$app->request;
        $rut = $request->get('rut');
        if($rut != null){
            $fullname = realpath(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'propietarios' . DIRECTORY_SEPARATOR . $rut . DIRECTORY_SEPARATOR . 'huella.jpg');
            if(file_exists($fullname)){
                return Yii::$app->response->sendFile($fullname);    
            }
        }
        throw new \yii\web\NotFoundHttpException();
    }
    
    public function actionFirma() {
        $request = Yii::$app->request;
        $rut = $request->get('rut');
        if($rut != null){
            $fullname = realpath(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'propietarios' . DIRECTORY_SEPARATOR . $rut . DIRECTORY_SEPARATOR . 'firma.jpeg');
            if(file_exists($fullname)){
                return Yii::$app->response->sendFile($fullname);    
            }
        }
        throw new \yii\web\NotFoundHttpException();
    }
    
    public function actionTemplate() {
        $request = Yii::$app->request;
        $rut = $request->get('rut');
        if($rut != null){
            $fullname = realpath(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'propietarios' . DIRECTORY_SEPARATOR . $rut . DIRECTORY_SEPARATOR . 'template.bin');
            if(file_exists($fullname)){
                return Yii::$app->response->sendFile($fullname);    
            }
        }
        throw new \yii\web\NotFoundHttpException();
    }
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        $model = new LoginForm();
        $model->rememberMe = FALSE;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = \app\models\User::find()->where("id = :id", [":id" => Yii::$app->user->id])->one();
            if ($user->requiere_cambio_pass === 1) {
                $user->generatePasswordResetToken();
                $user->save();
                return $this->redirect(["reset-password", "token" => $user->password_reset_token]);
            } else {
                return $this->redirect(['site/index']);
            }
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Revisa tu email para recibir más instrucciones.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Lo sentimos, no fue posible procesar su solicitud. Intente más tarde.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Nueva contraseña creada.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

}
