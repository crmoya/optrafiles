<?php

namespace app\controllers;

class VentaController extends \yii\web\Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = new \app\models\VentaForm();
        if ($model->load(\Yii::$app->request->post())) {
            if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[1]) {
                return $this->render("venta_unica", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[2]) {
                return $this->render("venta_unica", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[3]) {
                return $this->render("venta_unica", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[4]) {
                return $this->render("venta_unica", ["model" => $model]);
            }
        }
        return $this->render('index', ["model" => $model]);
    }

}
