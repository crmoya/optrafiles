<?php

namespace app\controllers;

use Yii;
use app\models\AuthItem;
use app\models\AuthItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthItemController implements the CRUD actions for AuthItem model.
 */
class AuthItemController extends BaseRbacController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexrol() {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->searchRol(Yii::$app->request->queryParams);

        return $this->render('indexrol', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItem();
        $auth = Yii::$app->authManager;

        if ($model->load(Yii::$app->request->post())) {
            if (empty($auth->getRole($model->name)) && empty($auth->getPermission($model->name))) {
                $role = $auth->createRole($model->name);
                $role->description = $model->description;
                if ($auth->add($role))
                    return $this->redirect(['view', 'id' => $model->name]);
                else
                    Yii::$app->getSession()->setFlash('error', "Imposible agregar el rol. Intente más tarde");
            }
            else {
                Yii::$app->getSession()->setFlash('error', "El rol ya existe");
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionPermisos($name) {
        $model = AuthItem::find()->where(['name' => $name])->one();

        $request = Yii::$app->request;
        $post = $request->post();
        if (isset($post['funciones'])) {
            //elimino todas las funciones asignadas anteriormente al rol
            $children = \app\models\AuthItemChild::find()
                            ->select('child')
                            ->where(['parent' => $name])
                            ->asArray()->all();

            $eliminar = array();
            foreach ($children as $child) {
                $item = AuthItem::find()->where(['name' => $child])->one();
                if ($item->type == 2) {
                    \app\models\AuthItemChild::deleteAll(['parent' => $name, 'child' => $child]);
                }
            }

            //agrego solamente las que están seleccionadas en el checkboxlist
            foreach ($post['funciones'] as $nombre => $valor) {
                if ($valor == 'on') {
                    $hijo = new \app\models\AuthItemChild();
                    $hijo->parent = $name;
                    $hijo->child = $nombre;
                    $hijo->save();
                }
            }
            Yii::$app->getSession()->setFlash('success', "Permisos con éxito asignados al rol " . $name);
        }
        return $this->render('permisos', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $auth = Yii::$app->authManager;
        $item = $auth->getRole($id);
        if (!empty($item)) {
            if ($auth->remove($item))
                Yii::$app->getSession()->setFlash('success', "Rol eliminado exitosamente");
            else
                Yii::$app->getSession()->setFlash('error', "Imposible eliminar Rol. Intente más tarde");
        } else {
            $item = $auth->getPermission($id);
            if (!empty($item)) {
                if ($auth->remove($item))
                    Yii::$app->getSession()->setFlash('success', "Permiso eliminado exitosamente");
                else
                    Yii::$app->getSession()->setFlash('error', "Imposible eliminar permiso. Intente más tarde");
            }
        }

        return $this->redirect(['indexrol']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
