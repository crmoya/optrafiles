<?php

namespace app\controllers;

use Yii;
use app\models\Operacion;
use app\models\OperacionSearch;
use yii\web\Controller;
use \yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OperacionController implements the CRUD actions for Operacion model.
 */
class OperacionController extends BaseRbacController {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Operacion models.
     * @return mixed
     */
    public function actionIndex() {
        
    }

    /**
     * Creates a new Operacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new \app\models\OperacionForm();
        if ($model->load(Yii::$app->request->post())) {
            $vehiculo = \app\models\Vehiculo::find()->joinWith(["modelo", "propietario"])->where("patente = :patente", ["patente" => $model->patente])->one();
            if ($vehiculo->propietario->persona_natural === 0 && $model->tipoVenta == 1) {
                Yii::$app->session->addFlash("error", "Esta patente está asociada a una empresa. Solo puede realizar ventas con Representante Empresa, Mandatario o Comunidad");
                return $this->redirect(["create", "model" => $model]);
            }
            if (isset($vehiculo)) {
                if ($vehiculo->validateNotInProcess()) {
                    $propietario = $vehiculo->propietario;
                    $model->vehiculo_id = $vehiculo->id;
                    $model->rut = $propietario->rut;
                    $model->nombre = $propietario->nombres;
                    $model->apellido_paterno = $propietario->apellido_paterno;
                    $model->apellido_materno = $propietario->apellido_materno;
                    $model->direccion = $propietario->direccion;
                    $model->persona_natural = $propietario->persona_natural;
                    if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[3]) {
                        $model->mandante_rut = $propietario->rut;
                        $model->mandante_nombre = $propietario->nombres;
                        $model->mandante_apellido_paterno = $propietario->apellido_paterno;
                        $model->mandante_apellido_materno = $propietario->apellido_materno;
                        $model->mandante_direccion = $propietario->direccion;
                        $model->mandante_persona_natural = $propietario->persona_natural;
                    }

                    $model->funcionario = Yii::$app->user->id;
                    $model->tipo_vehiculo = $vehiculo->tipo_vehiculo_id;
                    $model->tipo_vehiculo_nombre = $vehiculo->tipoVehiculo->nombre;
                    $model->marca = $vehiculo->modelo->marca->id;
                    $model->marca_nombre = $vehiculo->modelo->marca->nombre;
                    $model->modelo = $vehiculo->modelo->id;
                    $model->modelo_nombre = $vehiculo->modelo->nombre;
                    $model->agno = $vehiculo->agno;
                    $model->motor = $vehiculo->motor;
                    $model->chasis = $vehiculo->chasis;
                    $model->tiene_multas = $vehiculo->tiene_multa;
                    $model->tiene_prenda = $vehiculo->tiene_prenda;
                    $model->tasacion_comercial = $vehiculo->tasacion_comercial;
                    if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[1]) {
                        return $this->render("venta-unica", ["model" => $model]);
                    } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[2]) {
                        //$datos = $this->generateTopicDoc(\app\models\OperacionForm::PERSONERIA);
                        return $this->render("venta-empresa", ["model" => $model, "representante" => new \app\models\RepresentanteForm()]);
                    } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[3]) {
                        //$this->generateTopicDoc(\app\models\OperacionForm::MANDATARIO);
                        return $this->render("venta-mandato", ["model" => $model,]);
                    } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[4]) {
                        return $this->render("venta-sucesion", ["model" => $model, "vendedor" => new \app\models\VendedorForm()]);
                    }
                } else {
                    Yii::$app->session->addFlash("error", "La patente $model->patente ya se encuentra en un proceso de compra-venta.");
                    return $this->redirect(["create", "model" => $model]);
                }
            } else {
                Yii::$app->session->addFlash("error", "No se encontró ningún vehículo con la patente ingresada.");
                return $this->redirect(["create", "model" => $model]);
            }
        }
        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    public function actionAll() {
        $searchModel = new OperacionSearch();
        $fecha_actual = date("d-m-Y");
        $fec_fin = date("d-m-Y", strtotime($fecha_actual . "+ 1 days"));
        $fec_ini = date("d-m-Y", strtotime($fec_fin . "- 7 days"));
        $searchModel->fecha_desde = $fec_ini;
        $searchModel->fecha_hasta = $fec_fin;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $dataProvider->getModels();
        $datos = array();
        foreach ($model as $operacion) {
            $datos[] = [$operacion->id, $operacion->ventaVehiculo->patente, $operacion->venta_fecha, $operacion->ventaPropietario->completeName,
                $operacion->ventaVehiculo->tipoVehiculo->nombre, $operacion->ventaVehiculo->modelo->marca->nombre, $operacion->ventaVehiculo->modelo->nombre,
                $operacion->ventaVehiculo->agno, $operacion->ventaVehiculo->tasacion_comercial, $operacion->venta_precio];
        }

        return $this->render('all', [
                    'searchModel' => $searchModel,
                    'datos' => $datos,
        ]);
    }

    public function actionSaveAndView() {
        $model = new \app\models\OperacionForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $rut = \app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[3] ?
                        $model->mandante_rut : $model->rut;
                $propietario = \app\models\Propietario::find()->where("rut = :rut", [":rut" => $rut])->one();
                if (!isset($propietario)) {
                    $propietario = new \app\models\Propietario();
                    if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[2]) {
                        $propietario->persona_natural = 0;
                    }
                }
                if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[3]) {
                    $propietario->rut = $model->mandante_rut;
                    $propietario->nombres = $model->mandante_nombre;
                    $propietario->apellido_paterno = $model->mandante_apellido_paterno;
                    $propietario->apellido_materno = $model->mandante_apellido_materno;
                    $propietario->direccion = $model->mandante_direccion;
                } else {
                    $propietario->rut = $model->rut;
                    $propietario->nombres = $model->nombre;
                    $propietario->apellido_paterno = $model->apellido_paterno;
                    $propietario->apellido_materno = $model->apellido_materno;
                    $propietario->direccion = $model->direccion;
                }
                if (!$propietario->save()) {
                    throw new Exception("Hubo un problema: " . json_encode($propietario->getFirstErrors()));
                }

                $operacion = new Operacion();
                $operacion->etapa = 1;
                $operacion->venta_fecha = date('Y-m-d H:i:s');
                $operacion->venta_propietario_id = $propietario->id;
                $operacion->venta_user_id = $model->funcionario;
                $operacion->venta_vehiculo_id = $model->vehiculo_id;
                $operacion->venta_precio = $model->precio;
                $operacion->venta_tipo = $model->tipoVenta;
                $operacion->suscriptor_id = \app\models\User::getSuscriptorId();
                if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[3]) {
                    $mandatario = \app\models\Mandatario::find()->where("rut = :rut", [":rut" => $model->mandatario_rut])->one();
                    if (!isset($mandatario)) {
                        $mandatario = new \app\models\Mandatario();
                    }
                    $mandatario->nombres = $model->mandatario_nombre;
                    $mandatario->apellido_paterno = $model->mandatario_apellido_paterno;
                    $mandatario->apellido_materno = $model->mandatario_apellido_materno;
                    $operacion->venta_personeria_mandato = $model->personeria_mandato;
                    if (!$mandatario->save()) {
                        throw new Exception("Hubo un problema: " . json_encode($mandatario->getFirstErrors()));
                    } else {
                        $operacion->venta_mandatario_id = $mandatario->id;
                    }
                } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[2]) {
                    $operacion->venta_personeria_mandato = $model->personeria_mandato;
                }

                $vehiculo = \app\models\Vehiculo::findOne($model->vehiculo_id);
                $vehiculo->tasacion_comercial = $model->tasacion_comercial;
                $vehiculo->tiene_prenda = $model->tiene_prenda;
                $vehiculo->tiene_multa = $model->tiene_multas;
                if (!$vehiculo->save()) {
                    throw new Exception("Hubo un problema: " . json_encode($vehiculo->getFirstErrors()));
                }
                if ($operacion->save()) {
                    $model->ventaId = $operacion->id;
                    if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[2]) {
                        $representante = Yii::$app->request->post("RepresentanteForm");
                        if (isset($representante) && is_array($representante)) {
                            foreach ($representante as $i => $r) {
                                $newRepresentante = \app\models\Representante::find()->where("rut = :rut", [":rut" => $r["rut"]])->one();
                                if (!isset($newRepresentante)) {
                                    $newRepresentante = new \app\models\Representante();
                                }
                                $newRepresentante->rut = $r["rut"];
                                $newRepresentante->nombre = $r["nombre"];
                                $newRepresentante->apellido_paterno = $r["apellido_paterno"];
                                $newRepresentante->apellido_materno = $r["apellido_materno"];
                                $newRepresentante->direccion = $r["direccion"];
                                $newRepresentante->propietario_id = $propietario->id;
                                if (!$newRepresentante->save()) {
                                    throw new Exception("Hubo un problema al asociar los representantes: " . json_encode($newRepresentante->getFirstErrors()));
                                }
                            }
                        }
                    } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[4]) {
                        $vendedor = Yii::$app->request->post("VendedorForm");
                        if (isset($vendedor) && is_array($vendedor)) {
                            foreach ($vendedor as $i => $v) {
                                $ventaPropietario = new \app\models\VentaPropietario();
                                $ventaPropietario->operacion_id = $operacion->id;
                                $newPropietario = \app\models\Propietario::find()->where("rut = :rut", [":rut" => $v["rut"]])->one();
                                if (!isset($newPropietario)) {
                                    $newPropietario = new \app\models\Propietario();
                                }
                                $newPropietario->rut = $v["rut"];
                                $newPropietario->nombres = $v["nombre"];
                                $newPropietario->apellido_paterno = $v["apellido_paterno"];
                                $newPropietario->apellido_materno = $v["apellido_materno"];
                                $newPropietario->direccion = $v["direccion"];
                                if (!$newPropietario->save()) {
                                    throw new Exception("Hubo un problema al guardar los propietarios: " . json_encode($newPropietario->getFirstErrors()));
                                }
                                $ventaPropietario->propietario_id = $newPropietario->id;
                                if (!$ventaPropietario->save()) {
                                    throw new Exception("Hubo un registrar Venta Comunidad: " . json_encode($ventaPropietario->getFirstErrors()));
                                }
                            }
                        }
                    }

                    $transaction->commit();
                    return $this->redirect(["view", "id" => $operacion->id]);
                } else {
                    throw new Exception("Hubo un problema al guardar la operación: " . json_encode($operacion->getFirstErrors()));
                }
            } catch (Exception $ex) {
                $transaction->rollBack();
                Yii::$app->session->addFlash("error", $ex->getMessage());
                return $this->redirect(["create"]);
            }
        } else {
            if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[1]) {
                return $this->render("venta-unica", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[2]) {
                return $this->render("venta-empresa", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[3]) {
                return $this->render("venta-mandato", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_VENTA[$model->tipoVenta] === \app\models\Operacion::TIPO_VENTA[4]) {
                return $this->render("venta-sucesion", ["model" => $model]);
            }
        }
    }

    /**
     * Creates a new Operacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateCompra() {
        $model = new \app\models\OperacionCompraForm();
        if ($model->load(Yii::$app->request->post())) {
            $vehiculo = \app\models\Vehiculo::find()->joinWith(["modelo", "propietario"])->where("patente = :patente", ["patente" => $model->patente])->one();
            if (isset($vehiculo)) {
                if ($operacion = $vehiculo->validateInStage1()) {
                    $propietario = $vehiculo->propietario;
                    $model->operacion_id = $operacion->id;
                    $model->vehiculo_id = $vehiculo->id;
                    $model->rut = $propietario->rut;
                    $model->nombre = $propietario->nombres;
                    $model->apellido_paterno = $propietario->apellido_paterno;
                    $model->apellido_materno = $propietario->apellido_materno;
                    $model->direccion = $propietario->direccion;
                    $model->persona_natural = $propietario->persona_natural;
                    $model->funcionario = Yii::$app->user->id;
                    $model->tipo_vehiculo = $vehiculo->tipo_vehiculo_id;
                    $model->tipo_vehiculo_nombre = $vehiculo->tipoVehiculo->nombre;
                    $model->marca = $vehiculo->modelo->marca->id;
                    $model->marca_nombre = $vehiculo->modelo->marca->nombre;
                    $model->modelo = $vehiculo->modelo->id;
                    $model->modelo_nombre = $vehiculo->modelo->nombre;
                    $model->agno = $vehiculo->agno;
                    $model->motor = $vehiculo->motor;
                    $model->chasis = $vehiculo->chasis;
                    $model->tiene_multas = $vehiculo->tiene_multa;
                    $model->tiene_prenda = $vehiculo->tiene_prenda;
                    $model->tasacion_comercial = $vehiculo->tasacion_comercial;
                    $model->precio_etapa_1 = $operacion->venta_precio;
                    if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[1]) {
                        return $this->render("compra-unica", ["model" => $model]);
                    } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[2]) {
                        return $this->render("compra-empresa", ["model" => $model, "representante" => new \app\models\RepresentanteForm()]);
                    } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[3]) {
                        return $this->render("compra-para", ["model" => $model]);
                    } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[4]) {
                        return $this->render("compra-sucesion", ["model" => $model, "comprador" => new \app\models\CompradorForm()]);
                    }
                } else {
                    Yii::$app->session->addFlash("error", "Vehículo con patente $model->patente no se encuentra en proceso Etapa 1.");
                    return $this->redirect(["create-compra", "model" => $model]);
                }
            } else {
                Yii::$app->session->addFlash("error", "No se encontró ningún vehículo con la patente ingresada.");
                return $this->redirect(["create-compra", "model" => $model]);
            }
        }
        return $this->render('index-compra', [
                    'model' => $model,
        ]);
    }

    public function actionSaveAndViewCompra() {
        $model = new \app\models\OperacionCompraForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $vehiculo = \app\models\Vehiculo::findOne($model->vehiculo_id);
                if ($operacion = $vehiculo->validateInStage1()) {
                    $comprador = \app\models\Propietario::find()->where("rut = :rut", [":rut" => $model->comprador_rut])->one();
                    if (!isset($comprador)) {
                        $comprador = new \app\models\Propietario();
                        if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[2]) {
                            $comprador->persona_natural = 0;
                        }
                    }
                    $comprador->rut = $model->comprador_rut;
                    $comprador->nombres = $model->comprador_nombre;
                    $comprador->apellido_paterno = $model->comprador_apellido_paterno;
                    $comprador->apellido_materno = $model->comprador_apellido_materno;
                    $comprador->direccion = $model->comprador_direccion;
                    if (!$comprador->save()) {
                        throw new Exception("Hubo un problema al guardar el comprador: " . json_encode($comprador->getFirstErrors()));
                    }

                    $operacion->etapa = 2;
                    $operacion->compra_fecha = date('Y-m-d H:i:s');
                    $operacion->compra_propietario_id = $comprador->id;
                    $operacion->compra_user_id = $model->funcionario;
                    $operacion->venta_vehiculo_id = $model->vehiculo_id;
                    $operacion->compra_precio = $model->precio_etapa_2;
                    $operacion->compra_tipo = $model->tipoCompra;

                    if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[3]) {
                        $propietarioPara = \app\models\Propietario::find()->where("rut = :rut", [":rut" => $model->rut_para])->one();
                        if (!isset($propietarioPara)) {
                            $propietarioPara = new \app\models\Propietario();
                        }
                        $propietarioPara->rut = $model->rut_para;
                        $propietarioPara->nombres = $model->nombre_para;
                        $propietarioPara->apellido_paterno = $model->apellido_paterno_para;
                        $propietarioPara->apellido_materno = $model->apellido_materno_para;
                        $propietarioPara->direccion = $model->direccion_para;
                        if (!$propietarioPara->save()) {
                            throw new Exception("Hubo un problema al registrador comprador para: " . json_encode($propietarioPara->getFirstErrors()));
                        } else {
                            $operacion->compra_para_id = $propietarioPara->id;
                        }
                    } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[2]) {
                        $operacion->compra_personeria = $model->personeria_mandato;
                    }

                    $vehiculo->tasacion_comercial = $model->tasacion_comercial;
                    $vehiculo->tiene_prenda = $model->tiene_prenda;
                    $vehiculo->tiene_multa = $model->tiene_multas;
                    if (!$vehiculo->save()) {
                        throw new Exception("Hubo un problema: " . json_encode($vehiculo->getFirstErrors()));
                    }
                    if ($operacion->save()) {
                        $model->operacion_id = $operacion->id;
                        if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[2]) {
                            $representante = Yii::$app->request->post("RepresentanteForm");
                            if (isset($representante) && is_array($representante)) {
                                foreach ($representante as $i => $r) {
                                    $newRepresentante = \app\models\Representante::find()->where("rut = :rut", [":rut" => $r["rut"]])->one();
                                    if (!isset($newRepresentante)) {
                                        $newRepresentante = new \app\models\Representante();
                                    }
                                    $newRepresentante->rut = $r["rut"];
                                    $newRepresentante->nombre = $r["nombre"];
                                    $newRepresentante->apellido_paterno = $r["apellido_paterno"];
                                    $newRepresentante->apellido_materno = $r["apellido_materno"];
                                    $newRepresentante->direccion = $r["direccion"];
                                    $newRepresentante->propietario_id = $comprador->id;
                                    if (!$newRepresentante->save()) {
                                        throw new Exception("Hubo un problema al asociar los representantes: " . json_encode($newRepresentante->getFirstErrors()));
                                    }
                                }
                            }
                        } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[4]) {
                            $comprador = Yii::$app->request->post("CompradorForm");
                            if (isset($comprador) && is_array($comprador)) {
                                foreach ($comprador as $i => $v) {
                                    $compraPropietario = new \app\models\CompraPropietario();
                                    $compraPropietario->operacion_id = $operacion->id;
                                    $newPropietario = \app\models\Propietario::find()->where("rut = :rut", [":rut" => $v["rut"]])->one();
                                    if (!isset($newPropietario)) {
                                        $newPropietario = new \app\models\Propietario();
                                        $newPropietario->rut = $v["rut"];
                                        $newPropietario->nombres = $v["nombre"];
                                        $newPropietario->apellido_paterno = $v["apellido_paterno"];
                                        $newPropietario->apellido_materno = $v["apellido_materno"];
                                        $newPropietario->direccion = $v["direccion"];
                                        if (!$newPropietario->save()) {
                                            throw new Exception("Hubo un problema al guardar los propietarios: " . json_encode($newPropietario->getFirstErrors()));
                                        }
                                    }
                                    $compraPropietario->propietario_id = $newPropietario->id;
                                    if (!$compraPropietario->save()) {
                                        throw new Exception("Hubo un problema al registrar Compra Comunidad: " . json_encode($compraPropietario->getFirstErrors()));
                                    }
                                }
                            }
                        }
                        $transaction->commit();
                        return $this->redirect(["view", "id" => $operacion->id]);
                    } else {
                        throw new Exception("Hubo un problema al guardar la operación: " . json_encode($operacion->getFirstErrors()));
                    }
                } else {
                    throw new Exception("Esta patente no está asociada a una primera etapa: " . json_encode($operacion->getFirstErrors()));
                }
            } catch (Exception $ex) {
                $transaction->rollBack();
                Yii::$app->session->addFlash("error", $ex->getMessage());
                return $this->redirect(["create-compra"]);
            }
        } else {
            if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[1]) {
                return $this->render("compra-unica", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[2]) {
                return $this->render("compra-empresa", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[3]) {
                return $this->render("compra-mandato", ["model" => $model]);
            } else if (\app\models\Operacion::TIPO_COMPRA[$model->tipoCompra] === \app\models\Operacion::TIPO_COMPRA[4]) {
                return $this->render("compra-sucesion", ["model" => $model]);
            }
        }
    }

    public function actionDocAnexa($id) {
        $model = new \app\models\OperacionAnexosForm();
        $operacionModel = $this->findModel($id);
        $datos = array();
        foreach ($model->search($id)->getModels() as $files) {
            $datos[] = [$id, $files["description"], $files["url"], $files["user"]["nombre"] . " " . $files["user"]["apellido"]];
        }

        return $this->render("doc-anexa", ["model" => $model, "operacionModel" => $operacionModel, "datos" => $datos]);
    }

    public function actionAttachDocument($id, $action) {
        $model = new \app\models\OperacionAnexosForm();
        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
            if ($action == "cav") {
                $model->description = "CAV";
            } else if ($action == "multas") {
                $model->description = "CERTIFICADO_MULTAS";
            } else if ($action == "cedula") {
                $model->description = "CEDULA_INDENTIDAD";
            } else if ($action == "permiso") {
                $model->description = "PERMISO_CIRCULACIÓN";
            } else if ($action == "personeria") {
                $model->description = "PERSONERÍA";
            } else if ($action == "mandato") {
                $model->description = "MANDATO";
            }
            if ($model->saveDocument($id)) {
//                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                Yii::$app->getSession()->setFlash('success', 'Documento cargado exitosamente');
//                return \yii\helpers\Json::encode(['output' => '', 'message' => 'OK', 'success' => true]);
                return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
            } else {
                Yii::$app->getSession()->setFlash('error', 'Imposible guardar el archivo asociado. Intente más tarde');
                return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
            }
        } else {
            return $this->renderAjax("attach-document", ["model" => $model, "id" => $id]);
        }
    }

    public function actionAttachedView($url) {
        $url_explode = explode(DIRECTORY_SEPARATOR, $url);

        $id = $url_explode[2];
        $archivo = $url_explode[3];

        $desc_begin = strrpos($archivo, DIRECTORY_SEPARATOR) + 1;

        $fileName = substr($archivo, $desc_begin);
        $description = substr($fileName, strpos($fileName, '_') + 1);

        if (file_exists($url)) {
            return Yii::$app->response->sendFile($url, $description);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Imposible descargar el documento adjunto. Intente más tarde');
            return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
        }
    }

    /**
     * Permite eliminar un documento adjunto asociado a la escritura
     * 
     * @param string $url Ruta asociada al documento
     * @return mixed
     */
    public function actionAttachedDelete($url) {
        $url_explode = explode(DIRECTORY_SEPARATOR, $url);
        $id = $url_explode[2];
        if (unlink($url)) {
            Yii::$app->getSession()->setFlash('success', 'Documento adjunto eliminado exitosamente');
            return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Imposible eliminar el documento adjunto. Intente más tarde');
            return $this->redirect(['doc-anexa', "id" => $id, "action" => null]);
        }
    }

    /**
     * Displays a single Operacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $operacion = $this->findModel($id);
        $ventaPropietario = \app\models\VentaPropietario::find()->where("operacion_id = :ope", [":ope" => $id])->all();
        $compraPropietario = \app\models\CompraPropietario::find()->where("operacion_id = :ope", [":ope" => $id])->all();
        $representantesVenta = \app\models\Representante::find()->where("propietario_id = :pId", [":pId" => $operacion->venta_propietario_id])->all();
        $representantesCompra = \app\models\Representante::find()->where("propietario_id = :pId", [":pId" => $operacion->compra_propietario_id])->all();
        return $this->render('view', [
                    'model' => $operacion,
                    'ventaPropietario' => $ventaPropietario,
                    'compraPropietario' => $compraPropietario,
                    'representantesVenta' => $representantesVenta,
                    'representantesCompra' => $representantesCompra
        ]);
    }

    private function generateTopicDoc($doc_venta) {
        $sucursal_id = \Yii::$app->user->getIdentity()->sucursal_id;
        $path = \app\models\OperacionForm::PATH . $sucursal_id . DIRECTORY_SEPARATOR . $sucursal_id . '_' . $doc_venta . '.*';
        $file_array = glob($path);
        if (empty($file_array)) {
            \Yii::$app->getSession()->setFlash('warning', 'La materia no posee un documento matriz para generar el archivo');
            return $this->redirect(['create']);
        }

        $inverse_file_array = array_reverse($file_array);
        $source = array_pop($inverse_file_array);

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($source);
        $pre_model_elements = $templateProcessor->getVariables();
        $model_elements = [];
        foreach ($pre_model_elements as $value) {
            $model_elements[] = strtolower($value);
        }

        $model = new \yii\base\DynamicModel($model_elements);
        $model->addRule($model_elements, 'required');

        $rut_attributes = [];
        foreach ($model_elements as $value) {
            if (strpos(strtolower($value), 'rut') !== false || strpos(strtolower($value), 'run') !== false || strpos(strtolower($value), 'cedula_identidad') !== false) {
                $rut_attributes[] = $value;
            }
        }
        if (!empty($rut_attributes)) {
            $model->addRule($rut_attributes, \app\components\RutValidator::className());
        }
        /*
          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          $formatted_attributes = \app\components\FormFieldGenerator::formatAttributes($model->attributes);
          $values = array_values($formatted_attributes);
          $keys = array_keys($formatted_attributes);
          $templateProcessor->setValue($keys, $values);


          $path = \app\models\OperacionForm::GENERATED_PATH . $sucursal_id . DIRECTORY_SEPARATOR;
          $filename = $materia->materiaPrivada->nombre . '.docx';
          $fullname = $path . $filename;
          if (!file_exists($path)) {
          mkdir($path, 0777, true);
          }
          if (file_exists($fullname)) {
          unlink($fullname);
          }
          $templateProcessor->saveAs($fullname);

          // We'll be outputting an excel file
          //header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
          // It will be called file.xls
          //header('Content-Disposition: attachment; filename="' . $filename);
          // Write file to the browser
          // readfile($fullname);
          return \Yii::$app->response->sendFile($fullname);
          } else { */
        return ['model' => $model, 'model_elements' => $model_elements];
        //}
    }

    /**
     * Updates an existing Operacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Operacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['create']);
    }

    public function actionValidation() {
        // if "id" is not null(meaning the validation comes from update action), 
        // we will query the data from that id.
        // otherwise, the validaiton will perform for insert new record action.
        $model = new \app\models\OperacionForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionCompraValidation() {
        // if "id" is not null(meaning the validation comes from update action), 
        // we will query the data from that id.
        // otherwise, the validaiton will perform for insert new record action.
        $model = new \app\models\OperacionCompraForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    /**
     * Finds the Operacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Operacion::find()->joinWith(["ventaPropietario", "ventaUser", "ventaVehiculo", "ventaMandatario"])
                        ->where("operacion.id = :id", [":id" => $id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
