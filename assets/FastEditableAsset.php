<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FastEditableAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/fast-editable.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
