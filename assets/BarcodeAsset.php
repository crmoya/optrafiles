<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class BarcodeAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/jquery.scannerdetection.js',
        'js/jquery.scannerdetection.compatibility.js',
        'js/barcode.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
