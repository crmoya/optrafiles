<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class Select2Asset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/select2.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
