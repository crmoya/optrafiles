<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i> Matrices</h3>
    </div>
    <div class="panel-body">
        <?php
        $cabeceras = [
//            [
//                'name' => 'id',
//                'width' => 'small',
//                'visible' => false,
//                'exportable' => false,
//                'param1' => '',
//            ],
            [
                'name' => 'Descripción',
                'width' => 'big',
            ],
            [
                'name' => 'URL',
                'width' => 'small',
                'visible' => false,
                'exportable' => false,
                'param2' => 'url'
            ],
        ];
        $table = app\components\TableHelper::createTable($cabeceras, $datos, $this, NULL, ["render" => true, "columnIndexOrder" => 0,
                        "columnOrderType" => "asc",
                        "actions" => [["type" => "download", "url" => "/matriz-tipo-venta/attached-view"],
                            ["type" => "delete", "url" => "/matriz-tipo-venta/attached-delete"]]
            ]);
        ?>
    </div>
</div>
