<?php

use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

app\assets\FileInputAsset::register($this);
?>

<?= \dmstr\widgets\Alert::widget() ?>
<?php
$form = ActiveForm::begin([
            'id' => 'attach-document-form',
            //'type' => ActiveForm::TYPE_HORIZONTAL,
            /*'fieldConfig' => [
                'template' => '<div class="col-md-3 text-right">{label}</div>
                                        <div class="col-md-9">{input}{error}</div>',
            ],*/
            'options' => ['enctype' => 'multipart/form-data']
        ]);
?>
<?=
$form->field($model, 'file')->widget(kartik\file\FileInput::classname(), [
    'options' => ['accept' => 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
    'pluginOptions' => ['allowedFileExtensions' => app\models\Operacion::FILE_EXTENSION
]]);
/*
  $form->field($model, 'file')->widget(kartik\file\FileInput::classname(), [
  'options' => [
  //'accept' => 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'
  ],
  // CAMBIAR VALORES PARA QUE SOPORTE MAYOR ENVÍO DE DATOS EN PHP.INI -> upload_max_filesize Y post_max_size
  'pluginOptions' => [
  //'allowedFileExtensions' => ['doc', 'docx'],
  'maxFileCount' => 1,
  'maxFileSize' => 200000,
  'uploadUrl' => yii\helpers\Url::to(['/ot-documento/attach-document', 'id' => $id]),
  'uploadExtraData' => new \yii\web\JsExpression('function (previewId, index) {
  return {
  description:  $("#otattachform-description").val();
  };
  }'),
  ],
  ]);
 */
?>

<?php ActiveForm::end(); ?>