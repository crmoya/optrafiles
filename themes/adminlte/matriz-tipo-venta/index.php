<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MarcaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Matrices para Tipos de Venta';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?php
    $form = yii\widgets\ActiveForm::begin([
                'class' => 'form-horizontal',
                'fieldConfig' => [
                    'template' => '<div class="col-sm-3 text-right">{label}</div>
                                        <div class="col-sm-9">{input}{error}</div>',
                ],
    ]);
    ?>

    <div class="box-body">
        <div class="col col-sm-6">
            <label for="personeria">Documento Matriz para Personería</label>
            <?= Html::button('<i class="fa fa-file-word-o"></i> Subir Documento', ['class' => 'btn btn-default pull-right', "id" => "personeria"]) ?>
        </div>
        <div class="col col-sm-6">
            <?= Html::button('<i class="fa fa-file-word-o"></i> Subir Documento', ['class' => 'btn btn-default pull-right', "id" => "mandatario"]) ?>
            <label for="mandatario">Documento Matriz para Mandatario</label>
        </div>
    </div>
    <div class="box-footer" >
    </div>

    <?php yii\widgets\ActiveForm::end(); ?>

</div>

<?= $this->render('_attachedDocuments', ['model' => $model, "datos" => $datos]) ?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title"><i class="fa fa-file-word-o"></i> Subir Documento</h3>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div id='modalContent'>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS
        $(document).ready(function() {
            $("button").click(function() {
                action = $(this).attr("id");
                $("#myModal").find("#modalContent").html("");
                $("#myModal").find(".modal-title").text("");
                $("#myModal").modal("show");
                $.ajax({
                        url: "attach-document",
                        data: {action: action},
                        //type: "post",
                        success: function(respuesta) {
                                if (respuesta != null && respuesta == "OK") {
                                    $("#myModal").modal('toggle');
                                } else {
                                    $("#myModal").find("#modalContent").html(respuesta);
                                    $("#myModal").find(".modal-title").text($("#titulo").val());
                                }
                        },
                        error: function() {
                                $("#myModal").find("#modalContent").html("ERROR");
                        }
                });
            });
        
        });
JS;
$this->registerJs($script);
?>