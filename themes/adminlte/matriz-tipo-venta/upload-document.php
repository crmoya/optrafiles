<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MateriaPublica */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$form = ActiveForm::begin([
            'id' => 'create-escritura-form',
            'options' => ['enctype' => 'multipart/form-data']
        ]);
?>

<?=

$form->field($model, 'file')->widget(\kartik\file\FileInput::classname(), [
    'options' => ['accept' => 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
    'pluginOptions' => [
        'allowedFileExtensions' => [app\models\Operacion::FILE_EXTENSION],
        //'allowedFileExtensions' => ['doc', 'docx'],
        'maxFileCount' => 1,
        'maxFileSize' => 200000,
        'uploadUrl' => yii\helpers\Url::to(['/matriz-tipo-venta/upload-document', 'id' => $model->id]),
        'uploadExtraData' => new \yii\web\JsExpression('function (previewId, index) {
                    return {
                        //description:  $("#otattachform-description").val()
                    };
                }'),
]]);
?>

<?php ActiveForm::end(); ?>