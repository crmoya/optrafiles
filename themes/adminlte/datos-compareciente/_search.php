<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DatosComparecienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="datos-compareciente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'huella') ?>

    <?= $form->field($model, 'template') ?>

    <?= $form->field($model, 'template_size') ?>

    <?= $form->field($model, 'imagen') ?>

    <?php // echo $form->field($model, 'compareciente_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
