<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DatosCompareciente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="datos-compareciente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'huella')->textInput() ?>

    <?= $form->field($model, 'template')->textInput() ?>

    <?= $form->field($model, 'template_size')->textInput() ?>

    <?= $form->field($model, 'imagen')->textInput() ?>

    <?= $form->field($model, 'compareciente_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
