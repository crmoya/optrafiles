<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DatosCompareciente */

$this->title = 'Crear Datos Compareciente';
$this->params['breadcrumbs'][] = ['label' => 'Datos Comparecientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datos-compareciente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
