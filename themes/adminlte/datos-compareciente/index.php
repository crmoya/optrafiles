<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DatosComparecienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Datos Comparecientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="datos-compareciente-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Datos Compareciente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'huella',
            'template',
            'template_size',
            'imagen',
            //'compareciente_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
