<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DatosCompareciente */

$this->title = 'Editar Datos Compareciente: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Datos Comparecientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="datos-compareciente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
