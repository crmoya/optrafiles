<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehiculo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'patente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chasis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_motor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agno')->textInput() ?>

    <?= $form->field($model, 'tasacion_comercial')->textInput() ?>

    <?= $form->field($model, 'modelo_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
