<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VehiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehículos Ingresados';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Vehículos</h3>
    </div>
    <div class="panel-body">
        <?php
        $cabeceras = [
            [
                'name' => 'id',
                'width' => 'small',
                'visible' => false,
                'exportable' => false,
            ],
            [
                'name' => 'Patente',
                'width' => 'small'
            ],
            [
                'name' => 'Vendedor',
                'width' => 'big'
            ],
            [
                'name' => 'Tipo Vehículo',
                'width' => 'medium'
            ],
            [
                'name' => 'Marca',
                'width' => 'medium'
            ],
            [
                'name' => 'Modelo',
                'width' => 'medium'
            ],
            [
                'name' => 'Año',
                'width' => 'small'
            ],
            [
                'name' => 'Tasación Comercial',
                'width' => 'medium'
            ],
        ];
        $table = app\components\TableHelper::createTable($cabeceras, $datos, $this, NULL, []);
        ?>
    </div>
</div>