<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Parámetros de Búsqueda</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <?php
    $form = yii\widgets\ActiveForm::begin([
                'action' => ['all'],
                'method' => 'get',
    ]);
    ?>

    <div class="box-body">
        <div class="col-sm-3">
            <?php
            echo $form->field($model, 'fecha_desde')->input("text", ["class" => 'datepicker form-control']);
            ?>
        </div>
        <div class="col-sm-3">
            <?php
            echo $form->field($model, 'fecha_hasta')->input("text", ["class" => 'datepicker form-control']);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'patente')->input("text", ["class" => 'uppercase form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'repertorio')->input("text", ["class" => 'uppercase form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'compareciente')->input("text", ["class" => 'uppercase form-control']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'estado')->input("text", ["class" => 'uppercase form-control']) ?>
        </div>
        <div class="col-sm-3 box-footer" style="padding-top: 24px;">
            <?= \yii\helpers\Html::submitButton('Búsqueda', ['class' => 'btn btn-primary']) ?>
            <?php //kartik\helpers\Html::a('Reiniciar', ['busqueda/ot'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php yii\widgets\ActiveForm::end(); ?>

</div>