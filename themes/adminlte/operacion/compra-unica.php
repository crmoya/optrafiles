<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\RutAsset;

RutAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\VehiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Compra Única';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'create-compra-form',
                'action' => ['operacion/save-and-view-compra'],
                'validationUrl' => 'compra-validation',
                'enableAjaxValidation' => true,
                'fieldConfig' => [
                    'template' => '<div class="col-md-3 text-right">{label}</div>
                                        <div class="col-md-9">{input}{error}</div>',
                ]
    ]);
    ?>
    <div class="box-body">
        <?= Html::activeHiddenInput($model, 'vehiculo_id') ?>
        <?= Html::activeHiddenInput($model, 'tipoCompra') ?>
        <?= $form->field($model, 'patente')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('patente')]) ?>
        <?= $form->field($model, 'rut')->input('text', ['class' => 'uppercase optional-rut form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('rut')]) ?>
        <?= $form->field($model, 'nombre')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('nombre')]) ?>
        <?php
            if ($model->persona_natural != 0) {
                echo $form->field($model, 'apellido_paterno')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('razon_social')]);
                echo $form->field($model, 'apellido_materno')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('razon_social')]);
            }
        ?>
        <?= $form->field($model, 'direccion')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('direccion')]) ?>
        <?=
        $form->field($model, 'funcionario')->dropDownList(
                \yii\helpers\ArrayHelper::map(app\models\User::find()->select(["id", new \yii\db\Expression("CONCAT(nombre, ' ', apellido) as nombre")])->
                                where("id = :id", [":id" => $model->funcionario])->all(), "id", "nombre"), ['class' => 'uppercase form-control', 'style' => 'width:400px', 'options' => [$model->funcionario => ["Selected" => true]]])
        ?>
        <?= $form->field($model, 'tipo_vehiculo')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
            'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->tipo_vehiculo_nombre])
        ?>
        <?= $form->field($model, 'marca')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
            'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->marca_nombre])
        ?>
        <?= $form->field($model, 'modelo')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
            'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->modelo_nombre])
        ?>
        <?= $form->field($model, 'agno')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('agno')]) ?>
        <?= $form->field($model, 'motor')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('motor')]) ?>
        <?= $form->field($model, 'chasis')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('chasis')]) ?>
        <?= $form->field($model, 'tasacion_comercial')->input('number', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('tasacion_comercial')]) ?>
        <?= $form->field($model, 'precio_etapa_1')->input('number', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('precio')]) ?>

        <?= $form->field($model, 'comprador_rut')->input('text', ['class' => 'uppercase optional-rut form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('rut')]) ?>
        <?= $form->field($model, 'comprador_nombre')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('nombre')]) ?>
        <?= $form->field($model, 'comprador_apellido_paterno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('apellido_paterno')]) ?>
        <?= $form->field($model, 'comprador_apellido_materno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('apellido_materno')]) ?>
<?= $form->field($model, 'comprador_direccion')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('direccion')]) ?>

            <?= $form->field($model, 'precio_etapa_2')->input('number', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('precio')]) ?>

        <div class="custom-control custom-checkbox">
<?= $form->field($model, 'tiene_prenda')->checkbox(["class" => "custom-control-input"]) ?>
        <?= $form->field($model, 'tiene_multas')->checkbox(["class" => "custom-control-input"]) ?>
        </div>
    </div>
    <div class="box-footer" >
    <?= Html::submitButton('Guardar Compra', ['class' => 'btn btn-success pull-left']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
