<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OperacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Generación Compra';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'create-venta-form',
                'fieldConfig' => [
                    'template' => '<div class="col-md-3 text-right">{label}</div>
                                        <div class="col-md-9">{input}{error}</div>',
                ]
    ]);
    ?>
    <div class="box-body">
        <?= $form->field($model, 'patente')->input('text', ["class" => "uppercase form-control", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('patente')]) ?>
        <?= $form->field($model, 'tipoCompra')->dropDownList(app\models\Operacion::TIPO_COMPRA, ['style' => 'width:400px', "id" => "tipoCompra"]); ?>
        <div id="cantidad" style="display: none;">
            <?= $form->field($model, 'cantidad_sucesion')->input("number", ['style' => 'width:400px', 'id' => 'cantidad']); ?>
        </div>
        <?php //$form->field($model, 'impuesto')->input('number', ['style'=>'width:200px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('impuesto')]) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Generar Compra', ['class' => 'btn btn-success pull-left']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
$(document).ready(function() {
        $("#tipoCompra").on("change", function() {
            tipoCompra = $(this).children("option:selected").val();
            if (tipoCompra == 4 || tipoCompra == 2) {
                texto = tipoCompra == 2 ? "CANTIDAD REPRESENTANTES" : "CANTIDAD VENDEDORES";
                $("label[for=cantidad]").html(texto);
                $('#cantidad').css("display", "block");
            } else {
                $('#cantidad').css("display", "none");
            }
        });
});
JS;

$this->registerJs($script);
?>
