<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OperacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>

<?=
$this->render('_all', [
    'model' => $searchModel
])
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Ventas</h3>
    </div>
    <div class="panel-body">
        <?php
        $cabeceras = [
            [
                'name' => 'id',
                'width' => 'small',
                'visible' => false,
                'exportable' => false,
            ],
            [
                'name' => 'Patente',
                'width' => 'medium'
            ],
            [
                'name' => 'Fecha venta',
                'width' => 'small',
                'format' => 'date'
            ],
            [
                'name' => 'Vendedor',
                'width' => 'medium'
            ],
            [
                'name' => 'Tipo Vehículo',
                'width' => 'medium'
            ],
            [
                'name' => 'Marca',
                'width' => 'medium'
            ],
            [
                'name' => 'Modelo',
                'width' => 'medium'
            ],
            [
                'name' => 'Año',
                'width' => 'small'
            ],
            [
                'name' => 'Tasación Comercial',
                'width' => 'medium'
            ],
            [
                'name' => 'Precio Compra',
                'width' => 'medium'
            ],
        ];
        $table = app\components\TableHelper::createTable($cabeceras, $datos, $this, "operacion/view", ["columnIndexOrder" => 2]);
        ?>
    </div>
</div>