<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Documentos anexos</h3>
    </div>
    <div class="panel-body">
        <?php
        $cabeceras = [
            [
                'name' => 'id',
                'width' => 'small',
                'visible' => false,
                'exportable' => false,
            ],
            [
                'name' => 'Descripción',
                'width' => 'big',
            ],
            [
                'name' => 'URL',
                'width' => 'small',
                'visible' => false,
                'exportable' => false,
                'param1' => 'url'
            ],
            [
                'name' => 'Usuario',
                'width' => 'big',
            ],
        ];
        $table = app\components\TableHelper::createTable($cabeceras, $datos, $this, NULL, ["columnIndexOrder" => 1,
                "actions" => [["type" => "download", "url" => "attached-view"], ["type" => "delete", "url" => "attached-delete"]]]);
        ?>
    </div>
</div>
