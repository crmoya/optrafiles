<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\RutAsset;

RutAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\VehiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Venta Empresa';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'create-venta-form',
                'action' => ['operacion/save-and-view'],
                'validationUrl' => 'validation',
                'enableAjaxValidation' => true,
                'fieldConfig' => [
                    'template' => '<div class="col-md-4 text-right">{label}</div>
                                        <div class="col-md-8">{input}{error}</div>',
                ]
    ]);
    ?>
    <div class="box-body">
        <div class="col col-md-8">
            <?= Html::activeHiddenInput($model, 'vehiculo_id') ?>
            <?= Html::activeHiddenInput($model, 'tipoVenta') ?>
            <?= Html::activeHiddenInput($model, 'ventaId') ?>
            <?= $form->field($model, 'patente')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('patente')]) ?>
            <?= $form->field($model, 'rut')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('rut')]) ?>
            <?= $form->field($model, 'nombre')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('razon_social')]) ?>
            <?= $form->field($model, 'direccion')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('direccion')]) ?>
            <?php for ($i = 0; $i < $model->cantidad_sucesion; $i++) { ?>
                <h2>Representante <?= $i + 1 ?></h2>
                <?= $form->field($representante, '[' . $i . ']rut')->input('text', ['class' => 'uppercase optional-rut form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('rut')]) ?>
                <?= $form->field($representante, '[' . $i . ']nombre')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('nombre')]) ?>
                <?= $form->field($representante, '[' . $i . ']apellido_paterno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('apellido_paterno')]) ?>
                <?= $form->field($representante, '[' . $i . ']apellido_materno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('apellido_materno')]) ?>
                <?= $form->field($representante, '[' . $i . ']direccion')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('direccion')]) ?>
            <?php } ?>
            <?=
            $form->field($model, 'funcionario')->dropDownList(
                    \yii\helpers\ArrayHelper::map(app\models\User::find()->select(["id", new \yii\db\Expression("CONCAT(nombre, ' ', apellido) as nombre")])->
                                    where("id = :id", [":id" => $model->funcionario])->all(), "id", "nombre"), ['class' => 'uppercase form-control', 'style' => 'width:400px', 'options' => [$model->funcionario => ["Selected" => true]]])
            ?>
            <?=
            $form->field($model, 'tipo_vehiculo')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
                'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->tipo_vehiculo_nombre])
            ?>
            <?=
            $form->field($model, 'marca')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
                'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->marca_nombre])
            ?>
            <?=
            $form->field($model, 'modelo')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
                'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->modelo_nombre])
            ?>
            <?= $form->field($model, 'agno')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('agno')]) ?>
            <?= $form->field($model, 'motor')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('motor')]) ?>
            <?= $form->field($model, 'chasis')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('chasis')]) ?>
                <?= $form->field($model, 'tasacion_comercial')->input('number', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('tasacion_comercial')]) ?>
                <?= $form->field($model, 'precio')->input('number', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('precio')]) ?>
                <?= $form->field($model, 'personeria_mandato')->textarea(['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => ""]) ?>
            <div class="custom-control custom-checkbox">
<?= $form->field($model, 'tiene_prenda')->checkbox(["class" => "custom-control-input"]) ?>
<?= $form->field($model, 'tiene_multas')->checkbox(["class" => "custom-control-input"]) ?>
            </div>

            <!--
            <div class="form-group field-operacionform-precio">
                <div class="col-md-12 text-left"><h2 style="padding: 20px 40px">PERSONERÍA</h2></div>
            </div>
            -->
            <!-- NUNCA FUE EL FORMULARIO DINÁMICO !! PERDÍ TIEMPO EN ESTO!! = ( -->
            <?php
            /*
              foreach ($model_elements as $model_element) {
              echo \app\components\FormFieldGenerator::generateField($form, $dynamicModel, strtolower($model_element));
              }
             */
            ?>
        </div>
    </div>
    <div class="box-footer" >
    <?= Html::submitButton('Guardar Venta', ['class' => 'btn btn-success pull-left']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
$(document).ready( function () {
        $(".datepicker").datepicker();
});
JS;

$this->registerJs($script);
?>
