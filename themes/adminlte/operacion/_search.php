<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'venta_fecha') ?>

    <?= $form->field($model, 'venta_propietario_id') ?>

    <?= $form->field($model, 'venta_direccion') ?>

    <?= $form->field($model, 'venta_precio') ?>

    <?php // echo $form->field($model, 'venta_tipo') ?>

    <?php // echo $form->field($model, 'venta_user_id') ?>

    <?php // echo $form->field($model, 'venta_mandatario_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
