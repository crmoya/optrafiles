<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\RutAsset;

RutAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\VehiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Venta Representación Mandato';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'create-venta-form',
                'action' => ['operacion/save-and-view'],
                'fieldConfig' => [
                    'template' => '<div class="col-md-4 text-right">{label}</div>
                                        <div class="col-md-8">{input}{error}</div>',
                ]
    ]);
    ?>
    <div class="box-body">
        <?= Html::activeHiddenInput($model, 'vehiculo_id') ?>
        <?= Html::activeHiddenInput($model, 'tipoVenta') ?>
        <?= Html::activeHiddenInput($model, 'ventaId') ?>
        <?= $form->field($model, 'patente')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('patente')]) ?>
        <?= $form->field($model, 'mandante_rut')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandante_rut')]) ?>
        <?= $form->field($model, 'mandante_nombre')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandante_nombre')]) ?>
        <?php
            if ($model->persona_natural != 0) {
                echo $form->field($model, 'mandante_apellido_paterno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandante_apellido_paterno')]);
                echo $form->field($model, 'mandante_apellido_materno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandante_apellido_materno')]);
            }
        ?>
        <?= $form->field($model, 'mandante_direccion')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandante_direccion')]) ?>
        <?=
        $form->field($model, 'funcionario')->dropDownList(
                \yii\helpers\ArrayHelper::map(app\models\User::find()->select(["id", new \yii\db\Expression("CONCAT(nombre, ' ', apellido) as nombre")])->
                                where("id = :id", [":id" => $model->funcionario])->all(), "id", "nombre"), ['class' => 'uppercase form-control', 'style' => 'width:400px', 'options' => [$model->funcionario => ["Selected" => true]]])
        ?>
        <?= $form->field($model, 'tipo_vehiculo')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
            'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->tipo_vehiculo_nombre])
        ?>
        <?= $form->field($model, 'marca')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
            'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->marca_nombre])
        ?>
        <?= $form->field($model, 'modelo')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px',
            'min' => 0, 'placeholder' => $model->getAttributeLabel('agno'), 'value' => $model->modelo_nombre])
        ?>
        <?= $form->field($model, 'agno')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('agno')]) ?>
        <?= $form->field($model, 'motor')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('motor')]) ?>
        <?= $form->field($model, 'chasis')->input('text', ['class' => 'uppercase form-control', "readonly" => "readonly", 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('chasis')]) ?>
        <?= $form->field($model, 'tasacion_comercial')->input('number', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('tasacion_comercial')]) ?>
        <?= $form->field($model, 'precio')->input('number', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('precio')]) ?>

        <?= $form->field($model, 'mandatario_rut')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandatario_rut')]) ?>
        <?= $form->field($model, 'mandatario_nombre')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandatario_nombre')]) ?>
<?= $form->field($model, 'mandatario_apellido_paterno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandatario_apellido_paterno')]) ?>
            <?= $form->field($model, 'mandatario_apellido_materno')->input('text', ['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => $model->getAttributeLabel('mandatario_apellido_materno')]) ?>
            <?= $form->field($model, 'personeria_mandato')->textarea(['class' => 'uppercase form-control', 'style' => 'width:400px', 'min' => 0, 'placeholder' => ""]) ?>

        <div class="custom-control custom-checkbox">
<?= $form->field($model, 'tiene_prenda')->checkbox(["class" => "custom-control-input"]) ?>
        <?= $form->field($model, 'tiene_multas')->checkbox(["class" => "custom-control-input"]) ?>
        </div>
    </div>
    <div class="box-footer" >
    <?= Html::submitButton('Guardar Venta', ['class' => 'btn btn-success pull-left']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
