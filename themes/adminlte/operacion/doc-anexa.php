<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VehiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (\app\models\Operacion::TIPO_VENTA[$operacionModel->venta_tipo] === \app\models\Operacion::TIPO_VENTA[1]) {
    $this->title = 'Documentación Anexa Venta Única';
} else if (\app\models\Operacion::TIPO_VENTA[$operacionModel->venta_tipo] === \app\models\Operacion::TIPO_VENTA[2]) {
    $this->title = 'Documentación Anexa Venta con Representante Empresa';
} else if (\app\models\Operacion::TIPO_VENTA[$operacionModel->venta_tipo] === \app\models\Operacion::TIPO_VENTA[3]) {
    $this->title = 'Documentación Anexa Venta con Representante Mandato';
} else if (\app\models\Operacion::TIPO_VENTA[$operacionModel->venta_tipo] === \app\models\Operacion::TIPO_VENTA[4]) {
    $this->title = 'Documentación Anexa Venta en Comunidad';
}
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>
<?= \dmstr\widgets\Alert::widget() ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'create-venta-form',
                //'action' => ['operacion/doc-anexa'],
    ]);
    ?>
    <input type="hidden" id="venta_id" value="<?= $operacionModel->id ?>" />
    <div class="box-body">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-6"><?= Html::button('Adjuntar', ['class' => 'btn btn-outline-info pull-right', "id" => "cav"]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'cav')->checkbox(["class" => ""]) ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= Html::button('Adjuntar', ['class' => 'btn btn-outline-info pull-right', "id" => "multas"]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'certificado_multas')->checkbox(["class" => ""]) ?></div>
            </div>
            <div class="row">
                <div class="col-md-6"><?= Html::button('Adjuntar', ['class' => 'btn btn-outline-info pull-right', "id" => "permiso"]) ?></div>
                <div class="col-md-6"><?= $form->field($model, 'permiso_circulacion')->checkbox(["class" => ""]) ?></div>
            </div>
            <div class="row">
                <?php if (\app\models\Operacion::TIPO_VENTA[$operacionModel->venta_tipo] === \app\models\Operacion::TIPO_VENTA[2]) { ?>
                    <div class="col-md-6"><?= Html::button('Adjuntar', ['class' => 'btn btn-outline-info pull-right', "id" => "personeria"]) ?></div>
                    <div class="col-md-6"><?= $form->field($model, 'personeria')->checkbox(["class" => ""]) ?></div>
                    <?php
                } else if (\app\models\Operacion::TIPO_VENTA[$operacionModel->venta_tipo] === \app\models\Operacion::TIPO_VENTA[3]) {
                    ?>
                    <div class="col-md-6"><?= Html::button('Adjuntar', ['class' => 'btn btn-outline-info pull-right', "id" => "mandato"]) ?></div>
                    <div class="col-md-6"><?= $form->field($model, 'mandato')->checkbox(["class" => ""]) ?></div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="col-md-7">
            
        </div>
        <div class="box-footer" >
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title">Adjuntar</h3>
                    <small class="font-bold"></small>
                </div>
                <div class="modal-body">
                    <div id='modalContent'>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_attachedDocuments', ['model' => $model, 'venta_id' => $operacionModel->id, "datos" => $datos]) ?>

    <?php
    $script = <<< JS
        $(document).ready(function() {
            $("button").click(function() {
                action = $(this).attr("id");
                id = $("#venta_id").val();
                $("#myModal").find("#modalContent").html("");
                $("#myModal").find(".modal-title").text("");
                $("#myModal").modal("show");
                $.ajax({
                        url: "attach-document",
                        data: {id: id, action: action},
                        //type: "post",
                        success: function(respuesta) {
                                if (respuesta != null && respuesta == "OK") {
                                    $("#myModal").modal('toggle');
                                } else {
                                    $("#myModal").find("#modalContent").html(respuesta);
                                    $("#myModal").find(".modal-title").text($("#titulo").val());
                                }
                        },
                        error: function() {
                                $("#myModal").find("#modalContent").html("ERROR");
                        }
                });
            });
        
        });
JS;
    $this->registerJs($script);
    ?>