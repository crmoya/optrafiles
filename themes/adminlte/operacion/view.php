<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Operacion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Operaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<h1>Operación N° <?= Html::encode($this->title) ?></h1>
<div class="affix-wrapper" style="height: 50px;">
    <div data-spy="affix" data-offset-top="150" class="div-margin">
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> Guardar y Adjuntar Anexos', ['doc-anexa', 'id' => $model->id], ['class' => 'btn btn-primary pull-left', "value" => "anexo", "name" => "boton"]) ?> 
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> Actualizar', ['doc-anexa', 'id' => $model->id], ['class' => 'btn btn-primary pull-left', "value" => "anexo", "name" => "boton"]) ?>
    </div>
</div>

<div class="panel panel-default col-md-6" style="padding: 0px; margin-right: 10px;">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Datos de la Operación</h3>
    </div>
    <div class="panel-body">
        <h2>Datos Venta</h2>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                'venta_fecha',
                [
                    'attribute' => 'ventaPropietario.completeName',
                    'label' => 'Vendedor',
                ],
                'ventaPropietario.direccion:ntext',
                'venta_precio',
                [
                    'attribute' => 'venta_tipo',
                    'value' => function($model) {
                        return \app\models\Operacion::TIPO_VENTA[$model->venta_tipo];
                    },
                    'label' => 'Tipo Venta',
                ],
                [
                    'attribute' => 'ventaUser.completeName',
                    'label' => 'Funcionario',
                ],
                [
                    'attribute' => 'ventaMandatario.completeName',
                    'label' => 'Mandatario',
                    'visible' => $model->venta_tipo === 3
                ],
            ],
        ])
        ?>
        
        <?php if ($model->venta_tipo === 2) { ?>
            <h2>Representantes </h2>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                    <?php foreach ($representantesVenta as $i => $r) { ?>
                        <tr><th>Representante <?= $i + 1 ?></th><td><?= $r->completeNameWithRut ?></td></tr>
                        <tr><th>Dirección <?= $i + 1 ?></th><td><?= $r->direccion ?></td></tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else if ($model->venta_tipo === 4) { ?>
            <h2>Otros Vendedores</h2>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                    <?php foreach ($ventaPropietario as $i => $vp) { ?>
                        <tr><th>Vendedor <?= $i + 2 ?></th><td><?= $vp->propietario->completeNameWithRut ?></td></tr>
                        <tr><th>Dirección <?= $i + 2 ?></th><td><?= $vp->propietario->direccion ?></td></tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
        
        <h2>Datos Compra</h2>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'id',
                'compra_fecha',
                [
                    'attribute' => 'compraPropietario.completeName',
                    'label' => 'Comprador',
                ],
                'compraPropietario.direccion:ntext',
                'compra_precio',
                [
                    'attribute' => 'compra_tipo',
                    'value' => function($model) {
                        if (isset($model->compra_tipo))
                            return \app\models\Operacion::TIPO_COMPRA[$model->compra_tipo];
                        else
                            return "";
                    },
                    'label' => 'Tipo Compra',
                ],
                [
                    'attribute' => 'compraUser.completeName',
                    'label' => 'Funcionario',
                ],
                [
                    'attribute' => 'compraParaPropietario.completeName',
                    'label' => 'Compra Para',
                    'visible' => $model->compra_tipo === 3
                ],
            ],
        ])
        ?>
        <?php if ($model->compra_tipo === 2) { ?>
            <h2>Representantes </h2>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                    <?php foreach ($representantesCompra as $i => $r) { ?>
                        <tr><th>Representante <?= $i + 1 ?></th><td><?= $r->completeNameWithRut ?></td></tr>
                        <tr><th>Dirección <?= $i + 1 ?></th><td><?= $r->direccion ?></td></tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else if ($model->compra_tipo === 4) { ?>
            <h2>Otros Compradores</h2>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                    <?php foreach ($compraPropietario as $i => $cp) { ?>
                        <tr><th>Comprador <?= $i + 2 ?></th><td><?= $cp->propietario->completeNameWithRut ?></td></tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
            
    </div>

</div>
<div class="panel panel-default col-md-5" style="padding: 0px; margin-right: 10px;">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Datos del Vehículo</h3>
    </div>
    <div class="panel-body">
        <?=
        DetailView::widget([
            'model' => $model->ventaVehiculo,
            'attributes' => [
                'patente',
                [
                    'attribute' => 'tipoVehiculo.nombre',
                    'label' => 'Tipo Vehículo',
                ],
                'modelo.marca.nombre',
                'modelo.nombre',
                'agno',
                'tasacion_comercial',
                [
                    'attribute' => 'tiene_multa',
                    'label' => 'Multa',
                    'value' => function($model) {
                        return $model->tiene_multa ? "SÍ" : "NO";
                    }
                ],
                [
                    'attribute' => 'tiene_prenda',
                    'label' => 'Prenda',
                    'value' => function($model) {
                        return $model->tiene_prenda ? "SÍ" : "NO";
                    }
                ]
            ],
        ])
        ?>
    </div>
</div>