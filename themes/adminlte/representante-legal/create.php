<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RepresentanteLegal */

$this->title = 'Crear Representante Legal';
$this->params['breadcrumbs'][] = ['label' => 'Representante Legal', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="representante-legal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
