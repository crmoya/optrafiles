<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RepresentanteLegalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Representantes Legales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="representante-legal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Representante Legal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i> Representantes Legales</h3>
        </div>
        <div class="panel-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'rut',
                    'nombre',
                    'apellido_paterno',
                    'apellido_materno',
                    //'huella',
                    //'template',
                    //'template_size',
                    ['class' => 'yii\grid\ActionColumn', 'template'=>'{update} {delete}'],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
