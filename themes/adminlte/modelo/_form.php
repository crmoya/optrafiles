<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Modelo */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?php
    $form = ActiveForm::begin([
                'class' => 'form-horizontal',
                'fieldConfig' => [
                    'template' => '<div class="col-sm-3 text-right">{label}</div>
                                        <div class="col-sm-9">{input}{error}</div>',
                ]
    ]);
    ?>
    <div class="box-body">
        <?= $form->field($model, 'nombre')->textInput(["class" => "uppercase form-control", 'maxlength' => true]) ?>
        <?= $form->field($model, 'marca_id')->dropDownList(
            \yii\helpers\ArrayHelper::map(app\models\Marca::find()->select(["id", "nombre"])->all(), "id", "nombre")) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>