<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MandatarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mandatarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mandatario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mandatario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'rut',
            'nombres',
            'apellido_paterno',
            'apellido_materno',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
