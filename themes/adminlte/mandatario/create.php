<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mandatario */

$this->title = 'Create Mandatario';
$this->params['breadcrumbs'][] = ['label' => 'Mandatarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mandatario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
