<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mandatario */

$this->title = 'Update Mandatario: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mandatarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mandatario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
