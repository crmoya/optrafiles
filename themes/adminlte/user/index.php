<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i> Usuarios</h3>
        </div>
        <div class="panel-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:80px;  min-width:80px;'],
                    ],
                    'username',
                    'nombre',
                    'apellido',
                    [
                        'attribute' => 'nombre_sucursal',
                        'value' => 'sucursal.nombre',
                        'label' => "Sucursal"
                    ],
                    'email:email',
                    //'status',
                    //'created_at',
                    //'updated_at',
                    ['class' => 'yii\grid\ActionColumn', 'template'=>'{update} {delete}'],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
