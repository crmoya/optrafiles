<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $change_pass_model app\models\ChangePasswordForm */

$this->title = 'Perfil de Usuario';
?>

<section class="content-header">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    $this->params['breadcrumbs'][] = ['label' => 'Usuario', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    ?>
</section>

<section class="content">

    <?= Alert::widget() ?>
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="<?= Yii::getAlias("@web") . '/images/user_icon.png' ?>" alt="Imagen de perfil de Usuario">

                    <h3 class="profile-username text-center"><?= ($role != 'administrador') ? $model->nombre . " " . $model->apellido : $model->username ?></h3>

                    <p class="text-muted text-center"><?= ucfirst($role) ?></p>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Sobre mí</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>

                    <p class="text-muted">
                        <?= $model->email ?>
                    </p>

                    <hr>

                    <strong><i class="fa fa-clock-o margin-r-5"></i> Última modificación</strong>

                    <p class="text-muted">
                        <?php
                        //echo \Yii::t('app', '{0,date,dd MMMM YYYY HH:mm}', $model->updated_at);
                        echo Yii::$app->formatter->asDate($model->updated_at, 'd-MM-Y hh:i:ss');
                        ?></p>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#timeline" data-toggle="tab" aria-expanded="true">Mis observaciones</a></li>
                    <li class=""><a href="#updatePassword" data-toggle="tab" aria-expanded="false">Contraseña</a></li>
                </ul>
                <div class="tab-content">

                    <!-- /.tab-pane -->
                    <div class="tab-pane active" id="timeline">
                        <!-- The timeline -->
                        <div class="timeline-container">

                        </div>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane row" id="updatePassword">
                        <?php
                        $form = ActiveForm::begin([
                                    'enableAjaxValidation' => true,
                                    //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                                    'fieldConfig' => [
                                        'template' => '
                                            <div class="col-sm-3 text-right">{label}</div>
                                            <div class="col-sm-9">{input}{error}</div>',
                                    ]
                        ]);
                        ?>
                        <?= $form->field($change_pass_model, 'old_password')->passwordInput(["class" => "form-control", 'maxlength' => true, 'placeholder' => $change_pass_model->getAttributeLabel('old_password')]) ?>
                        <?= $form->field($change_pass_model, 'new_password')->passwordInput(["class" => "form-control", 'maxlength' => true, 'placeholder' => $change_pass_model->getAttributeLabel('new_password')]) ?>
                        <?= $form->field($change_pass_model, 'repeat_new_password')->passwordInput(["class" => "form-control", 'maxlength' => true, 'placeholder' => $change_pass_model->getAttributeLabel('repeat_new_password')]) ?>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <?= Html::submitButton('Actualizar', ['class' => 'btn btn-success pull-right']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
</section>
