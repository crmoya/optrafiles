<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\RutAsset;

RutAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?php
    $form = ActiveForm::begin([
                'class' => 'form-horizontal',
                'fieldConfig' => [
                    'template' => '<div class="col-sm-3 text-right">{label}</div>
                                        <div class="col-sm-9">{input}{error}</div>',
                ],
                'enableAjaxValidation' => true,
                'validationUrl' => ['user/email-validation'],
    ]);
    ?>
    <div class="box-body">
        <?= $form->field($model, 'username')->textInput(["class" => "uppercase optional-rut form-control", 'maxlength' => true]) ?>
        <?= $form->field($model, 'email')->textInput(["class" => "uppercase form-control", 'maxlength' => true]) ?>
        <?= $form->field($model, 'nombre')->textInput(["class" => "uppercase form-control", 'maxlength' => true]) ?>
        <?= $form->field($model, 'apellido')->textInput(["class" => "uppercase form-control", 'maxlength' => true]) ?>
        <?=
        $form->field($model, 'sucursal_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(app\models\Sucursal::find()->select(["id", "nombre"])->all(), "id", "nombre"), ['prompt' => 'Seleccione.. '])
        ?>
        <?=
        $form->field($model, 'tipo_usuario')->dropDownList(app\models\User::TIPOS_USUARIOS);
        ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success pull-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
