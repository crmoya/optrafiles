<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-form">

    <?=dmstr\widgets\Alert::widget();?>
    <?php $form = ActiveForm::begin(); ?>

    <h4>
        Seleccione las funciones que tendrá asociadas el rol
    </h4>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>
    
    <input type="text" class="form-control" id="filtro" placeholder=" ... FILTRO DE PERMISOS ..."/>
    <br/>
        
    <table style="font-size:14pt;" class="table-responsive table table-striped table-hover">
        
    <?php

    $funciones = app\models\AuthItem::find()->orderBy(['description' => SORT_ASC])->where(['type'=>2])->all();
    foreach($funciones as $funcion):
        $hijo = app\models\AuthItemChild::find()->where(['parent'=>$model->name,'child'=>$funcion->name])->one();
        $esHijo = isset($hijo);
    ?>
        <tr >
            <td>
                <input type="checkbox" name="funciones[<?=$funcion->name?>]" <?=$esHijo?"checked":""?>/>
            </td>
            <td class='funcion'>
                 <?=$funcion->description?>
            </td>
        </tr>
    <?php endforeach;?>
    </table>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>
    

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$(document).ready( function () {
    $('#filtro').keyup(function(e){
        filter($(this).val());
    });
        
    function filter(palabra){
        $('.funcion').parent().show();
        $('.funcion').each(function(e){
            var contenido = $(this).html().toUpperCase();
            if(contenido.indexOf(palabra.toUpperCase()) == -1){
                $(this).parent().hide();
            }
        });
    }
});            
JS;

$this->registerJs($script);
?>
