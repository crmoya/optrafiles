<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=dmstr\widgets\Alert::widget();?>
    <p>
        <?= Html::a('Nuevo Rol', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'name',
            'description:ntext',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{add}',
                'buttons' => [
                    'add' => function ($url, $model, $key) {
                        return \yii\helpers\Html::a('<i class="fa fa-tasks"></i>', yii\helpers\Url::to(['auth-item/permisos', 'name' => $model->name]), ['title' => 'Asignar Funciones al ROL',]);
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
