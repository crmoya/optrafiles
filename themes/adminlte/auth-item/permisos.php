<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */

$this->title = 'Editar Permisos del Rol: ' . $model->name;
?>
<div class="auth-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_permisos', [
        'model' => $model,
    ]) ?>

</div>
