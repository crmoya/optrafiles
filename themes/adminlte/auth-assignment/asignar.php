<?php
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AuthAssignment */

$this->title = 'Asignar Usuarios a Roles y/o funciones: ';
?>
<div class="auth-assignment-update">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= dmstr\widgets\Alert::widget() ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'email:email',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{asignar}',
                'buttons'=>[
                    'asignar' => function ($url, $model, $key) {
                        return \yii\helpers\Html::a('<i class="fa fa-tasks"></i>', yii\helpers\Url::to(['auth-assignment/usuario', 'id' => $model->id]), ['title' => 'Asignar Rol/Funciones al Usuario',]);
                    },
                ]
            ],
        ],
    ]);
    ?>
</div>
