<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\AuthItem;

/* @var $this yii\web\View */
/* @var $model app\models\AuthAssignment */

$this->title = 'Asignar Rol/Permisos a Usuario';

$request = Yii::$app->request;
$get = $request->get();
$rol = "";
if (isset($get['rol'])) {
    $rol = $get['rol'];
}
?>
<div class="auth-assignment-view">

    <h1><?= Html::encode($this->title) . " " . $model->username ?></h1>

    <table class="table table-striped" style="width: 400px;">
        <tbody>
            <tr>
                <th scope="row" class="text-right"><?= Html::activeLabel($model, "username") ?></th>
                <td><?= Html::getAttributeValue($model, "username") ?></td>
            </tr>
            <tr>
                <th scope="row" class="text-right"><?= Html::activeLabel($model, "email") ?></th>
                <td><?= Html::getAttributeValue($model, "email") ?></td>
            </tr>
    </table>


    <p style="font-size: 14pt;">
        Seleccione un rol del campo desplegable para asignar al usuario. Si lo desea, también puede seleccionar funciones individuales.
    </p>
    <?= dmstr\widgets\Alert::widget(); ?>

    <?php $form = ActiveForm::begin(); ?>


    <?php
    $datos = \yii\helpers\ArrayHelper::map(AuthItem::find()->where(['type' => 1])->all(), 'name', 'name');
    echo $form->field($modelForm, 'rol')->dropDownList($datos, ['prompt' => 'Seleccione Un Rol', 'id' => 'rol', 'value' => $rol]);
    ?>



    <?php if ($rol != ''): ?>
        <?= Html::submitButton('Asignar todas las funciones seleccionadas (incluyendo las del ROL)', ['class' => 'btn btn-success']) ?>
        <?= Html::submitButton('Asignar solo las funciones del ROL seleccionado', ['class' => 'btn btn-info', 'name' => 'ROLSeleccionado']) ?>
        <br/> <br/>

        <input type="text" class="form-control" id="filtro" placeholder=" ... FILTRO DE PERMISOS ..."/>
        <br/>

        <table style="font-size:14pt;" class="table-responsive table table-striped table-hover">
            <?php
            $funciones = app\models\AuthItem::find()->orderBy(['description' => SORT_ASC])->where(['type' => 2])->all();
            foreach ($funciones as $funcion):
                //preseleccionar funciones del rol
                $hijo = app\models\AuthItemChild::find()->where(['child' => $funcion->name, 'parent' => $rol])->one();
                $esHijo = isset($hijo);

                if (!$esHijo) {
                    //preseleccionar función individual si está asignada al usuario
                    $hijo = app\models\AuthAssignment::find()->where(['item_name' => $funcion->name, 'user_id' => $model->id])->one();
                    $esHijo = isset($hijo);
                }
                ?>
                <tr>
                    <td>
                        <input type="checkbox" class="seleccion" name="funciones[<?= $funcion->name ?>]" <?= $esHijo ? "checked" : "" ?>/>
                    </td>
                    <td class='funcion'>
                        <?= $funcion->description ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <br/>
        <div class="form-group">
            <?= Html::submitButton('Asignar todas las funciones seleccionadas (incluyendo las del ROL)', ['class' => 'btn btn-success']) ?>
            <?= Html::submitButton('Asignar solo las funciones del ROL seleccionado', ['class' => 'btn btn-info', 'name' => 'ROLSeleccionado']) ?>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>

</div>
<?php
$location = \yii\helpers\Url::to(["auth-assignment/usuario", 'id' => $model->id]);

$script = <<< JS
$(document).ready( function () {
    $('#rol').change(function(e){
        window.location = '$location&rol=' + $(this).val();
    });
        
    $('#filtro').keyup(function(e){
        filter($(this).val());
    });
                
    function filter(palabra){
        $('.funcion').parent().show();
        $('.funcion').each(function(e){
            var contenido = $(this).html().toUpperCase();
            if(contenido.indexOf(palabra.toUpperCase()) == -1){
                $(this).parent().hide();
            }
        });
    }
});            
JS;

$this->registerJs($script);
?>
<style>
    .form-control{
        border-radius: 5px;
    }
</style>
