<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Propietario */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Propietarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="propietario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro desea eliminar este propietario?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rut',
            'nombres',
            'apellido_paterno',
            'apellido_materno',
            ['attribute'=>'persona_natural','value'=>$model->persona_natural==1?"SÍ":"NO"],
        ],
    ]) ?>

    <!-- Images used to open the lightbox -->
    <div class="row">
      <div class="column">
          <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/huella.jpg',['class'=>'hover-shadow',"onclick"=>"openModal();currentSlide(1)"])?>
        
      </div>
      <div class="column">
        <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/firma.jpeg',['class'=>'hover-shadow',"onclick"=>"openModal();currentSlide(2)"])?>
      </div>
      <div class="column">
        <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula1.jpeg',['class'=>'hover-shadow',"onclick"=>"openModal();currentSlide(3)"])?>
      </div>
      <div class="column">
        <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula2.jpeg',['class'=>'hover-shadow',"onclick"=>"openModal();currentSlide(4)"])?>
      </div>
    </div>

    <!-- The Modal/Lightbox -->
    <div id="myModal" class="modal">
      <span class="close cursor" onclick="closeModal()">&times;</span>
      <div class="modal-content">

        <div class="mySlides">
          <div class="numbertext">1 / 4</div>
          <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/huella.jpg',['style'=>'height:70%'])?>
        </div>

        <div class="mySlides">
          <div class="numbertext">2 / 4</div>
          <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/firma.jpeg',['style'=>'height:70%'])?>
        </div>

        <div class="mySlides">
          <div class="numbertext">3 / 4</div>
          <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula1.jpeg',['style'=>'height:70%'])?>
        </div>

        <div class="mySlides">
          <div class="numbertext">4 / 4</div>
          <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula2.jpeg',['style'=>'height:70%'])?>
        </div>

        <!-- Next/previous controls -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

        <!-- Caption text -->
        <div class="caption-container">
          <p id="caption"></p>
        </div>

        <!-- Thumbnail image controls -->
        <div class="column">
            <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/huella.jpg',['height'=>'200px','alt'=>'Huella','class'=>'demo',"onclick"=>"currentSlide(1)"])?>
        </div>

        <div class="column">
            <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/firma.jpeg',['height'=>'200px','alt'=>'Firma','class'=>'demo',"onclick"=>"currentSlide(2)"])?>
        </div>

        <div class="column">
          <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula1.jpeg',['height'=>'200px','alt'=>'Cédula Anterior','class'=>'demo',"onclick"=>"currentSlide(3)"])?>
        </div>

        <div class="column">
          <?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula2.jpeg',['height'=>'200px','alt'=>'Cédula Posterior','class'=>'demo',"onclick"=>"currentSlide(4)"])?>
        </div>
      </div>
    </div> 
    
    
    <!--
    <table class="table-responsive table table-striped">
        <tr>
            <td>Huella: </td><td><?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/huella.jpg',['height'=>'100px'])?></td>
        </tr>
         <tr>
            <td>Firma: </td><td><?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/firma.jpeg',['height'=>'100px'])?></td>
        </tr>
        <tr>
            <td>Cédula cara Anterior: </td><td><?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula1.jpeg',['height'=>'100px'])?></td>
        </tr>
         <tr>
            <td>Cédula cara Posterior: </td><td><?=Html::img('@web/../optrafiles/propietarios/'.$model->rut.'/cedula2.jpeg',['height'=>'100px'])?></td>
        </tr>
    </table>
    -->
</div>
<style>

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Create four equal columns that floats next to eachother */
.column {
  float: left;
  width: 25%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

/* Hide the slides by default */
.mySlides {
  display: none;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* Caption text */
.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

img.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
  width:200px;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
<script>
// Open the Modal
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

// Close the Modal
function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>