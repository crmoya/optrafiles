<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Propietario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'create-propietario-form',
                'fieldConfig' => [
                    'template' => '<div class="col-md-3 text-right">{label}</div>
                                        <div class="col-md-9">{input}{error}</div>',
                ]
    ]);
    ?>
    <div class="box-body">
        <?php
        if (!$model->isNewRecord) {
            echo $form->field($model, 'rut')->textInput(["class" => "uppercase form-control", 'style' => 'width:400px', 'maxlength' => true, 'readonly' => true]);
        } else {
            echo $form->field($model, 'rut')->textInput(["class" => "uppercase form-control", 'style' => 'width:400px', 'maxlength' => true]);
        }
        ?>

        <?= $form->field($model, 'nombres')->textInput(["class" => "uppercase form-control", 'style' => 'width:400px', 'maxlength' => true]) ?>
        <?= $form->field($model, 'apellido_paterno')->textInput(["class" => "uppercase form-control", 'style' => 'width:400px', 'maxlength' => true]) ?>
        <?= $form->field($model, 'apellido_materno')->textInput(["class" => "uppercase form-control", 'style' => 'width:400px', 'maxlength' => true]) ?>
        <?= $form->field($model, 'persona_natural')->radioList(array('1' => 'SÍ', 0 => 'NO'))
                ->label('Persona Natural')
        ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>