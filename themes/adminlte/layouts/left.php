<?php

use yii\helpers\Url;
?>
<aside class="main-sidebar">

    <section class="sidebar">


        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                      <span class="input-group-btn">
                        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>-->
        <!-- /.search form -->

        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        ['label' => 'Menú', 'options' => ['class' => 'header']],
                        ['label' => 'Iniciar Sesión', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                        ['label' => 'Inicio', 'url' => ['site/index'], 'icon' => 'home', 'visible' => !Yii::$app->user->isGuest, 'active' => (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index')],
                        [
                            'label' => 'Operaciones',
                            'icon' => 'circle-o',
                            'url' => '#',
                            'visible' => Yii::$app->user->can("operaciones") || Yii::$app->user->can("admin") || Yii::$app->user->can("funcionario"),
                            'items' => [
                                [
                                    'label' => 'Vehículos Ingresados',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['vehiculo/index']),
                                    'visible' => Yii::$app->user->can("vehiculo/index") || Yii::$app->user->can("admin"),
                                    'active' => (
                                                    Yii::$app->controller->id == 'vehiculo' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create")
                                                )
                                ],
                                [
                                    'label' => 'Generación Venta',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['operacion/create']),
                                    'visible' => Yii::$app->user->can("operacion/create") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'operacion' && (Yii::$app->controller->action->id == "create" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view"))
                                ],
                                [
                                    'label' => 'Ventas',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['operacion/all']),
                                    'visible' => Yii::$app->user->can("operacion/all") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'operacion' && (Yii::$app->controller->action->id == "all"))
                                ],
                                [
                                    'label' => 'Generación de Compra',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['operacion/create-compra']),
                                    'visible' => Yii::$app->user->can("operacion/create-compra") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'operacion' && (Yii::$app->controller->action->id == "create-compra"))
                                ],
                            ]
                        ],
                        [
                            'label' => 'Mantenedores',
                            'icon' => 'circle-o',
                            'url' => '#',
                            'visible' => Yii::$app->user->can("mantenedor") || Yii::$app->user->can("admin"),
                            'items' => [
                                /* [
                                  'label' => 'Permisos de Usuarios',
                                  'icon' => 'circle-o',
                                  'url' => Url::to(['rbac/index']),
                                  'visible' => Yii::$app->user->can("rbac/index") || Yii::$app->user->can("admin"),
                                  'active' => (Yii::$app->controller->id == 'rbac' && Yii::$app->controller->action->id == "index")
                                  ], */
                                [
                                    'label' => 'Roles de Usuarios',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['auth-item/indexrol']),
                                    'visible' => Yii::$app->user->can("rbac/index") || Yii::$app->user->can("admin"),
                                    'active' => (
                                    (Yii::$app->controller->id == 'auth-item' && Yii::$app->controller->action->id == "indexrol") ||
                                    (Yii::$app->controller->id == 'auth-item' && Yii::$app->controller->action->id == "create") ||
                                    (Yii::$app->controller->id == 'auth-item' && Yii::$app->controller->action->id == "update") ||
                                    (Yii::$app->controller->id == 'auth-item' && Yii::$app->controller->action->id == "view") ||
                                    (Yii::$app->controller->id == 'auth-item' && Yii::$app->controller->action->id == "permisos")
                                    )
                                ],
                                [
                                    'label' => 'Asignar roles a Usuarios',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['auth-assignment/asignar']),
                                    'visible' => Yii::$app->user->can("rbac/index") || Yii::$app->user->can("admin"),
                                    'active' => (
                                    (Yii::$app->controller->id == 'auth-assignment' && Yii::$app->controller->action->id == "asignar") ||
                                    (Yii::$app->controller->id == 'auth-assignment' && Yii::$app->controller->action->id == "usuario")
                                    )
                                ],
                                [
                                    'label' => 'Propietarios',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['propietario/index']),
                                    'visible' => Yii::$app->user->can("propietario/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'propietario' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Marca',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['marca/index']),
                                    'visible' => Yii::$app->user->can("marca/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'marca' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Modelo',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['modelo/index']),
                                    'visible' => Yii::$app->user->can("modelo/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'modelo' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Notaría',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['notaria/index']),
                                    'visible' => Yii::$app->user->can("notaria/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'notaria' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Representante Legal',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['representante-legal/index']),
                                    'visible' => Yii::$app->user->can("representante-legal/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'representante-legal' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Suscriptor',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['suscriptor/index']),
                                    'visible' => Yii::$app->user->can("suscriptor/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'suscriptor' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Sucursal',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['sucursal/index']),
                                    'visible' => Yii::$app->user->can("sucursal/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'sucursal' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Usuario',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['user/index']),
                                    'visible' => Yii::$app->user->can("user/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'user' && (Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "update" || Yii::$app->controller->action->id == "view" || Yii::$app->controller->action->id == "create"))
                                ],
                                [
                                    'label' => 'Matrices Tipos de Venta',
                                    'icon' => 'circle-o',
                                    'url' => Url::to(['matriz-tipo-venta/index']),
                                    'visible' => Yii::$app->user->can("matriz-tipo-venta/index") || Yii::$app->user->can("admin"),
                                    'active' => (Yii::$app->controller->id == 'user' && (Yii::$app->controller->action->id == "index"))
                                ],
                            ],
                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
