<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


//if (Yii::$app->controller->action->id === 'login') { 
///**
// * Do not use this code in your template. Remove it. 
// * Instead, use the code  $this->layout = '//main-login'; in your controller.
// */
//    echo $this->render(
//        'main-login',
//        ['content' => $content]
//    );
//} else {

if (class_exists('app\assets\AppAsset')) {
    app\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode((!is_null($this->title)) ? $this->title : Yii::$app->name) ?></title>

        <?php $this->head() ?>    

        <?php
        $this->registerCssFile("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css", [
            'depends' => [\yii\bootstrap\BootstrapAsset::className()],
                ], 'css-print-theme');

        $this->registerCssFile("https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css", [
            'depends' => [\yii\bootstrap\BootstrapAsset::className()],
                ], 'css-print-theme2');

        $this->registerCssFile("https://code.jquery.com/ui/1.12.1/themes/redmond/jquery-ui.css", [
            'depends' => [\yii\bootstrap\BootstrapAsset::className()],
                ], 'css-media');


        $this->registerJsFile(
                'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );

        $this->registerJsFile(
                'https://unpkg.com/sweetalert/dist/sweetalert.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );

        $this->registerJsFile(
                'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->registerJsFile(
                'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->registerJsFile(
                'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->registerJsFile(
                'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->registerJsFile(
                'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->registerJsFile(
                'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->registerJsFile(
                'https://cdn.datatables.net/plug-ins/1.10.19/sorting/datetime-moment.js', ['depends' => [\yii\web\JqueryAsset::className()]]
        );

        $this->registerJsFile(
                "https://code.jquery.com/ui/1.12.1/jquery-ui.min.js", ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?=
            $this->render(
                    'header.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?=
            $this->render(
                    'left.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?=
            $this->render(
                    'content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
            )
            ?>

        </div>
        <?php
        yii\bootstrap\Modal::begin([
            'header' => '<div id="modalHeader"></div>',
            'id' => 'modal',
            'closeButton' => ['id' => 'close-button'],
            'size' => 'modal-lg',
            'options' => [
                'tabindex' => false // important for Select2 to work properly
            ],
        ]);
        echo "<div id='modalContent'></div>";
        yii\bootstrap\Modal::end();
        ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
<?php // }  ?>
