<?php

use yii\widgets\Breadcrumbs;
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
//                if ($this->title !== null) {
//                    echo \yii\helpers\Html::encode($this->title);
//                } else {
//                    echo \yii\helpers\Inflector::camel2words(
//                        \yii\helpers\Inflector::id2camel($this->context->module->id)
//                    );
//                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
//                } 
                ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
                [
                    'tag' => 'ol',
                    'itemTemplate' => "<li><i>{link}</i></li>\n",
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]
        )
        ?>
    </section>

    <section class="content">
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>
<?php
if (!Yii::$app->user->isGuest && Yii::$app->user->can("barra-progreso")) {
    ?>
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-ots-tab" data-toggle="tab"><i class="fa fa-clock-o"></i></a></li>
        </ul>
        <?php $current = app\models\Escritura::countAllCurrent(); ?>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-ots-tab">
                <h3 class="control-sidebar-heading">Progreso de Repertorios</h3>
                <ul class='control-sidebar-menu'>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['escritura/expired']) ?>">
                            <h4 class="control-sidebar-subheading">
                                Vencidas (<?= (empty($current) || $current == 0) ? 0 : app\models\Escritura::countAllExpired() ?>)
                                <span class="label label-danger pull-right"><?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllExpired() / $current * 100, 2) ?>%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: <?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllExpired() / $current * 100, 2) ?>%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['escritura/by-expired']) ?>">
                            <h4 class="control-sidebar-subheading">
                                A punto de vencer (<?= (empty($current) || $current == 0) ? 0 : app\models\Escritura::countAllByExpired() ?>)
                                <span class="label label-warning pull-right"><?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllByExpired() / $current * 100, 2) ?>%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: <?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllByExpired() / $current * 100, 2) ?>%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['escritura/normal']) ?>">
                            <h4 class="control-sidebar-subheading">
                                En tiempo normal (<?= (empty($current) || $current == 0) ? 0 : app\models\Escritura::countAllNormal() ?>)
                                <span class="label label-primary pull-right"><?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllNormal() / $current * 100, 2) ?>%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: <?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllNormal() / $current * 100, 2) ?>%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['escritura/new']) ?>">
                            <h4 class="control-sidebar-subheading">
                                Nuevas (<?= (empty($current) || $current == 0) ? 0 : app\models\Escritura::countAllNew() ?>)
                                <span class="label label-success pull-right"><?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllNew() / $current * 100, 2) ?>%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: <?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllNew() / $current * 100, 2) ?>%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="<?= \yii\helpers\Url::to(['escritura/without-timer']) ?>">
                            <h4 class="control-sidebar-subheading">
                                Sin temporizador (<?= (empty($current) || $current == 0) ? 0 : app\models\Escritura::countAllWithoutTimer() ?>)
                                <span class="label label-info pull-right"><?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllWithoutTimer() / $current * 100, 2) ?>%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-info" style="width: <?= (empty($current) || $current == 0) ? 0 : round(app\models\Escritura::countAllWithoutTimer() / $current * 100, 2) ?>%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->
            </div>
            <!-- /.tab-pane -->

        </div>
    </aside><!-- /.control-sidebar -->
    <?php
}
?>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>