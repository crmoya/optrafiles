<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\RutAsset;

RutAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Compareciente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?php
    $form = ActiveForm::begin([
                'class' => 'form-horizontal',
                'fieldConfig' => [
                    'template' => '<div class="col-sm-3 text-right">{label}</div>
                                        <div class="col-sm-9">{input}{error}</div>',
                ]
    ]);
    ?>
    <div class="box-body">
        <?= $form->field($model, 'rut')->textInput(["class" => "uppercase form-control", 'maxlength' => true, "placeholder" => $model->getAttributeLabel("rut")]) ?>
        <?= $form->field($model, 'nombre')->textInput(["class" => "uppercase form-control", 'maxlength' => true, "placeholder" => $model->getAttributeLabel("nombre")]) ?>
        <?= $form->field($model, 'apellido_paterno')->textInput(["class" => "uppercase form-control", 'maxlength' => true, "placeholder" => $model->getAttributeLabel("apellido_paterno")]) ?>
        <?= $form->field($model, 'apellido_materno')->textInput(["class" => "uppercase form-control", 'maxlength' => true, "placeholder" => $model->getAttributeLabel("apellido_materno")]) ?>
        <?= $form->field($model, 'telefono')->textInput(["class" => "uppercase form-control", 'maxlength' => true, "placeholder" => $model->getAttributeLabel("telefono")]) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success pull-right']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>