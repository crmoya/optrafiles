<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Compareciente */

$this->title = 'Crear Compareciente';
$this->params['breadcrumbs'][] = ['label' => 'Comparecientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compareciente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
