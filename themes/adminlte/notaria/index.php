<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotariaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notarías';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notaria-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Notaría', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="glyphicon glyphicon-tower"></i> Notarías</h3>
        </div>
        <div class="panel-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'nombre',
                    [
                        'attribute' => 'tiene_aiken',
                        'value' => function ($model) {
                            return $model->tiene_aiken === 1 ? "SÍ" : "NO";
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn', 'template'=>'{update} {delete}'],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
