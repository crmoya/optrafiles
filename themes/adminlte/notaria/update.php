<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Notaria */

$this->title = 'Actualizar Notaria: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notarias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="notaria-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
