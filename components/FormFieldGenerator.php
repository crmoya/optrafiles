<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

/**
 * Description of FormFieldGenerator
 *
 * @author Usuario
 */
class FormFieldGenerator {

    //put your code here
    public static function generateField($form, $model, $model_element) {

        if (strpos(strtolower($model_element), 'date') !== false || strpos(strtolower($model_element), 'fecha') !== false) {
            return self::generateDatePickerField($form, $model, $model_element);
        }   /* else if (strpos(strtolower($model_element), 'comuna') !== false) {
            return self::generateComunaSelectField($form, $model, $model_element);
        } */    else {
            return self::generateTextInputField($form, $model, $model_element);
        }
    }

    protected static function generateTextInputField($form, $model, $model_element) {
        return $form->field($model, $model_element)->textInput(['maxlength' => true,
            'placeholder' => $model->getAttributeLabel($model_element), 'class' => 'uppercase form-control', 'style' => 'width:400px']);
    }
/*
    protected static function generateComunaSelectField($form, $model, $model_element) {
        return $form->field($model, $model_element)->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Comuna::find()->asArray()->all(), 'id', 'nombre'),
                    'options' => ['placeholder' => 'Seleccione una Comuna...'],
                    'theme' => 'default',
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
        ]);
    }
*/
    protected static function generateDatePickerField($form, $model, $model_element) {
        return $form->field($model, $model_element)->input("text", ["class" => 'datepicker form-control', 'style' => 'width:400px']);
    }

    public static function formatAttributes($attributes) {
        $format_attributes = [];
        foreach ($attributes as $key => $value) {
            if (strpos(strtolower($key), 'date') !== false || strpos(strtolower($key), 'fecha') !== false) {
                $format_attributes[strtoupper($key)] = str_replace(' ', ' de ', \Yii::$app->formatter->asDate($value, 'php:d mm Y'));
            } else if (strpos(strtolower($key), 'comuna') !== false) {
                $comuna = \app\models\Comuna::findOne($value);
                if (isset($comuna)) {
                    $format_attributes[strtoupper($key)] = mb_strtoupper($comuna->nombre);
                } else {
                    $format_attributes[strtoupper($key)] = "";
                }
            } else {
                $format_attributes[strtoupper($key)] = $value;
            }
        }
        return $format_attributes;
    }

}
