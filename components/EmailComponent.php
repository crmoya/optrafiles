<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

/**
 * Validates that a rut has a correct format and is a valid rut 
 *
 * @author fvasquez
 */
class EmailComponent {

    public static function sendDirectEmail($from, $from_name, $to, $subject, $templates, $variables) {
        $mailer = \Yii::$app->mailer;
        $message = $mailer->compose($templates, $variables)
                ->setFrom([$from => $from_name])
                ->setTo($to)
                ->setSubject($subject);

        return ($message->send());
    }

    public static function sendQueueEmail($from, $from_name, $to, $subject, $templates, $variables) {
        $queue = new \app\models\EmailQueue();
        $queue->compose_view = serialize($templates);
        $queue->compose_data = serialize($variables);
        $queue->to_email = $to;
        $queue->subject = $subject;
        $queue->from_email = $from;
        $queue->from_name = $from_name;
        $queue->date_published = date("Y-m-d");
        $queue->max_attempts = 3;  //No of try to send this mail
        $queue->attempts = 0;
        $queue->success = 0;  //will be set to 0 on send.

        return $queue->save();
    }
    public static function sendEmailFromDefaultService($from, $from_name, $to, $subject, $templates, $variables) {
        if (\Yii::$app->params['directEmailSend']) {
            return self::sendDirectEmail($from, $from_name, $to, $subject, $templates, $variables);
        }
        else {
            return self::sendQueueEmail($from, $from_name, $to, $subject, $templates, $variables);
        }
        
    }
}
