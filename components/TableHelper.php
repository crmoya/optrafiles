<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

/**
 * Description of TableHelper
 *
 * @author crist
 */
class TableHelper {

    const SIN_LOGO = true;
    const LLEVA_CERTIFICADO = true;

    /**
     * Description of TableHelper
     *
     * @param array $cabeceras Entregar las columnas de datos con, al menos, el atributo name
     * @param array $datos Arreglo de datos
     * @param ? $view Vista a la que pertenece la tabla
     * @param String $clickUrl Puede ser null, corresponde a la URL a la que irá si se hace clic sobre una fila de la tabla
     * @param array $customize Es un arreglo al que se le pueden agregar los siguientes valores:
     * sin_logo: boolean; si NO lleva logo va en false (se imprimirán los datos de la notaría), de lo contrario es true.
     * lleva_certificado: boolean, footer de la última página, que indicará cuántas anotaciones practicó el funcionario en el día.
     * fecha_desde: String; fecha desde utilizada en página como filtro de búsqueda
     * datos_totales: array; si el reporte debe llevar una suma de datos totales, se agrega este arreglo que insertará una última fila de totales.
     * fuente_default: int, aplica al tamaño de la fuente en general.
     * fuente_table_header: int, aplica al tamaño de la fuente de los encabezados.
     * verPDF: boolean, si se agrega este valor, se ocultará el botón PDF para imprimir.
     * verExcel: boolean, si se agrega este valor, se ocultará el botón Excel para imprimir.
     * columnIndexOrder: int, indica el index de la columna con la cual se quiere ordernar por defecto.
     * columnOrderType: string, indicar valor "asc" si se quiere ordenar ascendentemente. Por defecto, tomará valor "desc"
     * render: boolean, si se agrega este valor, entonces se abrirá un popup para insertar contenido.
     * actions: array, $type indica el tipo como delete, update, etc. $url indica la URL base controller/action.
     * 
     * 
     * ATENCIÓN: SIEMPRE SE DEBE ENVIAR COMO LA PRIMERA COLUMNA EL IDENTIFICADOR DEL MODELO QUE SE ESTÁ LISTANDO!!
     * @author crist
     * @updates leo
     */
    public static function createTable($cabeceras, $datos, $view, $clickUrl = null, $customize = null) {
        $loading = "<div id='loading' class='row'><div class='col-md-4'></div><div class='col-md-8'><br/><br/><br/><br/>"
                . "<img src='" . \Yii::getAlias("@web") . "/images/Gear.gif'/></div></div>";
        echo $loading;

        $exportableColumns = array();
        $invisibleColumns = array();
        $segundo_header_honorarios_anulados = false;
        $verPDF = "true";
        $verExcel = "true";
        if (isset($customize['verPDF'])) {
            $verPDF = "false";
        }
        if (isset($customize['verExcel'])) {
            $verExcel = "false";
        }

        $orderBy = isset($customize['columnIndexOrder']) ? $customize['columnIndexOrder'] : 1;
        $orderType = isset($customize['columnOrderType']) ? $customize['columnOrderType'] : "desc";
        $tabla = "<div class='envoltorio-tabla'>"
                . "<table style='width:100%;height:100%;display:none;' data-order='[[ " . $orderBy . ", \"$orderType\" ]]' id='tabla' class='display nowrap' style='width:100%'>";
        $tabla .= "<thead>";
        $tabla .= "<tr>";

        $col = 0;
        foreach ($cabeceras as $th) {
            if (gettype($th) == 'array') {
                $atributos = "";
                $atributos_input = "";
                if (isset($th['width'])) {
                    $ancho = 0;
                    if ($th['width'] == 'small')
                        $ancho = 30;
                    if ($th['width'] == 'medium')
                        $ancho = 80;
                    if ($th['width'] == 'big')
                        $ancho = 150;
                    $atributos .= "style='width:" . $ancho . "px'";
                } else {
                    $atributos .= "style='width: auto'";
                }
                if (isset($th['exportable'])) {
                    if ($th['exportable']) {
                        $exportableColumns[] = $col;
                    }
                } else {
                    $exportableColumns[] = $col;
                }
                if (isset($th['visible'])) {
                    if (!$th['visible']) {
                        $invisibleColumns[] = $col;
                    }
                }
                if (isset($th['format'])) {
                    if ($th['format'] == 'date') {
                        $atributos_input .= "class='datepicker'";
                    }
                }
                if (isset($th['name'])) {
                    $tabla .= "<th " . $atributos . "><input $atributos_input id='" . $th['name'] . "' type='text' placeholder='" . $th['name'] . "' /></th>";
                    //$tabla .= "<th ".$atributos.">".$th['name']."</th>";
                }
            } else {
                $tabla .= "<th><input id='" . $th . "' type='text' placeholder='" . $th . "' /></th>";
            }
            $col++;
        }
        if (isset($customize['actions'])) {
            $tabla .= "<th>Acciones</th>";
        }
        $tabla .= " </tr>"
                . "</thead>";
        if (isset($customize['segundo_header_honorarios_anulados'])) {
            $segundo_header_honorarios_anulados = true;
        }
        $tabla .= "<tbody>";

        $exportableColumns = "[" . implode(",", $exportableColumns) . "]";
        $invisibleColumns = "[" . implode(',', $invisibleColumns) . "]";

        $cantidadDatos = count($datos);
        foreach ($datos as $modelo) {
            $tabla .= "<tr>";
            $i = 0;
            $params = "";
            foreach ($modelo as $celda) {
                $attributes = "";
                $width = "";
                $format = "";
                if (isset($cabeceras[$i]['width']))
                    $width = $cabeceras[$i]['width'];
                if (isset($cabeceras[$i]['format'])) {
                    $format = $cabeceras[$i]['format'];
                    if ($format == 'number') {
                        $attributes .= "style='text-align:left;' class='number'";
                    } elseif ($format == 'moneda') {
                        $attributes .= "style='text-align:right;' class='number'";
                        $celda = number_format($celda, 0, ',', '.');
                    } elseif ($format == 'percentage') {
                        $attributes .= "style='text-align:right;' class='number'";
                        $celda = number_format($celda, 1, ',', '.');
                    } elseif ($format == 'texto') {
                        $attributes .= "style='text-align:left;' ";
                    }
                }
                if (isset($cabeceras[$i]['suffix'])) {
                    $tabla .= "<td $attributes class='dots-" . $width . "'>" . $celda . " " . $cabeceras[$i]['suffix'] . "</td>";
                } else if (isset($cabeceras[$i]['prepend'])) {
                    $tabla .= "<td $attributes class='dots-" . $width . "'>" . $cabeceras[$i]['prepend'] . " " . $celda . "</td>";
                } else {
                    $tabla .= "<td $attributes class='dots-" . $width . "'>" . $celda . "</td>";
                }

                if (isset($cabeceras[$i]['param1'])) {
                    $params = "?" . $cabeceras[$i]['param1'] . "=$celda";
                }
                if (isset($cabeceras[$i]['param2'])) {
                    $params .= "&" . $cabeceras[$i]['param2'] . "=$celda";
                }
                $i++;
            }
            if (isset($customize['actions']) && gettype($customize["actions"]) == 'array') {
                $actions = "";
                foreach ($customize['actions'] as $action) {
                    $tipo = $action['type'];
                    $actionUrl = $action['url'];
                    $span = "";
                    if ($tipo === 'delete') {
                        $span = '<span class="glyphicon glyphicon-trash" title="Eliminar"></span> ';
                    } else if ($tipo === 'update') {
                        $span = '<span class="glyphicon glyphicon-pencil"></span> ';
                    } else if ($tipo === 'folder') {
                        $span = '<i class="fa fa-folder-open"></i> ';
                    } else if ($tipo === 'create') {
                        $span = '<i class="fa fa-folder-plus"></i> ';
                    } else if ($tipo === 'upload') {
                        $span = '<span class="glyphicon glyphicon-cloud-upload" title="Enviar a Revisión"></span> ';
                    } else if ($tipo === 'download') {
                        $span = '<span class="glyphicon glyphicon-cloud-download" title="Enviar a Revisión"></span> ';
                    }

                    //$actions .= '<a href="' . $actionUrl . $params . '" class="urlDatos">' . $span . '</a>';
                    //TODO: CONTINUAR ACÁ!!
                    if (isset($action['modal']) && $action['modal'] === TRUE) {
                        $actions .= \yii\helpers\Url::to($actionUrl . $params);
                    } else {
                        $actions .= \yii\helpers\Html::a($span, $actionUrl . $params, ['data-method' => 'post']);
                    }
                    //\yii\helpers\Html::a('<i class="glyphicon glyphicon-repeat"></i>', [$this->context->action->id]);
                    /*
                      '<a href="/escritura-nomina/delete?nomina_id=45&escritura_id=3644" title="Eliminar" aria-label="Eliminar" data-pjax="0" '
                      data-method="post" data-confirm="¿Estás seguro de querer borrar este elemento?" role="modal-remote" data-request-method="post"'
                      data-toggle="tooltip" data-confirm-title="¿Estás seguro?" data-confirm-message="¿Estás seguro de querer borrar este elemento?">'
                      <span class="glyphicon glyphicon-trash"></span></a>';
                     */
                }
                $tabla .= "<td class='dots-" . $width . " kv-align-center'>$actions</td>";
            }
            $tabla .= "</tr>";
        }
        if (!empty($datos) && isset($customize['datos_totales'])) {
            $tabla .= "<tfoot><tr>";
            $i = 0;
            $attributes = "style='text-align:right; font-weight: bold'";
            foreach ($customize['datos_totales'] as $celda) {
                if (is_array($celda)) {
                    $valor = $celda['dato'];
                    if (isset($celda['format'])) {
                        $format = $celda['format'];
                        if ($format == 'number') {
                            $attributes = "style='text-align:right; font-weight: bold' class='number'";
                        } elseif ($format == 'moneda') {
                            $attributes = "style='text-align:right; font-weight: bold' class='number'";
                            $valor = number_format($valor, 0, ',', '.');
                        } elseif ($format == 'percentage') {
                            $attributes = "style='text-align:right; font-weight: bold' class='number'";
                            $valor = number_format($valor, 1, ',', '.');
                        }
                    }
                } else {
                    $valor = $celda;
                }
                //if (is_float($valor) || is_numeric($valor)) {
                if (isset($celda['suffix'])) {
                    $tabla .= "<td $attributes>" . $valor . " " . $celda['suffix'] . "</td>";
                } else if (isset($celda['prepend'])) {
                    $tabla .= "<td $attributes>" . $celda['prepend'] . " " . $valor . "</td>";
                } else {
                    $tabla .= "<td $attributes>" . $valor . "</td>";
                }
                /* } else {
                  $tabla .= "<td style='text-align:right; font-weight: bold'>" . $celda . "</td>";
                  } */
                $i++;
            }
            $tabla .= "</tr></tfoot>";
        }
        $tabla .= "</tbody>";
        $tabla .= "</table>"
                . "</div>";
        echo $tabla;

        $location = $clickUrl != null ? \yii\helpers\Url::to([$clickUrl]) : "";

        $fecha = date("d-m-Y");
        $hora = date('H:i');

        $cabeceraIzquierda = "";
        $image = "";
        $fuenteDefault = 7;
        $fuenteTableHeader = 7;

        $sin_logo = "false";
        $certifica = "false";
        $certificoQue = "";
        $certificoQue2 = "";
        $fechaCertifico = "";
        if (isset($customize['sin_logo']) && $customize['sin_logo']) {
            $cabeceraIzquierda = "Carlos Miranda Jim\énez Notario P\úblico                  O\'Higgins 820 - Concepci\ón www.notariamiranda.cl";
            $sin_logo = "true";
        }
        if (isset($customize['lleva_certificado']) != null && $customize['lleva_certificado']) {
            $certificoQue = "CERTIFICO QUE EL D\ÍA DE HOY PRACTIQU\É ";
            $certificoQue2 = " ANOTACIONES, CONCEPCI\ÓN ";
            $fechaCertifico = $customize["fecha_desde"];
            $fecha = $fechaCertifico;
            $fuenteDefault = 10;
            $fuenteTableHeader = 11;
            $certifica = $customize['lleva_certificado'];
        } else {
            $cantidadDatos = "";
        }
        if (isset($customize['fuente_default'])) {
            $fuenteDefault = $customize['fuente_default'];
        }
        if (isset($customize['fuente_table_header'])) {
            $fuenteTableHeader = $customize['fuente_table_header'];
        }
        $render = '';
        if (isset($customize['render'])) {
            $render = $customize['render'];
        }
        $openNewWindow = '';
        if (isset($customize['openNewWindow'])) {
            $openNewWindow = $customize['openNewWindow'];
        }

        $script = <<< JS
                $(document).ready( function () {
                    var clickUrl = '$location';
                    var render = '$render';
                    var openNewWindow = '$openNewWindow';

                    $.datepicker.regional['es'] = {
                        closeText: 'Cerrar',
                        prevText: '<Ant',
                        nextText: 'Sig>',
                        currentText: 'Hoy',
                        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                        weekHeader: 'Sm',
                        dateFormat: 'dd/mm/yy',
                        firstDay: 1,
                        isRTL: false,
                        showMonthAfterYear: false,
                        yearSuffix: ''
                    };
                    $.datepicker.setDefaults($.datepicker.regional['es']);
                    $( ".datepicker" ).datepicker({ 
                        "dateFormat": "dd-mm-yy"
                    });
                    $.fn.dataTable.moment( 'DD-MM-YYYY' );
                    
                    $('#tabla td').attr("data-toggle",'tooltip');
                    $('#tabla td').attr("data-placement",'top');
                    $('#tabla td').mouseenter(function(e){
                        $(this).attr('title',$(this).text());
                    });
                    $('[data-toggle="tooltip"]').tooltip();

                    var table = $('#tabla').DataTable({
                        dom: 'Bfrtip',
                        searching: true, 
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
                        },
                        buttons: [
                            {
                                extend: 'pdfHtml5',
                                footer: true,
                                enabled: $verPDF,
                                action: function(e, dt, button, config) {
                                    var before = $('#tabla thead').html();
                                    $('#tabla thead th input').each( function () {
                                        var title = $(this).attr('placeholder');
                                        $(this).parent().html( title );
                                    } );
                                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(this, e, dt, button, config);
                                    $('#tabla thead').html(before);
                                    location.reload();
                                },
                                exportOptions: {
                                    columns: $exportableColumns
                                },
                                orientation: 'landscape',
                                customize: function ( doc ) {
                                    var title = doc.content[0].text;
                                    doc.defaultStyle.fontSize = $fuenteDefault;
                                    doc.styles.tableHeader.fontSize = $fuenteTableHeader;
                                    doc.styles.tableHeader.fillColor = '#3c8dbc';
                                    doc.styles.tableHeader.color = 'white';
                                    doc.styles.tableFooter.fontSize = $fuenteTableHeader;
                                    doc.styles.tableFooter.fillColor = '#3c8dbc';
                                    doc.styles.title.fontSize = 18;
                                    if('$segundo_header_honorarios_anulados' != '') {
                                        
                                        doc.content[1].table.body.unshift([{}, {text: "BOLETA ANTIGUA", style: {fontSize: $fuenteTableHeader, width: '40%', paddingBottom: '10'}}
                                            ,{},{},{},{text: "BOLETA NUEVA", style: {fontSize: $fuenteTableHeader}},{},{},{}]);
                                    }
                                    //doc.content[0].text = doc.content[0].text.trim();
                                    //doc.content[1].table.width = '100%',
                                    if ($sin_logo) {
                                        doc.content[0].text = '';
                                        doc.content.splice( 0, 1, {
                                            margin: [ 0, 10 ],
                                            alignment: 'center',
                                            columns: [
                                                {
                                                  width: '18%',
                                                  text: '$cabeceraIzquierda',
                                                  style: {fontSize: 10}
                                                },
                                                {
                                                  width: '*',
                                                  text: title,
                                                  style: 'title'
                                                },
                                                {
                                                  width: 100,
                                                  text: 'Fecha: $fecha',
                                                  style: {fontSize: 10}
                                                }
                                            ],
                                        });
                                    } else {
                                        doc['footer']=(function(page, pages) {
                                            return {
                                                columns: [
                                                    {
                                                        alignment: 'right',
                                                        text: [
                                                            'Página ',
                                                            { text: page.toString(), italics: true },
                                                            ' de ',
                                                            { text: pages.toString(), italics: true }
                                                        ]
                                                    }
                                                ],
                                                margin: [10, 0]
                                            }
                                        });
                                        doc.content.splice( 0, 0, {
                                            margin: [ 0, 0 ],
                                            alignment: 'left',
                                            messageBottom: 'Reporte generado el $fecha',
                                            //image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAAzCAYAAADLqmunAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH4gcBAwAJ5l6toQAAIABJREFUaAUBAzD8zwH/////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOLi5ADL0NUANDIqAB8cHQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJqjrgDLzdIAm5CAAP8A/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAAAAAAAEdYbwD9/P4Am5CAAAAAAAABAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgD+AD9OZwA6OC4AaWFTAP8A/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHaDlADy9/gAmIZ0AP8A/wABAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP////////////////////////////////////////////////////////////7+/9PY3f8AJED/8vPz//7//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQBxg5UAlX1sAA0MDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADa3uAAKEhlAP3ZugABAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAhJamADgwKwABAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//v4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAQABAMHP2wBjTDoAAAD/AAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD3+PkA297iAKuxugDHzNQA7/r9AAf+/gAnJRwAS0U+AEA5MgAUEhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//////////////////////////////////////////////////////T29/8AGz3/z9TW///////////////////////////////////////////////////////////////////////////////////////S1df/iY6W//Lz8//4+fr//f7+///////9/v7/+fn6/+nr7f9KVWn/e4OW////////////////////////////////////////////////////////////////////////////////////////////////////////////AP////////////////////////////////////////////////////+vucH/ABc5//7+/f//////////////////////////////////////////////////////////////////////////////////////7+/w//////////3////////////////////////////+//7//////62zuf9GU2n/+fr6//7//v///////////////////////////////////////////////////////////////////////////////////////////wD/////////////////////////////////////////////////////Y3OE/wsvTf///////////////////////////////////////////////////////////////////////v7+//b29v/+/v7//////////////////////9fX1/+3t7f/////////////////+fn5/8XFxf//////5enr/1Fec/////////////////////////////////////////////////////////////////////////////////////////////////8CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANXa4wAYFRIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5ubmAL29vQC6uroA7OzsAAAAAAAAAAAAAAAAAPHx8QDc3NwAFBQUAAAAAAAAAAAA9/f3AL6+vgDt7e0AAAAAABoWFABTTkAAZXKHAAD+/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADt7e0ABREQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADw8PAA4uLiAO7u7gAAAAAAExMTAOfn5wD///8AOTk5ABUVFQAAAAAAAAAAAOHh4QDY2NgAIyMjACkpKQAAAAAA5eXlAM/PzwAFBQUAQkJCAAAAAAAAAAAAW1NMAK22xAD3+/wACQcEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8BAPv7/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADh4eEA0tLSABYWFgA+Pj4AAgICAAAAAADj4+MASUlJAAEBAQAAAAAAAAAAAN7e3gDd3d0A/Pz8ACYmJgAAAAAA7e3tANLS0gAEBAQARUVFAAICAgAAAAAAAAAAAAAAAAB3aVsAbnyRAJuLdwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0PCwDs7e8AAAAAAAAAAAAAAAAAAAAAAAAAAAD///8Az8/PACgoKAA3NzcAAgICAAAAAADW1tYAFhYWAAEBAQAAAAAAAAAAAPT09AD19fUAFRUVAC8vLwADAwMA5eXlANjY2AAKCgoAJCQkAAICAgAAAAAAAAAAAAAAAAAAAAAAMjAoAJ+grQD5+/sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoIx4A8e3yAP///wAAAAAAAAAAAAAAAAAAAAAAAQEBACEhIQAPDw8AAAAAAAAAAAAAAAAA6OjoACkpKQAAAAAAAAAAANbW1gDe3t4ALS0tAOTk5AAcHBwA29vbAOTk5AAkJCQAAQEBACQkJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUEBQA4Ny8A3ODlAAAA/gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASEM4AAD5+QDO1dkAAAAAAAAAAAAAAAAAAAAAAAAAAAAvLy8AFhYWAAAAAAAAAAAA9fX1AAsLCwALCwsA/Pz8ANra2gD+/v4AKysrANfX1wAgICAA2dnZAOrq6gA3NzcAysrKADExMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANTQpAO3s8AD/AP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEtCPQAADxEAr7PAADMsKAAAAAAAAAAAAAAAAAAAAAAA+/v7APHx8QD39/cA/f39ANvb2wAaGhoA3NzcAN3d3QAYGBgACQkJAODg4ADk5OQAFhYWAOzs7AAfHx8A3t7eABISEgAHBwcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgGBQD5+vsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASDwwAHxsVALjJ0ACEAAAA/f3+APPz8gD09PQA+fn5AAICAgAHBwcABwcHAP39/QAVFRUAIyMjANLS0gAoKCgAGhoaAAAAAADg4OAAAgICAPT09AA1NTUA/Pz8AMnJyQArKysA5ubmAPz8/AAEBAQACwsLAAsLCwAKCgoAAAAAAAAAAADb4egATkcHAAEAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/v/9ADssJQCpxMwA5unqAPz49gAPEREAGhoaAAICAgAAAAAAAAAAAAAAAADY2NgALCwsALy8vAApKSkAHh4eAAEBAQD39/cA2NjYAAcHBwA7OzsAAAAAAOLi4gAGBgYAGxsbAAAAAAAAAAAA////APf39wDw8PAA5+fnAA4ODgAVFRQAwcPPAI+AbwAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//////////////////////////////////////////////////////7+/v/a3uD/ABo8/11ufv//////////////////////////////////////uLi4/7S0tP/u7u7/////////////////0dHR/7S0tP/5+fn////////////AwMD/4+Pj////////////////////////////5+fn//7+/v/8/Pz/29/k/xgqQv/29fX//f39////////////////////////////////////////////////////////////////////////////AgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEAJSEfAFZQQwCjvMsA4+fqAAAAAAAAAAAAAAAAAAAAAAAAAAAA2dnZAPz8/ABHR0cAERERAAAAAAAAAAAA8fHxAODg4AA2NjYABgYGAAAAAAAAAAAA+vr6ABEREQAAAAAAAAAAAAAAAAAAAAAAzs7OAAEBAQABAQEAAwMDAK21vwChmIcACQoKAP39/QD///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8A/wCEcmAAAwIHAHqLmQAAAAAAAAAAAAAAAAAAAAAA/Pz8ANvb2wA9PT0ABAQEAAAAAAAAAAAA////AM7OzgAWFhYAFBQUAAAAAAAAAAAAAAAAAPr6+gAFBQUAAAAAAAAAAAD///8Av7+/ABQUFAAXFxcAAAAAAPb39wCip7QART01AP///wAFBQUAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/////////////////////////////////////////////////////////////////////4qXov8AI0X/nKSt//3+/P//////+fn5/7y8vP/Z2dn///////////////////////Dw8P+tra3/9fX1//////////////////////++vr7/2tra///////6+vr/vr6+/+bm5v///////f/+//////82Ql3/6Ovt//T08//y8vP/////////////////////////////////////////////////////////////////////////////////AP//////////////////////////////////////////////////////////////////////////O1Fp/wcyUv/g4uX//////8XFxf+3t7f/+/v7/////////////////////////////v7+////////////////////////////7u7u/7a2tv+8vLz/tbW1/+Hh4f////////////j6+v9pc4P/zM7S/+3v7v+Ql57//////////////////////////////////////////////////////////////////////////////////////wD///////////////////////////////////////////////////////////////////////////////8SM03/AiJJ/9na3f+wsLD/4+Pj///////////////////////////////////////////////////////////////////////+/v7/8fHx//7+/v/+/v7/+vn6/7rCzP9JU2X//P7///////9caHb////+//////////////////////////////////////////////////////////////////////////////////////8A////////////////////////////////////////////////////////////////////////////////6+3t/womRP8OM1D/xcjL//z+/P//////////////////////////////////////////////////////////////////////////////////////8fHz/9fc4P/g4uX///////r7+v8zQVb/8fPz////////////////////////////////////////////////////////////////////////////////////////////AP///////////////////////////////////////////////////////////////////////////////////v/8/fv/MVBn/wAjRf+vt7/////////+/v///////////////////////////////////////////////////////////////////////////////v/9//3//////7zDy/8TKkb/3eHj//7+/////////////////////////////////////////////////////////////////////////////////////////////wD//////////////////////////////////////////////////////////////////////////////////////v////////9kcIP/ATVS/05dcP/6+vr//v7+///////////////////////////////////////////////////////////////////////+/v7//Pz6/1tsgP8ULUz/+Pr3//////////////////////////////////////////////////////////////////////////////////////////////////////8A//////////////////////////////////////////////////////////////////////////////////////////////7//////8vQ1f8gPVb/AylG/19yhf/o6e3//////////////////v/////////////////////+/f///v7////////////19PL/bn+P/wcnRP9vf5D/9fX1////////////////////////////////////////////////////////////////////////////////////////////////////////////Af////8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AP8AAQABAJ+osgCSprYA0MvPADk3NQBeVkUAMiskABoXFAAODQ0ABQMDAP3+/gD29vYA5ejsANXb4QCjsLwAusDMAEA7MACOemMAMSwrAAD//wAAAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPDy8wCRnqsAnaq3AOTt8AD/AgYAAPv+AAIBAAD+AP0AAAEDAAEC+wAkGBcAcGJTAGNXSgAHBwgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACRfszQAAAQDklEQVQAAAAAAAAAAAAB/////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAABAAAAAADz9PQA7O3xAPP29wD6/PsABgUFAA4LCQAVExEACwoKAAAAAAAAAP8AAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf////8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAAAP8A/wAAAP8A/wAAAAAAAAAAAAAA/wD/AAAAAAAAAP///gAAAAAAAAAAAAAAAAAAAAAA///+AP0A/wD/AP4AAAAAAAAAAAAAAAAAAAD/AAAAAAAAAAAAAAD/AAAAAAD///wAAAAAAAAAAAAAAAAA///7AAD+/QAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAP8AAP7/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAD//wAAAAAAAAAAAAAAAAD//v4AAAAAAP8AAAAAAAAAAAAAAAAAAAAC0dPTACdCXwATP1oAb3+RAAEAAAABAAEAzNDTACk+VgDb3uAAAQAAAAAAAABvf48AQFRrAL/HzgDFzdIAY3SHAFlugQD59vcAAQACAG5+kgB1hZUAvMPIACU9VwAPNlUAnamxAK62wAAeNU8AAQEEAAAAAAAAAAAAAAAAAMfM0wCOm6gAAAAAAAAAAAAAAAAA+vr6ANPX2QAkSGEAIkZhAMLJzwCvt74AME1oAFJmeADz8/UA/v7/ANfY2AA7VGoAFD1XAKKstQDx8vMAAAAAAAAAAAAAAAAA7vDxACQ4UwAA//4AAAAAAAAAAAAAAAAAAP////9AWWz/H0Rf/wAqSf96iJX////+//////9CT2j///////z9+/8sQVj/AB5B/97h5f////////7+//Dw8f8BLE7/IDxX//////+BiJX///////////8TLEj/AB5C/8/V2f//////lqKt//z+///////////+//j6+v8IJkL/ACRG//P19P/+/v7/////////////////G0Fe/xA5Vf///////////6Owuv8DNFb/fI2c////////////OFBo/wAqSv/W3uH//////////////////////3aHl/8BNlf/rrjA/////////////////wIAAAAA+Pr5AN26ngAWCQUAiay/AG17jwAAAAAA/f4AAAAAAADGzNQA1vT8ABkeFgAhHhoAAAAAAAABAAAPDw4AHgwDAODx9wCHkp4Ae3VlAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAGdcTgABAQAAAAAAAAAAAQC0uMEAeWpbAAAHAgBeb4gAAAH/AAAAAAAAAAAAAAAAAAABAAABAAAAAAAAAAAAAADY3+EA/fv7ACUdFgAAAAAAAAAAAAAAAAAAAAAA//7/AAAAAAAAAAAA/wD+ANXa3gDv6egAAPn3AF90hgAAAAAAAAAAAAAAAAACAAAAAAIAAAADAQIA2b2iACILBgCUrrwAaXeLAAQGAAAAAAAA7fP2AP7/AAAMCAgAAAAAAAAAAAAAAP8AAAAAAA8GBwAABgQA5ubsAAMCBQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAQQAAgAAAAAAAADt8fIAz8/VAH5vYgAeFxUAscrSAOnq7QAAAP4AAAAAAAAAAAAA/wEA+//+AJukrQAtSmcAk6G1AMebgABeVUwAAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAQCdn6sAmo+AAFg7LwDz/wkAdIGRAAAA/wAAAAAAAgAAAAD//wAAAAAAABAPDgDawKUALiEZAJi3wwDy+f4AAAAAACcdGQAB+PcA5OvtAAAAAAAAAAAA/wACAAAA/wDh7uwAAPHzAExIPAD/AP8AAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAA/wD8//4Ab3SGABchHQCNnKkANyghAP4BAgA/V20AAAACAAAAAAAAAAAAAAAAAAUAAQBlXFMAMCIZAPL5+gCKl6QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAAAADL0NUA/AMBAI2bqQA0MiwABgcBAJy4xgD3+foAAAAAAAD/////Nk9j//////////3///////////80T2T/NU1m////////////lpyo/wAfRf+dpq7////////////N0dX/AC9S/3+NnP/////////+////////////FC1I/wAeQv/Q1tr/////////////////6Ozt/7W8xf/+/vb//P38//////8uSWT/ATNS/7nBx////////////xtBYP8ROVT///////////9Tanz/ATVT/26AkP///////////zlQaf8AKkr/2eDi///////4+Pf/RVRo///////7/fv//Pz5/29+jP8AL1P/RFhs//////8DPENHgPn4/AD5+PYAJCIfAAIBAgAAAP8AYFJIAOLf4ABDPzgAAAAAADUyLABOQzQAxs/ZANjW3QDN0dgAx9LZAINoVABgV00AAAAAAAAAAQAAAAAAvsPGAAgKDQA1NCsA/vn0AB4aGQAKCgkA6OnpAJCcqwAJCQUAFxQVAA8NDQDd3t8ACQ0IACkcGQDl5OcAFxUQANHU1wAHBQcALyUhAP779gAaFxUAVko+APTr7gD4/P0A+/f3AO/u7QD6/v8AMiojAPjz8wAbGBUApK22ABQXGAAVEQsAGBUVAPn7/QDz8/MAGxMPABUVEwAHAwAAAf////8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASnp6cAycnJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADk5OQAB/////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAA6enpAMfHxwD09PQAXFxcALOzswBNTU0AsbGxAAUFBQBDQ0MA4+PjACMjIwCysrIAEhISAAAAAAAWFhYAzMzMAFZWVgAFBQUA09PTAC0tLQDT09MACgoKAOPj4wDf398ACQkJAFhYWADPz88AICAgAPPz8wAODg4ABQUFANjY2ADMzMwAGBgYACEhIQAtLS0A19fXACoqKgAAAAAAt7e3ABwcHAAtLS0A5OTkABwcHACysrIAAgICAAYGBgA8PDwA5eXlABkZGQDo6OgA0dHRABsbGwDn5+cA+fn5ABwcHAA8PDwAAAAAAAIAAAAAAAAAALKysgBQUFAAXFxcAM7OzgAREREAAAAAAPPz8wAKCgoAAgICAPv7+wDY2NgAOzs7AD09PQAKCgoA/f39AAAAAAAFBQUAAAAAAMDAwAD19fUA39/fAPPz8wD4+PgA9/f3AAsLCwAAAAAA8fHxAOTk5AAODg4Aq6urAOvr6wAAAAAAHh4eAE9PTwDd3d0Ay8vLAMPDwwAAAAAAAAAAAPj4+AAAAAAA5ubmAL+/vwDz8/MA39/fAPj4+AAMDAwA/Pz8ALe3twDj4+MABAQEAP7+/gAKCgoAUVFRAAsLCwA8PDwAAAAAAAAAAAACAAAAAAAAAAAFBQUA8fHxAPHx8QDz8/MAERERAL6+vgAUFBQA6enpAP///wD09PQACgoKAMrKygAAAAAA5eXlAA4ODgAXFxcA3NzcAPv7+wBPT08Al5eXAEVFRQDt7e0A9/f3AA4ODgDy8vIA3t7eACQkJAC9vb0A+fn5AGVlZQC5ubkAAgICAPb29gD09PQA7u7uAOjo6AA2NjYAvLy8AAAAAAD9/f0A+/v7ANvb2wAiIiIAu7u7AAoKCgDo6OgANTU1APj4+ABhYWEAy8vLAAICAgASEhIALCwsAJqamgAzMzMA9vb2AAAAAAAAAAAAAf////8AAAAAAAAAAM/PzwAICAgAISEhAAgICADa2toAFRUVAAgICAABAQEA1NTUABgYGAAMDAwA2dnZAC0tLQDw8PAA7+/vACsrKwD9/f0A+/v7AAcHBwABAQEA7u7uAP39/QD+/v4ACgoKAPLy8gAbGxsA7u7uAAEBAQAREREAAAAAAPLy8gDf398ABAQEACoqKgABAQEAAAAAAOLi4gAcHBwAwsLCADU1NQD09PQAFhYWAAAAAADu7u4A+Pj4AOXl5QAvLy8AAQEBAAUFBQD39/cA4ODgAP7+/gD///8A+Pj4ABMTEwAiIiIAAAAAAIfUDZ42rmO3AAAAAElFTkSuQmCC'
                                            image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABMCAYAAADtJ6CEAAAABHNCSVQICAgIfAhkiAAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M26LyyjAAAABV0RVh0Q3JlYXRpb24gVGltZQAzMC8zLzE22nqzUAAAGzFJREFUeF7tXAd4VVW2/m8q6SQhEEKo0kMooUgZOkgg4oAIgiM+RVGcQfA9BAfHcVAcnwL6dHCUMoogZQCl9yKhE4qAICgQWnrvPffut/99zsm9CUnQSS76ffh/ubn37LPPLv9ae+21z9nrmIQE7AiBUpjgpH6fOfc9Io8ex9NPPAZ/37oq7X6B3Ykmom/FYMyEF9GzZ1cs+uBNmWKRHwd17r4Bia49mIWwWORHP5R4Zc5CAY/W4uDJ83pKqTxtk+E+Qa1qtEUaClkkHE3acZs+4xEXG4uUK/vg5lpHS7xPUavj10GKzCDZJagPMnOykXvriCLZoszF/Qu72OjgThFIzspB8c1D6tioQJfBfYlan5HG/2kO4i5GI/XCNnUsjYki+H4mmagVoi26ym7ddwxrP1mOtV8vhLeXt24s7m+TYaCWTIfmrpka9UWHLi1wYdtyecxi73c9tqKWTIcD5ry/FEhKw7K3X9XTfiPZFrU2GZrqdkHzTm1x/eAaTZn5uc/WJNWhVqhYuUFOfIUOeHHcUD1F4jeSy6EGGi0My4w2Q/6AK2euI+7yJgQFNrgn5lkuQGFSwjRVWxW7Z1IZftkhVgOiNZrjk1LQqMNIBLdshJjjX2un7jFOnP8BF85eQkJ2HhydHRDk549OXVohrPUDeo5fHjUW8bqt+6W4nPHw7zrrKfcGkcdPYcxTL6Fxl4fxtzfn4cLN2ygtLUFudj4OHIjExImvwDm4BwY8+jwOR53Xr5KgWtXOtPTzQI2uCfqNfUnAp6vYtv+gnmJfXLl2XTz3p1fE3A+WirjEJD21any1fb9oFjpYtOo8UkTHJKq0X+KWVo2INlvMwr39MAGvbiI5NU1PtR+SkxLF1l37hMVSqqeQsspp01Kt59ZuPyjV2E/8Y8kaPeXeokZEX7sZI9Cgl0Cj3nqKfWHhLVgdVVNshVn/NlBQXCLcG3YXz706Tx1b7shhP9TIRl+8dFXaHgd41fNVx7xNag/Idqpvk+Y+KPAXP1lZWdiwYQN27Nih0m3BzvHaPXv2ID4+HnWcnZAXfxJrV6zFtNf/T15/77yQGtV0/XYc4OSMYH9/dWyqxfsaly9fxvbt23H69OlyBNuitLQU+/fvR506dcqEYQs5ArBr1y4UFhbi+++/11OB7PjTWPj3d7En8rg6lrptJxWxokZExyZlAI4C7p7aTX0THNV3daiKkLy8PMTFxeHs2bPYuXMnrl27hqKiIqSmpuq57sSpU6fg5OSkrh84cKCeqiExMRFHjhxRwigpKUFYWJh+RsP5SwcxbOCTEBazFKT9NbtGS/DJ//02/rXxAHqEtUHUhk/01J+Gmzdvqg+JJKFubm4ICAhAeno6nJ2dUVBQgK5du6JZs2b6FVbk5OTg6NGjikTmHTZsmEpnV6j9t2/fRlRUFNzd3ZU2Dxo0CL6+mnkjjHyDxkyFf4Ab1i+ar5+xH2pE9KTpb2HZlkh07dgKpzcv/kkLwitXruDChQswm83w8PBA9+7d4erqCh8fH3X+8OHDSEtLw0MPPaSIqghqL80FCaSAhg8frsoxwFFx/PhxeHl5qTxDhw6ttBwiIycXft6hSEw9jQa6+bMXajRmHB3l5XLYFReXqOO7SezixYs4f/68sqldunRBREQE6tevX0YycevWLQwYMKBKctavXw8/Pz84ODggODi4HMnR0dE4efIk/CVpHBEdOnSoshzC18sTXQf1xfyP1+op9kONiPbx8ZL/TUhKSVHHprtQTaJJzIMPPohWrVrpqVbQHHh7e6NuXeueD9sBx4mtbdu2aNGiBXJzcxWRBjhKaLMpJNp7kv3AA3dfgk+fMhaLP7Mh+j8e39WjRkQHB8rhZrYgOSVLHVdlNgyyqMnUNNrjykAXzJYcw5ZmZGRg3759SoM7deqEb775Rv329PRU+eih0CSNHDlSfefn56Nv377q3N0waGAf5MYnIjlD64O9mK4R0S2aNwVKCoD8UuTJzlVFteGe0e42bdq0UqJJJm1uSEiInqJdR9I2btyIli1bKg0uLi5W2tyxY0eVh6bozJkzimReT/PRo0cPde6noFE9Pzk0fWU5l7UEk2YGaxs1IrpjuxbyvyzC0YKT3+kNrQa0l926dVP2tSJOnDhRRp4BehUk+dFHHy3zPkhqkyZN1GTHiZPaPG7cOLi4uCj73KBBg0o9leoQFOSLK7eT9aO7u6j/CWpEdLPGjeDtJycyDy9s2altLfgpoEkgidRWumo//PCDsqt0wa5fv64+x44dU6s9aie1nX41zQInS9p3Coa+8pAhQ5TgWE5mZubP0mbDTHi4OSM5LV39thdqQLTWyJ4h0qa6umHz7mPquCoYi4/Y2Ngy0khoQkKC8qfpQ9N+Ozo6KgJpr9u1a6eupZtGMpOTk9GwYUPlwsXExCgzkyInYpbDlR+9EQqMafymK1g9NJNWVGKBi6u2EVPOCuq7tlEjjSYmjB4CaaBx4+oNxKek6akaqLFcPNDb4BAnqbSvXM3RuyBpJJiTWu/evdG6dWt1DUkMDw9XmkvbTMKDgoLUYoZ2m4Lh+ebNm6NRo0bKD2c9tP/UbAqEwiT5P/74ozqmoKtCXHI6mgQYC5oaU1IparRgITgJeraUKzNPJ8yZ/AT+NnOyJOKWJDRHaSF9ZBLJCZCrOGNiNMAVHDXRMAc0DaNHj1Y21xaHDh1SJPIzYsQIJSgD9EKowZwQDXDZzZFAwVG4/GbdtO10/QyPJTs3T86FXXH2/FZ0bk+X0yw/tW+na0S0tgvJhIETpiPyQgz8TcW4cXINnGWRru7edwxCVmVLNIf2unXr8Pvf/16t9kgOf1ecLDkaqJkEJ0z60Qa4iqTr16dPH+XyGd2pKFCC9VFQ2dnZykzVr++PI+euYuSIKbDkfSuvsY82EzUrWfbJYi7F/NeeB2JvIy0rE98cOo06lZBMVOw8TQk9Ed6lo4ZSkyvzSEg002labEkmKACaDpJMsI7KSCbo+nGkNG7cWJVVt64/lizbhD5DOiuSS0qK9Zy1jxqL0MHRCd1CQ9C0ZSPpj/phxtx/qvTqhomhdVzNUcvoH/fr10+lVQRNCYmjQNq3b6+nWkGzUJlwKoIeDFecNGEWm0G89cttmPPnP6rfRUXFysTcfRL9+agR0baas3bJO8DNG4i+kYo1G/ZUO3fzOpJMu8olOZfVlYGmhH4zBdOrVy89tTx4jhNmddi6dauaTDnxEkazZ8/7FIHtmmBIb80lNOYSe6DGGk1Q/g+GheChEVIrPTzxxIsztRNVgHaS5oAd5921qkC7TQ+FE6XtzSMDhi9Ora4MTF+5cqVaxHTurD2lpy5zXjHLVr/76l+xb5M2AqXElNCoBD9lhPxc1EqJRiFrFv8vUJwJeDXC8HHacKy4AZ2doXdB0KWrClevXi3zhdu0aaOnlgeFwPsn9JsrgpMkNZmuobaI4YYfS9lIa9I+Aq+8/jpCWjWXRxbkFxSVG6G1jVoVnV9dbyx7V2pzXiZ2nbiAFas2ygrKV7F37161gqP3QKKqAt0w2mV6ExVdPVvQbFAgNEWG7af/vHv3bmX7+fCA0BYiWluefOHPCPT3xfy5L6vj5avXy/N06+yHGvvRlWHMk9Ox4chlSXguLh5egZC2mg09duy41L5k5SVwwVFTGEP9wIEDSqsNP52miQ8UaPs5nhxsfOPnps2Rdv8czh7dpI7nL/wMzRoHYeyo4erYXqhVjdYkJvD1yo/wQKDUVv966ND/KSQlaEM7JjZGEfHz7kdUDWOo83khb1bRFtN1o5toTLBaBx0RfSsOzTsPg3sdlzKSv/xqB86cu6hIrn11K49a12hNgzQEhA5DapETXEQ+LmxfguhrP6CgyIJHRkZUazZqA7TtV65G40DUd9i285Cc4ASWfjQHgbwtKrFm4068PHMukq7xHo1Bgf1sNIef3VBcVCgC2w0VeGCEQFAPcfLUWZlq7DKqfUhyyzbZ5OUViMgjUeLkuYuipLR8nXM/WiaZbaAf3RvYjWizKFHfcuUo2v5ulEDTQQKBvcSGzbtUuv1R+T6mMc/PliQ3F4lpGXrKvYEdNdpcrqtjJs0SqNtDILifePy5mXqqgdremiXLkwJmqUbJ8fEJwvWBwQLeHURiUopKs9/YuhN2NR2E1lGtS0s/XyPQoLc0I/3kd3ex90CkSifuto+uJpj51odSi1uJ4NBwUVpSrKfeW9idaI1qq+1MSkkRA8ZMEajfU8C/q+ge8aw4ceqMOmeAOVVu44fN5kYrWK4eV86/SrIs/nKjcGwp5wi0FJNmvKWnShhqfg9xD4i2gQ0bu785LBo/+LCAawel4e37TxSLv/hKlBTfqXGlFmmGKjCpjmV6RdyOjRN/mfuBcGz1kHSy24jg7hHi+Gkj4P+Xw70lugxW0rbtiRTteo0WcA8R8O0iTUo/0SdioliwZJX4/kq0nqtqFBcVi6hvL4oZf/tINO0qy6nbTWpwB1EvdKhYt2mHnkuiEo2/l7DLyvCukDVapMtqu1pKSEzBR0tXY8nmA8i4nQpkp8t8Moe3J1zq+SDIzwd+fgGo4yRQWFKI5NQsxKbkAikyb24WUMcDaBKMcYPC8Nofx6NTB+u2BXbQjh7yT8IvQ/RdEJ+YjCOnLuDg0eO4cD0B12+mID23QD0VgUMJHFzc4OPqguAGvmjfIhC9uoUhfFAvtGneRC/h14dfDdFybPO/XFZTzyvXv+LSYljMFnWvpPKgN4ssR/t1L7bi/hz8ijTaaIa2g480yhlEp7O6ga/lNq6xLefXhFoh2trJ8qgq/dcH+7fUobCK/Q7qtT36//z8PD0VKJR2UrpW8hcbpw93nlAQyMnNQ2ZOLkpLzWXpzMknGtInk79LtUQFnrkTWkyuFRZx571iLYeRr/JyCgsLUFJaYS8d9cpSsTzrPliaMG5TsAjbdvJpTgmKiuQcURmUrpZvUUWYlq38t/hy7TZE55rg4iK9rFILonZ8DlcXV1k5STRh0Wcr8PWuI4hOzMCTI3pixktT4OPJR0uswIRvDkdh7qK1SL5xFf2HDIaLWx1cOB6FLLMTXp40Ck+OiVCV6dkxYNxUXLp6E73aN4dvXTdcupWGzOJimCUnIc384C09iPj4ZET9eAPvzHoe0yc9rl1fATv3H8aO3QewcN4bekp57DlwEPtk29bvPQUXT3egqBR9w9ooJYiNuY0kswcGd2yBZx8PR2iI9uCXTVy/YRP2HTmLvd/+CGd3N5Rm52D8Qz3Qr3cPhA8ZoPJVxJbdB3H08DG89/Zso5vlQdORkZWtLY3bhAu0jRCBHcJFYVEhT5W5n8fOXBKPTZqlH1md0qEjJwv4dBatejyqp1jx3Iy3peRCRXDYcJGeYb2J49VyiNgZeUI/EmLE+Kmy7hECnl3F0TPn9FQh63tVPDd7vn5kA33x0rr3GAGv7qK4oEAdW2wWMLZus0eIXLx0fFQujHrqKRrGT/mLCt2Dd5hYvnqTSjPu0GRmZkq/Pkwg9BHh0naoSiMqW4ESDTsPEwjsJ/IL8vWU8lBTM4f0I6P6o1PTBmDwT6LFGYEhQ1BYXFQmGRdH0x0vBew2eBz2fh8nDZATju36XE+1YumCv8CzeTBii9zg12qISispLsEbUx9DeP8H1TFhZiV865WTEwqLrEN9zWfvoolvxYeycnCaTPh8zS5cuR4jfed6eOwFvkuPsOqRrUa58lEY36wlR2mejRlcMu81WblMb9kMz8z+QKWZdNNWXFgMOMvrnBzhym8FekX6Txss/GwtEmKlT+/lickz5uqp5aGILjWbUVxUiHO7v0CIL4dYLjI96sOtaV9ky2FDSEHCbLzTRzZny479OHNBdrSuE3yaBqKer49shtX2GbZqQI92sn3Strn74pW5H8i2O+OV6S+o4aX9M2D0wJroJH//deaLFeyeg2rLgoWfokGLppI8N2zZvBcXf4iWJFTCgkRZPKG8ToWD6MjMzpZkymMp3AAf43VxWhk0mwrqsFxDdVjT5i/8HA3bN4Z07rFqxQ7ciJG8VEBZrWbpnxIXD6/HgDbS8c+Sq62gpvBpMRRFBXnw4H4Hm/q2Hz0NaWCVOjZrUF+lmcq1RyO9RUM5Sqilvp7YtzdKpZXvRNXgHKPNE+Xx6JP/g5WfvoOFMydxSQk0b4gxzxhvvpH1Kp/8TpicHJCYnIYScyluxyZg0MQZMrsLfApzcUyPKruztsoh9MlyUMQzOL57Jd59Qc4jt5Nkh1vgkbFT1Tm23aDEKl6b8g9s/wKj+8jJIS1DDs0g+LaNkP1JgJu79fET3ziqXWOzdLBto5qJ+bROr0Ke47Jb+1n5cqMiqKEV895OTEJSfDzCOoVg7KhwhLSRWi11/8qNOKz5aqv8zYewlRAtCxGudfDp8rXwCuqPpt0fx7Wr8Zj32tPIvPYNWjCko5yiVA+TyRnffvc96sjFU6MGAXjqiVFo2062xcEZF6OTsGPP4XItLyNazgHat17bhpX/wB+GdgYSU1HgXxdD/vBnePlYY/UGd+nAsSf7ZUZCeqZKEwaTEsZwjUmWUnaWnc/Kwe96lX/VRFm/7tZBSymKdcH16P04wscMx4fL1uHTlZvQu2eoVAjZjqBG+OObH6k8UnW1b1tI7k35+Xjv9Rk4tP1j9YQegfUwa/5SXLj4g8oi2PzKB4MOuoF0UTWMGDsN3X7XBf/44issXrUZnTu2kf1MBRo3wn/NfEflMRhRbCjmHbTH8dzDY/R85dIFmDy2rxyeUrNd6ujnNDzx2AgEcKIqcUPStWvyCjnobHb4GPYy8sx3aqM64hPxybuzVVp5D1VWrfKSCfmlelseQrbNReZZs2EXho/shwkjBqJnp3bo3L41Xp/2HPqEtaCjK/13C956f7F+lQ1YpLRrNAv5ch3Qo1sX7NsshfKdJLh+IDr2mYCEuDiVzUCZNioqbM5I/4FHn634N0ZLB+KJR4aie6f26Ni+Fd6dNRnd20lbLc1wak4J5v1zpXYNQdcjKydX9Bk9WbkhtjA8mZdn/13AJVTMnrdET9GQnpYm6rUZLFCvn4h48iU91Yr3PvpcvWICTfqJyz9e01P1hwA2ftKw8dOkWzlculndReQJ60MAlcWaTfi1trpZtigqLBAI6CPQWbpwAV1EUkqqfkaDd8cIgU6jBIL7CLPZ+gDrXyu/lm3vLtBFXle/p8jIyNTPSE4yswT85Dl5nUf7cD1VA++PB4Y+pB+VR2p6hlZmp7HSZe4hkuUx4di978A573y4RDr/J+HoYIanhxuC9MmNoPTCB/dFblGW2nH/SLgRcy3g5uaOWVOfknbKjFXbDuOrDdull1KIs+d/xOvvL8Hn63bhxWdHIXLdh2gYGKhfJ3WFhcp/3Lm/Ydd+fPn1HghH6WPIxUFs9GXU9/dWmxJdnKUJkHlPREVhhJy4bqWkw0POE54e7ggM0CJdU5KSsPPQcew/dVp6cM4Qsk1rV/8b3cJC1Qp18fI1OHD8IlzlIkiUlCAnOxVuLg5o2jgYYR3bwclkRuSRk3Dy88SiRavR+oFGMk82Fiz+EmdvxMPd1RmlBflywBWgnp8vrt24gfDHpyEuIxceziZpTj1ke7W23JKj9kjUSeyKPA2nOo6wODlj08bt6BLaEqb4xEThKicJLy93pKVmwNXVRToTlb9EOy09A/6yMg1yKEq7aXuXLE7O6DExCXKFa0ZgYABayIlUgxx/tLEaw1qSBH3azOxc1QFHablYVm5egdri1SCgnopnYX4K2FH6wu5qn10q3D08UNeb/rWDXBYXIlXOEfXq+UtXXKujqKQU+Xn5SlCF0h/295dtpiciTVt6uhZPWF/m14yYE4rlesFBun3F0qfOycqWCidtsWyLLwNW5XUWWWa6rMNNmkBurHR25Z4/V6Qmp0vX2U1yp0Uf0CxlZGaqzZvSCVVtKZAeVzH39VGtVa7fYFeYzp07J7h3jfuCf+O8dkGHgNuKGaJnSkhIEAwf445Ng2jDY6iKeJ6vTigVr79beZXhbnVUhf/0OnuAbeFmeoaD/GY67hEc50jovxUY9ivNiXrTAEMNGCq2bds2FVxDrFixQr3HiK/gYUzJqlWryoUW8zUPDLhk5Ct3dHIz4+bNm1XkFAPmjc2NvJ6eBcH4bo4qTjR8Aw3vB3OzOtvCjeTcjstIKu6B5jXMx/PcOUocPHhQhSUvWrRI7b3mO5S4s9R4PYVx3gAn18jISLWJnQFELJ9DnPuxCbaF1zJijMFIDC7l+0UI5uGzy2+//VZp65YtW1TkL9u1evVqFWXAb77xxnZft9VlkGBHSBBD0J599lkVMMlO8UKGCRMMcmdgD10zRriyEgMLFixQxDC4fuLEiXj11VeVwGj/p06dqnsRGt58801FMMFN49ygzroYmElBMSqWto3kEYwAYJgEQWEtXLhQlU0wPwcmCeQ1U6ZMUXUTjNRdsmSJ+m2A9TByl6Qw2JPXffjhh/pZrW0Mr2D8zJgxY1SZbCv7RPAcPSMKhMo2fvx4FStJzihgnq8YClKOaL7L6Omnn9aP5MpZSnrNmjV46623sGzZMpXGAtauXatiQgKlb2y7/ZYk2b5Dg4KiJpHIjz/+uCwvO/jaa6/hvffeU8d8YkNNoU2jMLjbn1FU1Lpp06apPIyEDQ0NVYQTbNOmTZtUVK4R+sb2Gnj44YeVNlIYkyZNUn0zQCL4gJf5+U3NZJ0cNXx7zeDBg1U4NWPT+Y4QKhcFSQ4mTJig+sFwPX44Mm3fUMZ+GkK2RTmiSQobbsCINuUwYkwgwYYxjIwxIgTPG+BQMbSUIPEMQ37//feVpDmkCWoQtZxpy5cvVyOAQ96YLqhxFBhNGInnNzvdv39/lZ/gcJ4/fz6++OILJQS2w7YtJIfHFCBNG4e4AdZjfFg+Q6KnT5+ON954Q5kIBicxjX1hZAJNHttMU/PJJ5/gmWeeUUKiCenZs6eK5CXYzwqWuAxlRLNSvo6BGkAp0YRQgnwdDx1wviCKWkGp8zdtG4cOhUGJMtR45syZqiHsOM0NTdClS5eU5jHcwQhhM4Y6hxq1maTT7vPDzlHjSewLL7ygzBGHMIXBeBXWxzAKI0CI52kGCJJDRaEwGK5Me8+oL7afsSw8JkguyyFRnA9oAkkizRXzcW6iIKnpNGmcDxhLzvxs16xZs1T/WQ7fREYFMsKr2V/Wb5haA2VeB78MjaB2UWIV7YxtHoKax2FEYlip4Yvzev5m4wkec2gyz88FNazidRROZSFqTKftZN1sV8X2GmXR9PENNcZkXB1s+1YZKCiWa5ynMNgOCstaN/D/5RfTA3xuECoAAAAASUVORK5CYII='
                                        } );
                                        doc.content.splice( 0, 0, {
                                            margin: [ 0, 0 ],
                                            alignment: 'right',
                                            text: 'Reporte generado el $fecha a las $hora'
                                        } );
                                    }
                                    if ($certifica) {
                                        doc.content.splice( 4, 0, {
                                            margin: [ 0, 10 ],
                                            alignment: 'center',
                                            text: [
                                                "$certificoQue",
                                                { text: "$cantidadDatos", bold: true},
                                                "$certificoQue2",
                                                { text: "$fechaCertifico", bold: true}
                                            ]
                                        });
                                    }
                                },
                            },
                            {
                                extend: 'excelHtml5',
                                //extend: 'csv',
                                text: 'Excel',
                                footer: true,
                                enabled: $verExcel,
                                action: function(e, dt, button, config) {
                                    var before = $('#tabla thead').html();
                                    $('#tabla thead th input').each( function () {
                                        var title = $(this).attr('placeholder');
                                        $(this).parent().html( title );
                                    } );
                                    $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, button, config);
                                    $('#tabla thead').html(before);
                                    location.reload();
                                },
                                exportOptions: {
                                    columns: $exportableColumns,
                                    format: {
                                        body: function(data, row, column, node) {
                                            data = $('<p>' + data + '</p>').text();
                                            //return data;
                                            return $.isNumeric(data.replace('.', '')) ? data.replace('.', '') : data;
                                            //return $.isNumeric(data) ? parseInt(data.replace('.', '')) : data;
                                        }
                                    },
                                    extend: 'csv',
                                    footer: true,
                                    text: 'Excel',
                                }
                            }
                        ],
                        initComplete: function(settings, json) {
                            $('#tabla').show();
                            $('#loading').hide();
                            $('#tabla_filter').hide();
                            if ($verPDF) {
                                $('.buttons-pdf').html('<img src="../images/pdf.png"/>');
                            }
                            if ($verExcel) {
                                $('.buttons-excel').html('<img src="../images/xls.png"/>');
                            }
                            $('.dt-button').css('background','transparent');
                            $('.dt-button').css('border','none');
                        },
                        "columnDefs": [
                            {
                                "targets": $invisibleColumns,
                                "visible": false,
                                "searchable": false
                            },
                        ]
                    });

                    table.columns().every( function () {
                        var that = this;
                        $( 'input', this.header() ).on( 'change keyup', function () {
                            if ( that.search() !== this.value ) {
                                that
                                    .search( this.value )
                                    .draw();
                            }
                        } );
                    } );

                    if(clickUrl != ''){
                        $('#tabla tbody tr').addClass('clickeable');
                        $('#tabla tbody').on( 'click', 'tr', function () {
                            var id = table.row( this ).data()[0];
                            if (render != '') {
                                $("#myModal").find("#modalContent").html("");
                                $("#myModal").find(".modal-title").text("");
                                $("#myModal").modal("show");
                                //$("#myModal").find("#modalContent").html($loading);
                                $.ajax({
                                        url: clickUrl,
                                        data: {id: id},
                                        //type: post,
                                        success: function(respuesta) {
                                                $("#myModal").find("#modalContent").html(respuesta);
                                                $("#myModal").find(".modal-title").text($("#titulo").val());
                                        },
                                        error: function() {
                                                $("#myModal").find("#modalContent").html("ERROR");
                                        }
                                });
                            } else {
                                if (openNewWindow != '') {
                                    window.open('$location?id='+id);
                                } else {
                                    window.location = '$location?id='+id;
                                }
                            }
                        } );
                    }
                        
                    $('input').on('click',function(e){
                        return false;
                    });
                });
JS;

        $view->registerJs($script);

        $view->registerCssFile("@web/css/tablas.css", [
            'depends' => [\yii\bootstrap\BootstrapAsset::className()],
                ], 'css-tablas');
    }

}
