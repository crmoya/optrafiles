<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "representante_legal".
 *
 * @property int $id
 * @property string $rut
 * @property string $nombre
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property resource $huella
 * @property resource $template
 * @property int $template_size
 *
 * @property Contrato[] $contratos
 */
class RepresentanteLegal extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'representante_legal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['rut', 'nombre', 'apellido_paterno', 'apellido_materno'], 'required'],
            [['huella', 'template'], 'string'],
            [['template_size'], 'integer'],
            [['rut'], 'string', 'max' => 12],
            [['nombre', 'apellido_paterno', 'apellido_materno'], 'string', 'max' => 100],
            [['rut'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'rut' => 'Rut',
            'nombre' => 'Nombre',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'huella' => 'Huella',
            'template' => 'Template',
            'template_size' => 'Template Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContratos() {
        return $this->hasMany(Contrato::className(), ['representante_legal_id' => 'id']);
    }

}
