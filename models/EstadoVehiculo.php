<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_vehiculo".
 *
 * @property int $vehiculo_id
 * @property int $usuario_id
 * @property string $estado
 * @property string $desde
 * @property int $suscriptor_id
 *
 * @property Usuario $usuario
 * @property Vehiculo $vehiculo
 * @property Suscriptor $suscriptor
 */
class EstadoVehiculo extends \yii\db\ActiveRecord {

    const ESTADOS = [
        1 => "RECEPCIONADO",
        2 => "VENDIDO",
        3 => "EN VENTA",
        4 => "NO VENDIDO"
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'estado_vehiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['vehiculo_id', 'usuario_id', 'estado', 'desde'], 'required'],
            [['vehiculo_id', 'usuario_id', 'estado', 'suscriptor_id'], 'integer'],
            [['desde'], 'safe'],
            [['vehiculo_id', 'usuario_id'], 'unique', 'targetAttribute' => ['vehiculo_id', 'usuario_id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
            [['vehiculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['vehiculo_id' => 'id']],
            [['suscriptor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Suscriptor::className(), 'targetAttribute' => ['suscriptor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'vehiculo_id' => 'Vehiculo ID',
            'usuario_id' => 'Usuario ID',
            'estado' => 'Estado',
            'desde' => 'Desde',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario() {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo() {
        return $this->hasOne(Vehiculo::className(), ['id' => 'vehiculo_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuscriptor() {
        return $this->hasOne(Suscriptor::className(), ['id' => 'suscriptor_id']);
    }

}
