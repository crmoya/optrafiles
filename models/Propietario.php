<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propietario".
 *
 * @property int $id
 * @property string $rut
 * @property string $nombres
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property int $persona_natural
 * @property string $direccion
 * @property string $fecha_huella
 * @property string $fecha_fotos
 *
 * @property CompraPropietario[] $compraPropietarios
 * @property Operacion[] $operacions
 * @property Operacion[] $operacions0
 * @property Operacion[] $operacions1
 * @property Representante[] $representantes
 * @property Vehiculo[] $vehiculos
 * @property VentaPropietario[] $ventaPropietarios
 */
class Propietario extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'propietario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['rut', 'nombres'], 'required'],
            [['persona_natural'], 'integer'],
            [['direccion'], 'string'],
            [['fecha_huella', 'fecha_fotos'], 'safe'],
            [['rut'], 'string', 'max' => 12],
            [['nombres', 'apellido_paterno', 'apellido_materno'], 'string', 'max' => 100],
            [['rut'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'rut' => 'Rut',
            'nombres' => 'Nombres',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'persona_natural' => 'Persona Natural',
            'direccion' => 'Direccion',
            'fecha_huella' => 'Fecha Huella',
            'fecha_fotos' => 'Fecha Fotos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraPropietarios() {
        return $this->hasMany(CompraPropietario::className(), ['propietario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions() {
        return $this->hasMany(Operacion::className(), ['venta_propietario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions0() {
        return $this->hasMany(Operacion::className(), ['compra_propietario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions1() {
        return $this->hasMany(Operacion::className(), ['compra_para_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepresentantes() {
        return $this->hasMany(Representante::className(), ['propietario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos() {
        return $this->hasMany(Vehiculo::className(), ['propietario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaPropietarios() {
        return $this->hasMany(VentaPropietario::className(), ['propietario_id' => 'id']);
    }

    public function getCompleteName() {
        return $this->nombres . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno;
    }
    
    public function getCompleteNameWithRut() {
        return $this->nombres . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno . ' (' . $this->rut . ')';
    }
    
}
