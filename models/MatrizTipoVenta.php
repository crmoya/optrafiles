<?php

namespace app\models;

/**
 * Description of MatrizTipoVenta
 *
 * @author LeoGuitar
 */
class MatrizTipoVenta extends \yii\base\Model {

    public $file;
    public $documento_subido;

    const PATH = 'uploads/materia_privada/';
    const GENERATED_PATH = 'uploads/materia_privada/generated/';
    const FILE_EXTENSION = 'docx';

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'file' => 'Documento Matriz',
        ];
    }

    public function saveMateria() {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array
        try {
            $file = \yii\web\UploadedFile::getInstance($this, 'file');

            if (!is_null($file)) {
                $filename_array = (explode(".", $file->name));
                // store the source file name
                $ext = end($filename_array);

                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                $path = self::PATH . $this->nombre . '.' . $ext;

                if (!$this->save()) {
                    throw new \Exception('Imposible crear materia privada. Error: ' . join(', ', $this->getFirstErrors()));
                }
                if (!file_exists(self::PATH)) {
                    mkdir(self::PATH, 0777, true);
                }
                if (!$file->saveAs($path)) {
                    throw new \Exception('Imposible crear materia privada. Error: ' . $file->error);
                }
            } else {
                if (!$this->save()) {
                    throw new \Exception('Imposible crear materia privada. Error: ' . join(', ', $this->getFirstErrors()));
                }
            }
            $transaction->commit();
            return ['status' => 'success', 'message' => 'La materia ha sido creada exitosamente'];
        } catch (\Exception $ex) {
            $transaction->rollBack();
            return ['status' => 'error', 'message' => $ex->getMessage()];
        }
    }

    public function getDocumentoSubido() {
        /*
          $path = MateriaPrivada::PATH.DIRECTORY_SEPARATOR.$this->nombre.".".MateriaPrivada::FILE_EXTENSION;
          return file_exists($path)?1:0;
         */
    }

}
