<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Operacion;

/**
 * OperacionSearch represents the model behind the search form of `app\models\Operacion`.
 */
class OperacionSearch extends Operacion {

    public $fecha_desde;
    public $fecha_hasta;
    
    public $patente;
    public $repertorio;
    public $compareciente;
    public $estado;
    public $direccion;
    
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'venta_propietario_id', 'venta_precio', 'venta_tipo', 'venta_user_id', 'venta_mandatario_id'], 'integer'],
            [['venta_fecha', 'fecha_desde', 'fecha_hasta', 'direccion', 'patente', 'repertorio', 'compareciente', 'estado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Operacion::find()->joinWith(["ventaVehiculo"]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'venta_propietario_id' => $this->venta_propietario_id,
            'venta_precio' => $this->venta_precio,
            'venta_tipo' => $this->venta_tipo,
            'venta_user_id' => $this->venta_user_id,
            'venta_mandatario_id' => $this->venta_mandatario_id,
        ]);
        
        $query->andFilterWhere(['>=', 'venta_fecha', \app\components\Helper::fixDateFormat($this->fecha_desde)])
                ->andFilterWhere(['<=', 'venta_fecha', \app\components\Helper::fixDateFormat($this->fecha_hasta)]);

        //$query->andFilterWhere(['like', 'ventaPropietario.direccion', $this->direccion]);

        return $dataProvider;
    }

}
