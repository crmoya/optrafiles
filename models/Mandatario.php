<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mandatario".
 *
 * @property int $id
 * @property string $rut
 * @property string $nombres
 * @property string $apellido_paterno
 * @property string $apellido_materno
 *
 * @property Operacion[] $operacions
 */
class Mandatario extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'mandatario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['rut'], 'required'],
            [['rut'], 'string', 'max' => 12],
            [['nombres', 'apellido_paterno', 'apellido_materno'], 'string', 'max' => 100],
            [['rut'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'rut' => 'Rut',
            'nombres' => 'Nombres',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions() {
        return $this->hasMany(Operacion::className(), ['venta_mandatario_id' => 'id']);
    }

    public function getCompleteName() {
        return $this->nombres . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno;
    }
    
}
