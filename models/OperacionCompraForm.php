<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * OperacionCompraForm is the model behind the contact form.
 */
class OperacionCompraForm extends Model {

    public $operacion_id;
    public $vehiculo_id;
    public $patente;
    public $tipoCompra;
    // DATOS COMPRA - ETAPA 2
    // ETAPA COMPRA
    public $rut;
    public $nombre;
    public $persona_natural;
    public $razon_social;
    public $apellido_paterno;
    public $apellido_materno;
    public $direccion;
    public $funcionario;
    public $tipo_vehiculo;
    public $marca_nombre;
    public $marca;
    public $modelo;
    public $modelo_nombre;
    public $agno;
    public $motor;
    public $chasis;
    public $tipo_vehiculo_nombre;
    public $precio_etapa_1;
    public $precio_etapa_2;
    public $tasacion_comercial;
    public $personeria_mandato;
    public $tiene_prenda;
    public $tiene_multas;
    
    public $comprador_rut;
    public $comprador_nombre;
    public $comprador_razon_social;
    public $comprador_persona_natural;
    public $comprador_apellido_paterno;
    public $comprador_apellido_materno;
    public $comprador_direccion;
    
    // PARA COMPRA PARA
    public $rut_para;
    public $nombre_para;
    public $apellido_paterno_para;
    public $apellido_materno_para;
    public $direccion_para;
    // PARA VENTA COMUNIDAD
    public $cantidad_sucesion;
    // DOCUMENTOS
    public $cav;
    public $certificado_multas;
    public $cedula_fotocopia;
    public $permiso_circulacion;
    public $personeria;
    public $mandato;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['patente', 'compraId', 'tipoCompra', 'operacion_id',
            'rut', 'nombre', 'apellido_paterno', 'apellido_materno', 'direccion', 'funcionario', 'tipo_vehiculo', 'marca', 'modelo', 'agno', 'motor', 'chasis',
            'tiene_prenda', 'tiene_multas', 'vehiculo_id', 'tasacion_comercial', 'tipo_vehiculo_nombre', 'marca_nombre', 'modelo_nombre', 'persona_natural',
            'comprador_rut', 'comprador_nombre', 'comprador_apellido_paterno', 'comprador_apellido_materno', 'comprador_direccion',
            'comprador_razon_social', 'comprador_persona_natural', 'cantidad_sucesion', 'apellido_materno_para', 'direccion_para',
            'cav', 'certificado_multas', 'cedula_fotocopia', 'permiso_circulacion', 'personeria', 'mandato', 'personeria_mandato'], 'safe'],
            [["precio_etapa_1", "precio_etapa_2"], "number"],
            [['rut', 'comprador_rut', 'rut_para'], \app\components\RutValidator::className()],
            [['rut', 'comprador_rut', 'rut_para'], 'string', 'max' => 13],
            [["patente", 'funcionario', 'tipo_vehiculo', 'marca', 'modelo', 'agno', 'motor', 'chasis', 'comprador_rut'], "required"],
            [["nombre", "apellido_paterno", 'comprador_nombre', 'comprador_apellido_paterno'], "required", "when" => function($model) {
                return $model->tipoCompra === 1;
            }],
            [["razon_social", "comprador_razon_social"], "required", "when" => function($model) {
                return $model->tipoCompra === 2;
            }],
            [['rut_para', 'nombre_para', 'apellido_paterno_para'], "required", "when" => function($model) {
                return $model->tipoCompra === 3;
            }],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        $cedula = "";
        $cedula_comprador = "";
        if (isset($this->tipoCompra)) {
            if (Operacion::TIPO_COMPRA[$this->tipoCompra] === Operacion::TIPO_COMPRA[2]) {
                $cedula = "RUT";
                $cedula_comprador = "RUT COMPRADOR";
            } else {
                $cedula = "CÉDULA IDENTIDAD";
                $cedula_comprador = "CÉDULA IDENTIDAD COMPRADOR";
            }
        }
        
        return [
            'tipoCompra' => "TIPO COMPRA",
            'patente' => "PATENTE",
            'rut' => $cedula,
            'nombre' => 'NOMBRES',
            'apellido_paterno' => 'APELLIDO PATERNO',
            'apellido_materno' => 'APELLIDO MATERNO',
            'razon_social' => 'RAZÓN SOCIAL',
            'direccion' => 'DIRECCIÓN',
            'funcionario' => 'FUNCIONARIO COMPRA',
            'tipo_vehiculo' => 'TIPO VEHÍCULO',
            'marca' => 'MARCA',
            'modelo' => 'MODELO',
            'agno' => 'AÑO',
            'motor' => 'N° MOTOR',
            'chasis' => 'N° CHASIS',
            'precio_etapa_1' => 'PRECIO DE COMPRA',
            'precio_etapa_2' => 'PRECIO DE VENTA',
            'tasacion_comercial' => "TASACIÓN COMERCIAL",
            'comprador_rut' => $cedula_comprador,
            'comprador_nombre' => "NOMBRES COMPRADOR",
            'comprador_razon_social' => 'RAZÓN SOCIAL COMPRADOR',
            'comprador_apellido_paterno' => "APELLIDO PATERNO COMPRADOR",
            'comprador_apellido_materno' => "APELLIDO MATERNO COMPRADOR",
            'comprador_direccion' => "DIRECCIÓN COMPRADOR",
            'personeria_mandato' => "CLÁUSULA PERSONERÍA",
            'cav' => "CAV",
            'certificado_multas' => "CERTIFICADO MULTAS",
            'cedula_fotocopia' => "CEDULA IDENTIDAD (AMBOS LADOS)",
            'permiso_circulacion' => "PERMISO CIRCULACIÓN",
            'personeria' => "PERSONERÍA",
            'mandato' => "MANDATO",
            'cantidad_sucesion' => "CANTIDAD ",
            'rut_para' => "CEDULA IDENTIDAD PARA",
            'nombre_para' => "NOMBRES PARA",
            'apellido_paterno_para' => "APELLIDO PATERNO PARA",
            'apellido_materno_para' => "APELLIDO MATERNO PARA",
            'direccion_para' => "DIRECCIÓN PARA",
        ];
    }

}
