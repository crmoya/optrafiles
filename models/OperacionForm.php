<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * OperacionForm is the model behind the contact form.
 */
class OperacionForm extends Model {

    public $operacion_id;
    public $vehiculo_id;
    public $patente;
    public $tipoVenta;
    // DATOS VENTA - ETAPA 1
    public $ventaId;
    public $ventaFecha;
    public $ventaPropietarioId;
    public $ventaDireccion;
    public $ventaPrecio;
    public $ventaTipo;
    public $ventaUserId;
    public $ventaMandatarioId;
    // ETAPA VENTA
    public $rut;
    public $nombre;
    public $persona_natural;
    public $razon_social;
    public $apellido_paterno;
    public $apellido_materno;
    public $direccion;
    public $funcionario;
    public $tipo_vehiculo;
    public $tipo_vehiculo_nombre;
    public $marca_nombre;
    public $marca;
    public $modelo;
    public $modelo_nombre;
    public $agno;
    public $motor;
    public $chasis;
    public $precio;
    public $tasacion_comercial;
    public $personeria_mandato;
    public $tiene_prenda;
    public $tiene_multas;
    // PARA VENTA EMPRESA
    public $representante_rut;
    public $representante_nombre;
    public $representante_apellido_paterno;
    public $representante_apellido_materno;
    public $representante_direccion;
    // PARA VENTA MANDATARIO
    public $mandante_rut;
    public $mandante_nombre;
    public $mandante_persona_natural;
    public $mandante_apellido_paterno;
    public $mandante_apellido_materno;
    public $mandante_direccion;
    public $mandatario_rut;
    public $mandatario_nombre;
    public $mandatario_apellido_paterno;
    public $mandatario_apellido_materno;
    // PARA VENTA COMUNIDAD
    public $cantidad_sucesion;
    // DOCUMENTOS
    public $cav;
    public $certificado_multas;
    public $cedula_fotocopia;
    public $permiso_circulacion;
    public $personeria;
    public $mandato;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['patente', 'ventaId', 'tipoVenta', 'ventaFecha', 'ventaPropietarioId', 'ventaDireccion', 'ventaPrecio', 'ventaTipo', 'ventaUserId', 'ventaMandatarioId',
            'rut', 'nombre', 'apellido_paterno', 'apellido_materno', 'direccion', 'funcionario', 'tipo_vehiculo', 'marca', 'modelo', 'agno', 'motor', 'chasis', 'precio',
            'tiene_prenda', 'tiene_multas', 'vehiculo_id', 'tasacion_comercial', 'tipo_vehiculo_nombre', 'marca_nombre', 'modelo_nombre', 'persona_natural',
            'representante_rut', 'representante_nombre', 'representante_apellido_paterno', 'representante_apellido_materno', 'representante_direccion',
            'mandatario_rut', 'mandatario_nombre', 'mandatario_apellido_paterno', 'mandatario_apellido_materno', 'mandante_persona_natural',
            'mandante_rut', 'mandante_nombre', 'mandante_apellido_paterno', 'mandante_apellido_materno', 'mandante_direccion', 'cantidad_sucesion',
            'cav', 'certificado_multas', 'cedula_fotocopia', 'permiso_circulacion', 'personeria', 'mandato', 'personeria_mandato'], 'safe'],
            [['rut', 'mandatario_rut', 'mandante_rut', 'representante_rut'], \app\components\RutValidator::className()],
            [['rut', 'mandatario_rut', 'mandante_rut', 'representante_rut'], 'string', 'max' => 13],
            [["patente", 'funcionario', 'tipo_vehiculo', 'marca', 'modelo', 'agno', 'motor', 'chasis'], "required"],
            [["nombre", "apellido_paterno"], "required", "when" => function($model) {
                return $model->tipoVenta === 1;
            }],
            [["razon_social", 'representante_rut', 'representante_nombre', 'representante_apellido_paterno'], "required", "when" => function($model) {
                return $model->tipoVenta === 2;
            }],
            [["mandante_rut", "mandante_nombre", "mandante_apellido_paterno", 'mandatario_rut', 'mandatario_nombre', 'mandatario_apellido_paterno'],
                'required', 'when' => function($model) {
                    return $model->tipoVenta === 3;
            }],
        ];
    }
    
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        $cedula = "";
        $per_man = "";
        if (isset($this->tipoVenta)) {
            if (Operacion::TIPO_VENTA[$this->tipoVenta] === Operacion::TIPO_VENTA[2]) {
                $per_man = "PERSONERÍA";
                $cedula = "RUT";
            } else if (Operacion::TIPO_VENTA[$this->tipoVenta] === Operacion::TIPO_VENTA[3]) {
                $per_man = "MANDATO";
            } else {
                $cedula = "CÉDULA IDENTIDAD";
            }
        }
        return [
            'tipoVenta' => "TIPO VENTA",
            'patente' => "PATENTE",
            'rut' => $cedula,
            'nombre' => 'NOMBRES',
            'apellido_paterno' => 'APELLIDO PATERNO',
            'apellido_materno' => 'APELLIDO MATERNO',
            'razon_social' => 'RAZÓN SOCIAL',
            'direccion' => 'DIRECCIÓN',
            'funcionario' => 'FUNCIONARIO VENTA',
            'tipo_vehiculo' => 'TIPO VEHÍCULO',
            'marca' => 'MARCA',
            'modelo' => 'MODELO',
            'agno' => 'AÑO',
            'motor' => 'N° MOTOR',
            'chasis' => 'N° CHASIS',
            'precio' => 'PRECIO DE COMPRA',
            'tasacion_comercial' => "TASACIÓN COMERCIAL",
            'personeria_mandato' => $per_man,
            'cav' => "CAV",
            'certificado_multas' => "CERTIFICADO MULTAS",
            'cedula_fotocopia' => "CEDULA IDENTIDAD (AMBOS LADOS)",
            'permiso_circulacion' => "PERMISO CIRCULACIÓN",
            'personeria' => "PERSONERÍA",
            'mandato' => "MANDATO",
            'cantidad_sucesion' => "CANTIDAD ",
            'mandante_rut' => "CEDULA IDENTIDAD MANDANTE",
            'mandante_nombre' => "NOMBRES MANDANTE",
            'mandante_apellido_paterno' => "APELLIDO PATERNO MANDANTE",
            'mandante_apellido_materno' => "APELLIDO MATERNO MANDANTE",
            'mandante_direccion' => "DIRECCIÓN MANDANTE",
            'mandatario_rut' => "CEDULA IDENTIDAD MANDATARIO",
            'mandatario_nombre' => "NOMBRES MANDATARIO",
            'mandatario_apellido_paterno' => "APELLIDO PATERNO MANDATARIO",
            'mandatario_apellido_materno' => "APELLIDO MATERNO MANDATARIO",
        ];
    }

}
