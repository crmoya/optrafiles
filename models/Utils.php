<?php
namespace app\models{
    class Utils{
        
        public static function replaceVowels($text){
            $text = str_replace('\$a', 'á', $text);
            $text = str_replace('\$e', 'é', $text);
            $text = str_replace('\$i', 'í', $text);
            $text = str_replace('\$o', 'ó', $text);
            $text = str_replace('\$u', 'ú', $text);
            $text = str_replace('\$A', 'Á', $text);
            $text = str_replace('\$E', 'É', $text);
            $text = str_replace('\$I', 'Í', $text);
            $text = str_replace('\$O', 'Ó', $text);
            $text = str_replace('\$U', 'Ú', $text);
            return $text;
        }
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

