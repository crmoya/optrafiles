<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_vehiculo".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property Vehiculo[] $vehiculos
 */
class TipoVehiculo extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'tipo_vehiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos() {
        return $this->hasMany(Vehiculo::className(), ['tipo_vehiculo_id' => 'id']);
    }

}
