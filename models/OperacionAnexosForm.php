<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * OperacionForm is the model behind the contact form.
 */
class OperacionAnexosForm extends Model {
    
    public $file;
    public $description;
    public $user;
    public $documento_subido;

    const PATH = 'uploads' . DIRECTORY_SEPARATOR . 'operation' . DIRECTORY_SEPARATOR;
    const GENERATED_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'operation' . DIRECTORY_SEPARATOR . 'generated' . DIRECTORY_SEPARATOR;
    const FILE_EXTENSION = 'pdf';
    
    public $cav;
    public $certificado_multas;
    public $cedula_fotocopia;
    public $permiso_circulacion;
    public $personeria;
    public $mandato;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['file'], 'file', 'skipOnEmpty' => true,],
            [['file', 'description'], 'required'],
            [['cav', 'certificado_multas', 'cedula_fotocopia', 'permiso_circulacion', 'personeria', 'mandato'], 'safe']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'file' => 'Documento',
            'description' => 'Descripción',
        ];
    }
    
    public function saveDocument($operation_id) {
        if ($this->validate()) {
            $filename_array = (explode(".", $this->file->name));
            // store the source file name
            $ext = end($filename_array);
            try {
                $folderPath = static::PATH . $operation_id . DIRECTORY_SEPARATOR;
                //$path = $folderPath . $this->description . '.' . $ext;
                $path = $folderPath . Yii::$app->user->id . '_' . $this->description . '.' . $ext;

                if (!is_dir($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
                if ($this->file->saveAs($path)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception $ex) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public function search($operation) {
        $files = (file_exists(self::PATH . DIRECTORY_SEPARATOR . $operation . DIRECTORY_SEPARATOR)) ? \yii\helpers\FileHelper::findFiles(self::PATH . $operation . DIRECTORY_SEPARATOR) : null;
        $files_array = [];
        if (!empty($files)) {
            foreach ($files as $file) {
                $desc_begin = strrpos($file, DIRECTORY_SEPARATOR) + 1;
                $desc_end = strrpos($file, '.');
                //$file_array['descripcion'] = substr($file, $desc_begin, $desc_end - $desc_begin);
                $fileName = substr($file, $desc_begin, $desc_end - $desc_begin);
                $userId = substr($fileName, 0, strpos($fileName, '_'));
                $description = substr($fileName, strpos($fileName, '_') + 1);
                $this->user = User::find()->where('id = :id', [':id' => $userId])->one();

                $file_array['description'] = $description;
                $file_array['url'] = $file;
                //$file_array['url'] = \yii\helpers\Url::base() . $file;
                $file_array['user'] = $this->user;
                $files_array[] = $file_array;
            }
        }

        $dataProvider = new \yii\data\ArrayDataProvider([
            'key' => 'url',
            'allModels' => $files_array,
            'sort' => [
                'attributes' => ['descripcion'],
            ],
        ]);
        return $dataProvider;
    }
}
