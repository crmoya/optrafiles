<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Modelo;

/**
 * ModeloSearch represents the model behind the search form of `app\models\Modelo`.
 */
class ModeloSearch extends Modelo {

    public $nombreMarca;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'marca_id'], 'integer'],
            [['nombre', 'nombreMarca'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Modelo::find()->joinWith("marca");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'marca_id' => $this->marca_id,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);
        $query->andFilterWhere(['like', 'marca.nombre', $this->nombreMarca]);

        return $dataProvider;
    }

}
