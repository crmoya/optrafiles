<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class ChangePasswordForm extends Model {

    public $old_password;
    public $new_password;
    public $repeat_new_password;
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['old_password', 'new_password', 'repeat_new_password'], 'required'],
            // Email must be a valid email
            ['old_password', 'existsPassword'],
            ['new_password', 'string', 'min' => 6],
            ['repeat_new_password', 'compare', 'compareAttribute' => 'new_password'],
        ];
    }

    public function existsPassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->old_password)) {
                $this->addError($attribute, 'Contraseña incorrecta');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'old_password' => 'Contraseña actual',
            'new_password' => 'Nueva contraseña',
            'repeat_new_password' => 'Repita nueva contraseña',
        ];
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser() {
        if ($this->_user === null) {
            $this->_user = User::findByUsername(Yii::$app->user->identity->username);
        }

        return $this->_user;
    }

}
