<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * MateriaPublica Upload Document form
 */
class MatrizTipoVentaForm extends Model {

    /**
     * @var UploadedFile
     */
    public $file;
    public $description;
    public $sucursal;
    public $documento_subido;
    
    public $unica;
    public $empresa;
    public $mandato;
    public $sucesion;
    
    const PATH = 'uploads' . DIRECTORY_SEPARATOR . 'matrices' . DIRECTORY_SEPARATOR . 'sucursal' . DIRECTORY_SEPARATOR;
    const GENERATED_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'matrices' . DIRECTORY_SEPARATOR . 'sucursal' . DIRECTORY_SEPARATOR . 'generated' . DIRECTORY_SEPARATOR;
    const FILE_EXTENSION = ['doc', 'docx'];
    
    const PERSONERIA = "PERSONERÍA";
    const MANDATARIO = "MANDATARIO";

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx'],
            [['id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'file' => 'Documento Matriz',
        ];
    }

    public function saveDocument($sucursal_id) {
        if ($this->validate()) {
            $filename_array = (explode(".", $this->file->name));
            // store the source file name
            $ext = end($filename_array);
            try {
                $folderPath = static::PATH . $sucursal_id . DIRECTORY_SEPARATOR;
                //$path = $folderPath . $this->description . '.' . $ext;
                $path = $folderPath . Yii::$app->user->id . '_' . $this->description . '.' . $ext;

                if (!is_dir($folderPath)) {
                    mkdir($folderPath, 0777, true);
                }
                if ($this->file->saveAs($path)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception $ex) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function search($sucursal_id) {
        $files = (file_exists(self::PATH . DIRECTORY_SEPARATOR . $sucursal_id . DIRECTORY_SEPARATOR)) ? \yii\helpers\FileHelper::findFiles(self::PATH . $sucursal_id . DIRECTORY_SEPARATOR) : null;
        $files_array = [];
        if (!empty($files)) {
            foreach ($files as $file) {
                $desc_begin = strrpos($file, DIRECTORY_SEPARATOR) + 1;
                $desc_end = strrpos($file, '.');
                //$file_array['descripcion'] = substr($file, $desc_begin, $desc_end - $desc_begin);
                $fileName = substr($file, $desc_begin, $desc_end - $desc_begin);
                $userId = substr($fileName, 0, strpos($fileName, '_'));
                $description = substr($fileName, strpos($fileName, '_') + 1);
                $this->sucursal = Sucursal::find()->where('id = :id', [':id' => $userId])->one();

                $file_array['description'] = $description;
                $file_array['url'] = \yii\helpers\Url::base() . $file;
                $file_array['sucursal'] = $this->sucursal;
                $files_array[] = $file_array;
            }
        }

        $dataProvider = new \yii\data\ArrayDataProvider([
            'key' => 'url',
            'allModels' => $files_array,
            'sort' => [
                'attributes' => ['descripcion'],
            ],
        ]);
        
        return $dataProvider;
    }
    
}
