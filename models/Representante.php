<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "representante".
 *
 * @property int $id
 * @property int $propietario_id
 * @property string $rut
 * @property string $nombre
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $direccion
 *
 * @property Propietario $propietario
 */
class Representante extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'representante';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['propietario_id'], 'integer'],
            [['rut', 'nombre', 'apellido_paterno'], 'required'],
            [['direccion'], 'string'],
            [['rut'], 'string', 'max' => 13],
            [['nombre', 'apellido_paterno', 'apellido_materno'], 'string', 'max' => 100],
            [['rut'], 'unique'],
            [['propietario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['propietario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'propietario_id' => 'Propietario ID',
            'rut' => 'Rut',
            'nombre' => 'Nombre',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'direccion' => 'Direccion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario() {
        return $this->hasOne(Propietario::className(), ['id' => 'propietario_id']);
    }

    public function getCompleteName() {
        return $this->nombre . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno;
    }
    
    public function getCompleteNameWithRut() {
        return $this->nombre . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno . ' (' . $this->rut . ')';
    }
    
}
