<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class UserForm extends Model {

    public $id;
    public $username;
    public $auth_key;
    public $password_hash;
    public $password_reset_token;
    public $nombre;
    public $apellido;
    public $sucursal_id;
    public $email;
    public $status;
    public $created_at;
    public $updated_at;
    public $requiere_cambio_pass;
    public $tipo_usuario;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'sucursal_id', 'tipo_usuario', 'email'], 'required'],
            [['username', 'nombre', 'apellido'], 'string', 'max' => 100],
            [['sucursal_id', 'status', 'created_at', 'updated_at', 'requiere_cambio_pass'], 'integer'],
            ['email', 'checkUniqueEmail'],
            [['username', 'email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'Usuario',
            'auth_item' => 'Roles y Permisos',
        ];
    }

    public function checkUniqueEmail($attribute, $params) {
        $email = User::find()->select("email")->where("email = :email", [":email" => $this->email])->one();
        if (isset($email)) {
            $this->addError($attribute, "$email->email ya existe. Ingrese otro nombre.");
        }
    }

    public function createUser() {
        $signUpForm = new \app\models\SignupForm();
        $signUpForm->username = trim($this->username);
        $signUpForm->email = trim($this->email);
        $signUpForm->password = Yii::$app->security->generateRandomString(8);

        if ($user = $signUpForm->signup($this->tipo_usuario, $this->sucursal_id)) {
            $user->nombre = trim($this->nombre);
            $user->apellido = $this->apellido;
            if ($user->save()) {
                $user->sendEmailForNewUser($user->nombre, $user->apellido, $user->username, $user->password_hash, $user->email);
            } else {
                throw new \Exception(join(',', $this->getFirstErrors()));
            }
        } else {
            throw new \Exception('Error al intentar crear al usuario para el sistema');
        }
    }

}
