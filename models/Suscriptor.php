<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suscriptor".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property EstadoVehiculo[] $estadoVehiculos
 * @property Operacion[] $operacions
 */
class Suscriptor extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'suscriptor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoVehiculos() {
        return $this->hasMany(EstadoVehiculo::className(), ['suscriptor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions() {
        return $this->hasMany(Operacion::className(), ['suscriptor_id' => 'id']);
    }

}
