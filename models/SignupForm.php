<?php

namespace app\models;

use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $username;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'El nombre de usuario ya ha sido tomado'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Este correo electrónico ya ha sido tomado.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'username' => 'Usuario',
            'password' => 'Contraseña',
            'email' => 'Correo electrónico',
        ];
    }

    /**
     * Signs user up.
     *
     * @param The role assign to the user
     * @return User|null the saved model or null if saving fails
     */
    public function signup($role, $sucursal_id) {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->sucursal_id = $sucursal_id;
            $user->requiere_cambio_pass = 1;
            $user->status = User::STATUS_ACTIVE;
            $user->save(false);

            // the following three lines were added:
            $auth = \Yii::$app->authManager;
            $authorRole = $auth->getRole($role);
            $auth->assign($authorRole, $user->getId());

            return $user;
        }

        return null;
    }

}
