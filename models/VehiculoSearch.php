<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vehiculo;

/**
 * VehiculoSearch represents the model behind the search form of `app\models\Vehiculo`.
 */
class VehiculoSearch extends Vehiculo {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'agno', 'modelo_id', 'tiene_prenda', 'tiene_multa', 'tipo_vehiculo_id', 'tipo_combustible_id'], 'integer'],
            [['patente', 'chasis', 'motor', 'color', 'pbv'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Vehiculo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'agno' => $this->agno,
            'modelo_id' => $this->modelo_id,
            'tiene_prenda' => $this->tiene_prenda,
            'tiene_multa' => $this->tiene_multa,
            'tipo_vehiculo_id' => $this->tipo_vehiculo_id,
            'tipo_combustible_id' => $this->tipo_combustible_id,
        ]);

        $query->andFilterWhere(['like', 'patente', $this->patente])
                ->andFilterWhere(['like', 'chasis', $this->chasis])
                ->andFilterWhere(['like', 'motor', $this->motor])
                ->andFilterWhere(['like', 'color', $this->color])
                ->andFilterWhere(['like', 'pbv', $this->pbv]);

        return $dataProvider;
    }

}
