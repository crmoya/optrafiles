<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class AuthItemChildForm extends Model {

    public $parent;
    public $child;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['parent', 'child',], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'parent' => 'Rol o Permiso Padre',
            'child' => 'Rol o Permiso Hijo',
        ];
    }

}
