<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operacion".
 *
 * @property int $id
 * @property string $venta_fecha
 * @property int $venta_propietario_id
 * @property int $venta_precio
 * @property int $venta_tipo
 * @property int $venta_user_id
 * @property int $venta_mandatario_id
 * @property int $venta_vehiculo_id
 * @property string $venta_personeria_mandato
 * @property string $compra_fecha
 * @property int $compra_precio
 * @property int $compra_propietario_id
 * @property int $compra_tipo
 * @property string $compra_personeria
 * @property int $compra_user_id
 * @property int $compra_para_id
 * @property int $sucursal_id
 *
 * @property CompraPropietario[] $compraPropietarios
 * @property EnviadaA[] $enviadaAs
 * @property Notaria[] $notarias
 * @property Notificacion[] $notificacions
 * @property Mandatario $ventaMandatario
 * @property Propietario $ventaPropietario
 * @property Propietario $compraPropietario
 * @property Propietario $compraPara
 * @property Sucursal $sucursal
 * @property User $ventaUser
 * @property User $compraUser
 * @property Vehiculo $ventaVehiculo
 * @property VentaPropietario[] $ventaPropietarios
 * @property VentaRepresentante[] $ventaRepresentantes
 * @property CompraRepresentante[] $compraRepresentantes
 */
class Operacion extends \yii\db\ActiveRecord {

    const TIPO_VENTA = array("1" => "TOMA VEHÍCULO ÚNICA", "2" => "TOMA VEHÍCULO CON REPRESENTANTE EMPRESA", 
        "3" => "TOMAR VEHÍCULO CON REPRESENTANTE MANDATO", "4" => "TOMA VEHÍCULO COMUNIDAD");
    const TIPO_COMPRA = array("1" => "VENTA ÚNICA", "2" => "VENTA CON REPRESENTANTE EMPRESA", "3" => "VENTA PARA",
        "4" => "VENTA COMUNIDAD");
    const FILE_EXTENSION = 'docx';
    
    public $etapa;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'operacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['venta_fecha', 'venta_propietario_id', 'venta_user_id', 'venta_vehiculo_id', 'sucursal_id'], 'required'],
            [['venta_fecha', 'compra_fecha'], 'safe'],
            [['venta_propietario_id', 'venta_precio', 'venta_tipo', 'venta_user_id', 'venta_mandatario_id', 'venta_vehiculo_id', 'compra_precio', 'compra_propietario_id', 'compra_tipo', 'compra_user_id', 'compra_para_id', 'sucursal_id'], 'integer'],
            [['venta_personeria_mandato', 'compra_personeria'], 'string'],
            [['venta_mandatario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mandatario::className(), 'targetAttribute' => ['venta_mandatario_id' => 'id']],
            [['venta_propietario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['venta_propietario_id' => 'id']],
            [['compra_propietario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['compra_propietario_id' => 'id']],
            [['compra_para_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['compra_para_id' => 'id']],
            [['sucursal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['sucursal_id' => 'id']],
            [['venta_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['venta_user_id' => 'id']],
            [['compra_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['compra_user_id' => 'id']],
            [['venta_vehiculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['venta_vehiculo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'venta_fecha' => 'Venta Fecha',
            'venta_propietario_id' => 'Venta Propietario ID',
            'venta_precio' => 'Venta Precio',
            'venta_tipo' => 'Venta Tipo',
            'venta_user_id' => 'Funcionario',
            'venta_mandatario_id' => 'Venta Mandatario ID',
            'venta_vehiculo_id' => 'Venta Vehiculo ID',
            'venta_personeria_mandato' => 'Venta Personeria Mandato',
            'compra_fecha' => 'Compra Fecha',
            'compra_precio' => 'Compra Precio',
            'compra_propietario_id' => 'Compra Propietario ID',
            'compra_tipo' => 'Compra Tipo',
            'compra_personeria' => 'Compra Personeria',
            'compra_user_id' => 'Compra User ID',
            'compra_para_id' => 'Compra Para ID',
            'sucursal_id' => 'Sucursal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraPropietarios() {
        return $this->hasMany(CompraPropietario::className(), ['operacion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnviadaAs() {
        return $this->hasMany(EnviadaA::className(), ['operacion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotarias() {
        return $this->hasMany(Notaria::className(), ['id' => 'notaria_id'])->viaTable('enviada_a', ['operacion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificacions() {
        return $this->hasMany(Notificacion::className(), ['operacion_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaMandatario() {
        return $this->hasOne(Mandatario::className(), ['id' => 'venta_mandatario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaPropietario() {
        return $this->hasOne(Propietario::className(), ['id' => 'venta_propietario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraPropietario() {
        return $this->hasOne(Propietario::className(), ['id' => 'compra_propietario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraPara() {
        return $this->hasOne(Propietario::className(), ['id' => 'compra_para_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursal() {
        return $this->hasOne(Sucursal::className(), ['id' => 'sucursal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaUser() {
        return $this->hasOne(User::className(), ['id' => 'venta_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraUser() {
        return $this->hasOne(User::className(), ['id' => 'compra_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaVehiculo() {
        return $this->hasOne(Vehiculo::className(), ['id' => 'venta_vehiculo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaPropietarios() {
        return $this->hasMany(VentaPropietario::className(), ['operacion_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaRepresentantes() {
        return $this->hasMany(VentaRepresentante::className(), ['operacion_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraRepresentantes() {
        return $this->hasMany(CompraRepresentante::className(), ['operacion_id' => 'id']);
    }

    public function getCompleteNameVendedorWithRut() {
        return $this->ventaPropietario->nombres . ' ' . $this->ventaPropietario->apellido_paterno . ' ' . $this->ventaPropietario->apellido_materno .
                    ' (' . $this->ventaPropietario->rut . ')';
    }
    
}
