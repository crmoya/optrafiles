<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notaria".
 *
 * @property int $id
 * @property string $nombre
 * @property int $tiene_aiken
 *
 * @property Etapa3[] $etapa3s
 * @property Contrato[] $contratos
 */
class Notaria extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'notaria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['nombre'], 'required'],
            [['tiene_aiken'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'tiene_aiken' => 'Tiene Aiken',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtapa3s() {
        return $this->hasMany(Etapa3::className(), ['notaria_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContratos() {
        return $this->hasMany(Contrato::className(), ['id' => 'contrato_id'])->viaTable('etapa3', ['notaria_id' => 'id']);
    }

}
