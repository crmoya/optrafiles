<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "venta_propietario".
 *
 * @property int $id
 * @property int $propietario_id
 * @property int $operacion_id
 *
 * @property Operacion $operacion
 * @property Propietario $propietario
 */
class VentaPropietario extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'venta_propietario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['propietario_id', 'operacion_id'], 'required'],
            [['propietario_id', 'operacion_id'], 'integer'],
            [['operacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacion::className(), 'targetAttribute' => ['operacion_id' => 'id']],
            [['propietario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['propietario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'propietario_id' => 'Propietario ID',
            'operacion_id' => 'Operacion ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion() {
        return $this->hasOne(Operacion::className(), ['id' => 'operacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario() {
        return $this->hasOne(Propietario::className(), ['id' => 'propietario_id']);
    }

}
