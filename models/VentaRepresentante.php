<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "venta_representante".
 *
 * @property int $id
 * @property int $operacion_id
 * @property int $representante_id
 *
 * @property Operacion $operacion
 * @property Representante $representante
 */
class VentaRepresentante extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'venta_representante';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['operacion_id', 'representante_id'], 'required'],
            [['operacion_id', 'representante_id'], 'integer'],
            [['operacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacion::className(), 'targetAttribute' => ['operacion_id' => 'id']],
            [['representante_id'], 'exist', 'skipOnError' => true, 'targetClass' => Representante::className(), 'targetAttribute' => ['representante_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'operacion_id' => 'Operacion ID',
            'representante_id' => 'Representante ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion() {
        return $this->hasOne(Operacion::className(), ['id' => 'operacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepresentante() {
        return $this->hasOne(Representante::className(), ['id' => 'representante_id']);
    }

}
