<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class AuthAssignmentForm extends Model {

    public $user_id;
    public $auth_item;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['user_id', 'auth_item',], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'Usuario',
            'auth_item' => 'Roles y Permisos',
        ];
    }

}
