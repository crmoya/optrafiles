<?php

namespace app\models;

use \yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $nombre
 * @property string $apellido
 * @property int $sucursal_id
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $requiere_cambio_pass
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property EstadoVehiculo[] $estadoVehiculos
 * @property Vehiculo[] $vehiculos
 * @property Operacion[] $operacions
 * @property Sucursal $sucursal
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const TIPOS_USUARIOS = [
        "admin" => "ADMINISTRADOR",
        "funcionario" => "FUNCIONARIO",
        "mecanico" => "MECÁNICO",
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user';
    }

    public function behaviors() {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['username', 'auth_key', 'password_hash', 'sucursal_id'], 'required'],
            [['sucursal_id', 'status', 'created_at', 'updated_at', 'requiere_cambio_pass'], 'integer'],
            [['username', 'nombre', 'apellido'], 'string', 'max' => 100],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['sucursal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['sucursal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'sucursal_id' => 'Sucursal',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'requiere_cambio_pass' => 'Requiere Cambio Pass',
        ];
    }

    public function fields() {
        $fields = parent::fields();
        // quita los campos con información sensible
        unset($fields['auth_key'], $fields['password_hash'], $fields['password_reset_token']);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments() {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames() {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoVehiculos() {
        return $this->hasMany(EstadoVehiculo::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos() {
        return $this->hasMany(Vehiculo::className(), ['id' => 'vehiculo_id'])->viaTable('estado_vehiculo', ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacions() {
        return $this->hasMany(Operacion::className(), ['venta_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursal() {
        return $this->hasOne(Sucursal::className(), ['id' => 'sucursal_id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public static function getUsername($id) {
        $user = static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        return (empty($user)) ? "" : $user->username;
    }

    public static function getEmails() {
        $emails = \yii\helpers\ArrayHelper::map(static::find()->select('email')->asArray()->all(), 'email', 'email');
        return $emails;
    }

    public static function sendEmailForNewUser($names, $lastname, $rut, $password, $to) {
        $variables = [
            'names' => $names,
            'lastname' => $lastname,
            'rut' => $rut,
            'password' => $password,
        ];
        $subject = 'Usuario creado en Notaría Miranda';
        $templates = ['html' => 'userCreated-html', 'text' => 'userCreated-text'];
        return \app\components\EmailComponent::sendQueueEmail(\Yii::$app->params['seguimientoEmail'], Yii::$app->name, $to, $subject, $templates, $variables);
    }

    public function removeUserAndAuth() {
        $transaction = Yii::$app->db->beginTransaction();
        $auth = \Yii::$app->authManager;

        if ($auth->revokeAll($this->id) && $this->delete()) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            return false;
        }
    }

    public function getRole() {
        if (!\Yii::$app->user->isGuest) {
            $a = AuthAssignment::find()->where('user_id = :id', [':id' => Yii::$app->user->id])->one();
            return $a->item_name;
        } else {
            return "";
        }
    }
    
    public function getCompleteName() {
        return $this->nombre . ' ' . $this->apellido;
    }

    public static function getSuscriptorId() {
        return \Yii::$app->user->getIdentity()->sucursal->suscriptor->id;
    }
    
}
