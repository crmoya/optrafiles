<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vehiculo_en_sucursal".
 *
 * @property int $vehiculo_id
 * @property int $sucursal_id
 * @property string $desde
 *
 * @property Vehiculo $vehiculo
 */
class VehiculoEnSucursal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vehiculo_en_sucursal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vehiculo_id', 'sucursal_id', 'desde'], 'required'],
            [['vehiculo_id', 'sucursal_id'], 'integer'],
            [['desde'], 'safe'],
            [['vehiculo_id', 'sucursal_id'], 'unique', 'targetAttribute' => ['vehiculo_id', 'sucursal_id']],
            [['vehiculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['vehiculo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vehiculo_id' => 'Vehiculo ID',
            'sucursal_id' => 'Sucursal ID',
            'desde' => 'Desde',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'vehiculo_id']);
    }
}
