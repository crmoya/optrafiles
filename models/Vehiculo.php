<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vehiculo".
 *
 * @property int $id
 * @property string $patente
 * @property string $chasis
 * @property string $motor
 * @property int $agno
 * @property int $modelo_id
 * @property int $tiene_prenda
 * @property int $tiene_multa
 * @property int $tipo_vehiculo_id
 * @property string $color
 * @property int $tipo_combustible_id
 * @property string $pbv
 * @property int $propietario_id
 * @property int $tasacion_comercial
 *
 * @property EstadoVehiculo[] $estadoVehiculos
 * @property User[] $usuarios
 * @property Modelo $modelo
 * @property TipoCombustible $tipoCombustible
 * @property TipoVehiculo $tipoVehiculo
 * @property VehiculoEnSucursal[] $vehiculoEnSucursals
 * @property Sucursal[] $sucursals
 * @property Propietario $propietario
 * 
 */
class Vehiculo extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'vehiculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['patente', 'chasis', 'motor', 'agno', 'modelo_id', 'tipo_vehiculo_id', 'tipo_combustible_id'], 'required'],
            [['agno', 'modelo_id', 'tiene_prenda', 'tiene_multa', 'tipo_vehiculo_id', 'tipo_combustible_id', 'tasacion_comercial', 'propietario_id'], 'integer'],
            [['patente'], 'string', 'max' => 8],
            [['chasis', 'motor', 'color'], 'string', 'max' => 45],
            [['pbv'], 'string', 'max' => 20],
            [['patente'], 'unique'],
            [['chasis'], 'unique'],
            [['motor'], 'unique'],
            [['modelo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modelo::className(), 'targetAttribute' => ['modelo_id' => 'id']],
            [['propietario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['propietario_id' => 'id']],
            [['tipo_combustible_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoCombustible::className(), 'targetAttribute' => ['tipo_combustible_id' => 'id']],
            [['tipo_vehiculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoVehiculo::className(), 'targetAttribute' => ['tipo_vehiculo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'patente' => 'Patente',
            'chasis' => 'N° Chasis',
            'motor' => 'N° Motor',
            'agno' => 'Año',
            'tasacion_comercial' => 'Tasación Comercial',
            'modelo_id' => 'Modelo ID',
            'tiene_prenda' => 'Tiene Prenda',
            'tiene_multa' => 'Tiene Multa',
            'tipo_vehiculo_id' => 'Tipo Vehiculo',
            'color' => 'Color',
            'tipo_combustible_id' => 'Tipo Combustible',
            'pbv' => 'PBV',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoVehiculos() {
        return $this->hasMany(EstadoVehiculo::className(), ['vehiculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios() {
        return $this->hasMany(User::className(), ['id' => 'usuario_id'])->viaTable('estado_vehiculo', ['vehiculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelo() {
        return $this->hasOne(Modelo::className(), ['id' => 'modelo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCombustible() {
        return $this->hasOne(TipoCombustible::className(), ['id' => 'tipo_combustible_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoVehiculo() {
        return $this->hasOne(TipoVehiculo::className(), ['id' => 'tipo_vehiculo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculoEnSucursals() {
        return $this->hasMany(VehiculoEnSucursal::className(), ['vehiculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursals() {
        return $this->hasMany(Sucursal::className(), ['id' => 'sucursal_id'])->viaTable('vehiculo_en_sucursal', ['vehiculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario() {
        return $this->hasOne(Propietario::className(), ['id' => 'propietario_id']);
    }

    public function validateNotInProcess() {
        $operacion = Operacion::find()->where("venta_vehiculo_id = :vId", [":vId" => $this->id])->orderBy("id DESC")->one();
        if (isset($operacion)) {
            if (isset($operacion->compra_fecha)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    public function validateInStage1() {
        $operacion = Operacion::find()->where("venta_vehiculo_id = :vId", [":vId" => $this->id])->orderBy("id DESC")->one();
        if (isset($operacion)) {
            if (isset($operacion->venta_fecha) && !isset($operacion->compra_fecha)) {
                return $operacion;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    
}
