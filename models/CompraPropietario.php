<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compra_propietario".
 *
 * @property int $id
 * @property int $operacion_id
 * @property int $propietario_id
 *
 * @property Operacion $operacion
 * @property Propietario $propietario
 */
class CompraPropietario extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'compra_propietario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['operacion_id', 'propietario_id'], 'required'],
            [['operacion_id', 'propietario_id'], 'integer'],
            [['operacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacion::className(), 'targetAttribute' => ['operacion_id' => 'id']],
            [['propietario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Propietario::className(), 'targetAttribute' => ['propietario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'operacion_id' => 'Operacion ID',
            'propietario_id' => 'Propietario ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacion() {
        return $this->hasOne(Operacion::className(), ['id' => 'operacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario() {
        return $this->hasOne(Propietario::className(), ['id' => 'propietario_id']);
    }

}
