<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sucursal".
 *
 * @property int $id
 * @property string $nombre
 * @property int $suscriptor_id
 *
 * @property Usuario[] $usuarios
 * @property VehiculoEnSucursal[] $vehiculoEnSucursals
 * @property Vehiculo[] $vehiculos
 * @property Suscriptor $suscriptor
 */
class Sucursal extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'sucursal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
            [['suscriptor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Suscriptor::className(), 'targetAttribute' => ['suscriptor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios() {
        return $this->hasMany(Usuario::className(), ['sucursal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculoEnSucursals() {
        return $this->hasMany(VehiculoEnSucursal::className(), ['sucursal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculos() {
        return $this->hasMany(Vehiculo::className(), ['id' => 'vehiculo_id'])->viaTable('vehiculo_en_sucursal', ['sucursal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuscriptor() {
        return $this->hasOne(Suscriptor::className(), ['id' => 'suscriptor_id']);
    }
    
}
