<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Vendedor form
 */
class VendedorForm extends Model {

    public $rut;
    public $nombre;
    //public $razon_social;
    public $apellido_paterno;
    public $apellido_materno;
    public $direccion;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['rut', 'nombre', 'apellido_paterno'], 'required'],
            [['apellido_paterno', 'apellido_materno'], 'string', 'max' => 100],
            [['rut'], \app\components\RutValidator::className()],
            [['rut'], 'string', 'max' => 13],
            [['direccion'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'rut' => "RUT",
            'nombre' => 'NOMBRES',
            'apellido_paterno' => 'APELLIDO PATERNO',
            'apellido_materno' => 'APELLIDO MATERNO',
            'direccion' => 'DIRECCIÓN',
        ];
    }
    
}
