<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * AuthItem form
 */
class AuthItemForm extends Model {

    public $name;
    public $description;
    public $type;

    const ROLE = 1;
    const PERMISSION = 2;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['name', 'type'], 'required'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => 'Nombre',
            'description' => 'Descripción',
            'type' => 'Tipo de permiso',
        ];
    }

    public function getAuthItemTypes() {
        return [
            self::ROLE => 'Rol',
            self::PERMISSION => 'Permiso'
        ];
    }

}
